﻿using Ozone.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ozone.DAL
{
   public class InvoiceDAL
    {
       public InvoiceTab Getinvoicebyid(long invid) 
       {
           InvoiceTab objinvoice;
           using (var db = new OzoneDBEntities())
           {
               objinvoice = db.InvoiceTabs.Where(d => d.PK_Invoice_Id == invid).FirstOrDefault();


           }
           return objinvoice;
       }
       public string AddPayment(InvoiceTab invtab)
       {
           using (var db = new OzoneDBEntities())
           {
               INV_PAYMENT_TAB objinvpayment = new INV_PAYMENT_TAB();
               objinvpayment.Amount = invtab.TotalCost;
               objinvpayment.DateReceive = invtab.UpdatDate;
               objinvpayment.FK_Invoice_Id = invtab.PK_Invoice_Id;
               db.INV_PAYMENT_TAB.Add(objinvpayment);
               var invoice = db.InvoiceTabs.FirstOrDefault(d => d.PK_Invoice_Id == invtab.PK_Invoice_Id);
               invoice.RemeainCost = invoice.RemeainCost - invtab.TotalCost;
               invoice.UpdatDate = System.DateTime.Now;
               db.SaveChanges();
               
           }
           return "suc";
       }
       public List<INV_PAYMENT_TAB> ListofPayments(long invid)
       {
           List<INV_PAYMENT_TAB> lstpayments = new List<INV_PAYMENT_TAB>();
           using (var db= new OzoneDBEntities())
           {
               lstpayments = db.INV_PAYMENT_TAB.Where(d => d.FK_Invoice_Id == invid).ToList();
           }
           return lstpayments;

       }

       public List<ListofinvoceModel> GetStatement(int display, int lenght)
       {

           using (var db = new OzoneDBEntities())
           {
               var data = db.SHOPPING_CART.Where(d => d.Is_Deleted == false && d.Order_Status != "Extra").Select(
                   so => new ListofinvoceModel {  
                      
                        PK_User_ID = so.USER_REGISTRATION.Pk_Register_Id,
                        ReceivePayment =so.InvoiceTabs.Sum(a=>(a.TotalCost - a.RemeainCost)).HasValue?so.InvoiceTabs.Sum(a=>(a.TotalCost - a.RemeainCost)).ToString() :"0" ,
                        totalorderpayment =so.Payment_Amt.ToString(),
                        TotalPayment= so.InvoiceTabs.Sum(b=>b.TotalCost + b.ShippingRate).HasValue? so.InvoiceTabs.Sum(b=>b.TotalCost + b.ShippingRate).ToString() : "0",
                        UserName= so.USER_REGISTRATION.Company_Name,
                        orderdate = so.Create_Date,
                        Salesordernumber = so.Order_Number,
                        orderid=  so.Pk_UserCart_Id
              
                   }
                   ).OrderByDescending(d=>d.orderdate).OrderBy(d=>d.UserName).Skip(display).Take(lenght).ToList();

               return data;  
           }

       }
       public int Statementcount()
       {
           using (var db = new OzoneDBEntities())
           {
               return db.SHOPPING_CART.Where(d => d.Is_Deleted == false && d.Order_Status != "Extra").Count();
           }
       }
       public List<ListofinvoceModel> Getexpringinvoice()
       {
           List<ListofinvoceModel> lstinv = new List<ListofinvoceModel>();
           using (var db = new OzoneDBEntities())
           {
               var data = db.InvoiceTabs.Where(d=>d.RemeainCost>0) .OrderBy(d => d.DueDate).Take(10).ToList();

               foreach (var item in data)
               {
                   lstinv.Add(new ListofinvoceModel
               {
                   Salesordernumber = item.SHOPPING_CART.Order_Number,
                   InvoiceOrder = item.InvoiceNumber,
                   DueDate = item.DueDate.Value.ToShortDateString(),
                   TotalPayment = item.RemeainCost.Value.ToString(),
                   UserName =Math.Round( (item.DueDate.Value - System.DateTime.Now).TotalDays,0).ToString()
               });
               }

              
           }
           return lstinv;

       }
    }
}
