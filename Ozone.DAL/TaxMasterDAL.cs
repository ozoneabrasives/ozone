﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Ozone.DAL
{
    public class TaxMasterDAL
    {
        public string Insert(TaxMaster objtax)
        {
            string result;
            using (var db = new OzoneDBEntities())
            {
                var t = db.TaxMasters.Where(d => d.FK_Country_id == objtax.FK_Country_id && d.IsDeleted == false).FirstOrDefault();
                if (t==null)
                {
                    db.TaxMasters.Add(objtax);
                }
                else
                {
                    t.Charges = objtax.Charges;
                }
               

               
                db.SaveChanges();
                result= "success";
                
            }
            return result;
        }
        public string Update(TaxMaster objtax)
        {
            string result;
            using (var db = new OzoneDBEntities())
            {
                var tax = db.TaxMasters.Where(d => d.TaxID == objtax.TaxID).FirstOrDefault();
                tax.Charges = objtax.Charges;
                tax.FK_Country_id = objtax.FK_Country_id;
                db.SaveChanges();
                result = "success";

            }
            return result;
        }
        public TaxMaster GetByid(long id)
        {
            TaxMaster objtax;
            using (var db = new OzoneDBEntities())
            {
                objtax = db.TaxMasters.Where(d => d.TaxID == id).FirstOrDefault();
                
            }
            return objtax;
        }
        public List<TaxMaster> ListTax(int display, int lenght)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                dbCon.Configuration.ProxyCreationEnabled = false;
                return dbCon.TaxMasters.Where(d=>d.IsDeleted==false).Include(d=>d.COUNTRY_MASTER). OrderBy(d =>d.TaxID ).Skip(display).Take(lenght).ToList();
            }
        }
        public int GetCount()
        {
            int count;
            using (var db = new OzoneDBEntities())
            {
                count = db.TaxMasters.Where(d => d.IsDeleted == false).Count();
                
            }
            return count;
        }
        public decimal GetTaxByUserid(long userid,int qty)
        {
            decimal tax=0;
            using (var db = new OzoneDBEntities())
            {
             //   long user = db.USER_REGISTRATION.Where(d => d.Pk_Register_Id == userid).Select(d => d.Fk_Country_Id.Value).FirstOrDefault();

                if (userid == 29)
                {
                    var dbtax = db.ShipRateTabs.Where(d => d.Qty == qty).FirstOrDefault();
                    if (dbtax!=null)
                    {
                        tax = dbtax.Rate.Value;
                        
                    }
                }
                else {
                    var dbtax = db.TaxMasters.Where(d => d.FK_Country_id == userid).FirstOrDefault();
                    if (dbtax != null)
                    {
                        tax = dbtax.Charges.Value;

                    }
                }
                
                
                
            }
            return tax;
        }
    }
}
