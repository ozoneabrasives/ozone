//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ozone.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class PACKAGE_MASTER
    {
        public PACKAGE_MASTER()
        {
            this.PURCHASEORDER_ITEMS = new HashSet<PURCHASEORDER_ITEMS>();
            this.SHOPPING_CART_ITEMS = new HashSet<SHOPPING_CART_ITEMS>();
            this.INVENTORies = new HashSet<INVENTORY>();
        }
    
        public long Pk_Package_Id { get; set; }
        public Nullable<long> Fk_Product_Id { get; set; }
        public string Package_Title { get; set; }
        public string Package_Unit { get; set; }
        public Nullable<long> Created_by { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<long> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public Nullable<bool> Is_Deleted { get; set; }
        public Nullable<decimal> Cost_Price { get; set; }
        public Nullable<decimal> Sell_Price { get; set; }
        public Nullable<int> Package_Amt { get; set; }
    
        public virtual PRODUCT_DETAILS PRODUCT_DETAILS { get; set; }
        public virtual ICollection<PURCHASEORDER_ITEMS> PURCHASEORDER_ITEMS { get; set; }
        public virtual ICollection<SHOPPING_CART_ITEMS> SHOPPING_CART_ITEMS { get; set; }
        public virtual ICollection<INVENTORY> INVENTORies { get; set; }
    }
}
