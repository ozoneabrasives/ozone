﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ozone.DAL
{
    public class RegisterDAL
    {
        //OzoneDBEntities dbContext = new OzoneDBEntities();

        public RegisterDAL()
        {
        }

        public RegisterDAL(long id)
        {
            using (var dbContext = new OzoneDBEntities())
            {
                USER_REGISTRATION usrReg = dbContext.USER_REGISTRATION.SingleOrDefault(c => c.Pk_Register_Id == id);
            }
        }

        public long InsertRegister(USER_REGISTRATION usrReg)
        {
            try
            {
                using (var dbContext = new OzoneDBEntities())
                {
                    dbContext.USER_REGISTRATION.Add(usrReg);
                    dbContext.SaveChanges();
                    return usrReg.Pk_Register_Id;
                }
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var eve in ex.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Value: \"{1}\", Error: \"{2}\"",
                 ve.PropertyName,
                 eve.Entry.CurrentValues.GetValue<object>(ve.PropertyName),
                 ve.ErrorMessage);
                    }
                }
                throw;
            }
        }

        public long UpdateRegister(USER_REGISTRATION usr)
        {
            try
            {
                using (var dbCon = new OzoneDBEntities())
                {
                    USER_REGISTRATION reg = dbCon.USER_REGISTRATION.SingleOrDefault(c => c.Pk_Register_Id == usr.Pk_Register_Id);
                    reg.Auth_Officer_Name = usr.Auth_Officer_Name;
                    reg.Company_Name = usr.Company_Name;
                    reg.Company_Reg_No = usr.Company_Reg_No;
                    reg.Correspond_Add = usr.Correspond_Add;
                    reg.Created_Date = System.DateTime.Now;

                    reg.Email_ID = usr.Email_ID;
                    reg.Fax = usr.Fax;
                    reg.Fk_City_Id = usr.Fk_City_Id;
                    reg.Fk_Country_Id = usr.Fk_Country_Id;
                    reg.Fk_State_Id = usr.Fk_State_Id;
                    reg.Is_Approved = usr.Is_Approved;
                    reg.Landline_No = usr.Landline_No;
                    reg.Mobile_No = usr.Mobile_No;
                    reg.Payment_Terms = usr.Payment_Terms;
                    reg.Permanent_Add = usr.Permanent_Add;
                    //reg.Register_Type = usr.Register_Type;

                    reg.VAT_No = usr.VAT_No;
                    reg.Have_Credit = false;
                    reg.Is_Approved = true;
                    reg.CurrencyID = usr.CurrencyID;
                    //reg.Register_Type = usr.Register_Type;
                    //reg.User_Type = usr.User_Type;
                    dbCon.SaveChanges();
                }
                return usr.Pk_Register_Id;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public USER_REGISTRATION CheckUser(USER_REGISTRATION usrReg)
        {
            try
            {
                using (var dbCon = new OzoneDBEntities())
                {
                    if (string.IsNullOrEmpty(usrReg.User_Password))
                        return dbCon.USER_REGISTRATION.SingleOrDefault(c => c.User_Name == usrReg.User_Name && c.Is_Approved == true);
                    else
                        return dbCon.USER_REGISTRATION.SingleOrDefault(c => c.User_Name == usrReg.User_Name && c.User_Password == usrReg.User_Password && c.Is_Approved == true);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<USER_REGISTRATION> ListUsers(int display, int lenght ,string regtype)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                dbCon.Configuration.ProxyCreationEnabled = false;
                if (regtype == "")
                {
                    return dbCon.USER_REGISTRATION.Where(c => c.Is_Approved == true).OrderBy(d => d.Created_Date).Skip(display).Take(lenght).ToList();
                }
                else
                {
                    return dbCon.USER_REGISTRATION.Where(c => c.Is_Approved == true && c.Register_Type == regtype).OrderBy(d => d.Created_Date).Skip(display).Take(lenght).ToList();
                }
                
            }
        }

        public int GetUsersCount(string regtype)
        {
            using (var dbcon = new OzoneDBEntities())
            {
                if (regtype == "")
                {
                    return dbcon.USER_REGISTRATION.Where(c => c.Is_Approved == true ).Count();
                }
                else
                {
                    return dbcon.USER_REGISTRATION.Where(c => c.Is_Approved == true && c.Register_Type == regtype).Count();
                }
               

            }
        }

        public USER_REGISTRATION GetById(long id)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                return dbCon.USER_REGISTRATION.SingleOrDefault(c => c.Is_Approved == true && c.Pk_Register_Id == id);
            }
        }

        public List<USER_REGISTRATION> UpdateCredit(List<USER_REGISTRATION> lst)
        {
            try
            {
                List<USER_REGISTRATION> lstCred = new List<USER_REGISTRATION>();
                using (var dbCon = new OzoneDBEntities())
                {
                    foreach (var item in lst)
                    {
                        USER_REGISTRATION reg = dbCon.USER_REGISTRATION.SingleOrDefault(c => c.Pk_Register_Id == item.Pk_Register_Id);
                        if (reg.Have_Credit == true)
                        {
                            lstCred.Add(reg);
                        }
                        reg.Have_Credit = item.Have_Credit;

                        dbCon.SaveChanges();
                      
                    }
                    return lstCred; //"All selected users are allow to purchase on credit.";
                }

            }
            catch (Exception ex)
            {
                return null; //"Something Wrong, Please contact administrator.";
            }

        }
        public List<USER_REGISTRATION> DDLVendor()
        {
            try
            {
                using (var dbCon = new OzoneDBEntities())
                {
                    return dbCon.USER_REGISTRATION.AsEnumerable().Where(c => c.User_Type == "Vendor").Select(c => new USER_REGISTRATION()
                    {
                        Pk_Register_Id = c.Pk_Register_Id,
                        Company_Name = c.Company_Name
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static long GetCorLastId()
        {
            try
            {
                using (var db = new OzoneDBEntities())
                {
                    return db.USER_REGISTRATION.Where(d => d.Is_Approved == true && d.Register_Type == "Corporate").Max(c => c.Pk_Register_Id)+1;
                }
            }
            catch (Exception)
            {
                return 1;
            }
        }

        public static long GetRetailLastId()
        {
            try
            {
                using (var db = new OzoneDBEntities())
                {
                    return db.USER_REGISTRATION.Where(d => d.Is_Approved == true && d.Register_Type == "Retail").Max(c => c.Pk_Register_Id)+1;
                }
            }
            catch (Exception)
            {
                return 1;
            }
           
        }

        public void ConfirmUsr(long id)
        {
            using (var db = new OzoneDBEntities())
            {
                USER_REGISTRATION usr = db.USER_REGISTRATION.SingleOrDefault(c => c.Pk_Register_Id == id);
                usr.Is_Approved = true;
                db.SaveChanges();
            }
        }

        public List<USER_REGISTRATION> getuserlistbyId(List<long> lst)
        {
            using (var d = new OzoneDBEntities())
            {
                var lstuser = d.USER_REGISTRATION.Where(e => lst.Contains(e.Pk_Register_Id)).ToList();
                return lstuser;
            }
        }

        public USER_REGISTRATION GetUserByEmail(string email)
        {
            USER_REGISTRATION objuser;
            using (var db = new OzoneDBEntities())
            {
                objuser = db.USER_REGISTRATION.Where(d => d.Email_ID == email && d.Is_Approved == true).FirstOrDefault();
                
            }
            return objuser;
        }
    }
}
