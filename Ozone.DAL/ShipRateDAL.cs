﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ozone.DAL
{
   public class ShipRateDAL
    {
       public string Insert(ShipRateTab objshiprate)
       {
           string responce;
           using (var db = new OzoneDBEntities())
           {
               db.ShipRateTabs.Add(objshiprate);
               db.SaveChanges();
               responce = "success";
               
           }
           return responce;
       }
       public string Update(ShipRateTab objshiprate)
       {
           string responce;
           using (var db = new OzoneDBEntities())
           {
               var selected = db.ShipRateTabs.Where(d => d.ShipRateId == objshiprate.ShipRateId).FirstOrDefault();
               selected.Qty = objshiprate.Qty;
               selected.Rate = objshiprate.Rate;
               db.SaveChanges();
               responce = "Success";

               
           }
           return responce;
       }

       public ShipRateTab GetBybyid(long id)
       {
           ShipRateTab objshiprate;
           using (var db = new OzoneDBEntities())
           {
               objshiprate = db.ShipRateTabs.Where(d => d.ShipRateId == id).FirstOrDefault();

           }
           return objshiprate;
       }
       public List<ShipRateTab> ListShipRate(int display, int lenght)
       {
           using (var dbCon = new OzoneDBEntities())
           {
               dbCon.Configuration.ProxyCreationEnabled = false;
               return dbCon.ShipRateTabs.Where(d => d.IsDeleted == false).OrderBy(d => d.ShipRateId).Skip(display).Take(lenght).ToList();
           }
       }
       public int GetCount()
       {
           int count;
           using (var db = new OzoneDBEntities())
           {
               count = db.ShipRateTabs.Where(d => d.IsDeleted == false).Count();

           }
           return count;
       }

    }
}
