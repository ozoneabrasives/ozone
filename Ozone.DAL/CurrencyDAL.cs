﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ozone.DAL
{
   public class CurrencyDAL
    {
       public List<CurrencyTab> getlist()
       {
           using (var db = new OzoneDBEntities())
           {
               return db.CurrencyTabs.ToList();
               
           }
       }
       public CurrencyTab getbyid(long currencyid)
       {
           using (var db = new OzoneDBEntities())
           {
               return db.CurrencyTabs.SingleOrDefault(d => d.Currencyid == currencyid);
               
           }
       }
    }
}
