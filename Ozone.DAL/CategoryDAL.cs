﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ozone.DAL
{
    public class CategoryDAL
    {
         public CategoryDAL()
        {

        }

        public CategoryDAL(long id)
        {
            using (var dbContext = new OzoneDBEntities())
            {
                CATEGORY_MASTER coutnry = dbContext.CATEGORY_MASTER.SingleOrDefault(c => c.Pk_Category_Id == id && c.Is_Deleted == false);
            }
        }

        public string InsertCategory(CATEGORY_MASTER Category)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                dbCon.CATEGORY_MASTER.Add(Category);
                dbCon.SaveChanges();
                return "Success";
            }
        }

        public List<CATEGORY_MASTER> ListCategory(int display, int lenght,string search)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                dbCon.Configuration.ProxyCreationEnabled = false;
                if (search!="")
                {
                    return dbCon.CATEGORY_MASTER.Where(c => c.Is_Deleted == false && c.Category_Description.Contains(search) || c.Category_Name.Contains(search) ).OrderBy(d => d.Created_Date).Skip(display).Take(lenght).ToList();    
                }
                else
                {
                    return dbCon.CATEGORY_MASTER.Where(c => c.Is_Deleted == false).OrderBy(d => d.Created_Date).Skip(display).Take(lenght).ToList();
                }
                
            }
        }

        public string EditCategory(CATEGORY_MASTER Category)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                CATEGORY_MASTER dalCategory = dbCon.CATEGORY_MASTER.SingleOrDefault(c => c.Pk_Category_Id == Category.Pk_Category_Id);
                dalCategory.Category_Name = Category.Category_Name;
                dalCategory.Category_Description = Category.Category_Description;
                 
                dalCategory.Modified_Date = System.DateTime.Now;
                dbCon.SaveChanges();
                return "Updated";
            }
        }

        public string DeleteCategory(long id)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                CATEGORY_MASTER dalCategory = dbCon.CATEGORY_MASTER.SingleOrDefault(c => c.Pk_Category_Id == id);
                dalCategory.Is_Deleted = true;
                dalCategory.Modified_Date = System.DateTime.Now;
                dbCon.SaveChanges();
                return "Deleted";
            }
        }

        public List<CATEGORY_MASTER> DDLCategory()
        {
            using (var dbCon = new OzoneDBEntities())
            {
                return dbCon.CATEGORY_MASTER.Where(c => c.Is_Deleted == false).AsEnumerable().Select(c => new CATEGORY_MASTER { Category_Name = c.Category_Name, Pk_Category_Id = c.Pk_Category_Id }).ToList();
            }
        }

        public CATEGORY_MASTER GetCategoryById(long id)
        {
            using (var dbContext = new OzoneDBEntities())
            {
                return dbContext.CATEGORY_MASTER.SingleOrDefault(c => c.Pk_Category_Id == id && c.Is_Deleted == false);
            }
        }

        public int GetCategoryCount(string search)
        {
            using (var dbcon = new OzoneDBEntities())
            {
                if (search!="")
                {
                    return dbcon.CATEGORY_MASTER.Where(c => c.Is_Deleted == false && c.Category_Description.Contains(search) || c.Category_Name.Contains(search) ).Count();
                    
                }
                else
                {
                    return dbcon.CATEGORY_MASTER.Where(c => c.Is_Deleted == false).Count();
                }
                

            }
        }

    }
}
