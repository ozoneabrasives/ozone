﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;


namespace Ozone.DAL
{
    public class PurchaseOrderDAL
    {
        public PurchaseOrderDAL()
        {

        }

        public PurchaseOrderDAL(long id)
        {
            using (var dbContext = new OzoneDBEntities())
            {
                PURCHASE_ORDER coutnry = dbContext.PURCHASE_ORDER.SingleOrDefault(c => c.Pk_PurchaseOrder_Id == id && c.Is_Deleted == false);
            }
        }

        public string InsertPO(PURCHASE_ORDER PO)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                dbCon.PURCHASE_ORDER.Add(PO);
                dbCon.SaveChanges();
                return "Success";
            }
        }

        public List<PURCHASE_ORDER> ListPO(int display, int lenght,long id,string type,string startdate,string enddate,int vid,string ponumber)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                dbCon.Configuration.ProxyCreationEnabled = false;
                if (type == "Admin" || type == "Vendor")
                {
                    DateTime stdt = startdate==""?System.DateTime.Now: Convert.ToDateTime(startdate);
                    DateTime enddt = enddate == "" ? System.DateTime.Now : Convert.ToDateTime(enddate);

                    var query = dbCon.PURCHASE_ORDER.Include(c => c.USER_REGISTRATION).Where(c=>
                        c.Is_Deleted==false && (ponumber!=""?c.PO_Number==ponumber:c.PO_Number==c.PO_Number)&&

                        (startdate!=""?EntityFunctions.TruncateTime(c.Created_Date.Value)>stdt: c.Created_Date==c.Created_Date)
                        && (enddate!=""? EntityFunctions.TruncateTime(c.Created_Date.Value)<enddt: c.Created_Date==c.Created_Date)&& (vid>0?c.USER_REGISTRATION.Pk_Register_Id==vid: true)
                        ).OrderByDescending(d => d.Created_Date).Skip(display).Take(lenght).ToList();
                    return query;
                    
                    //if (startdate != "")
                    //{
                    //    DateTime dt = Convert.ToDateTime(startdate);
                    //    query.Concat( query.Where(d=>  EntityFunctions.TruncateTime(d.Created_Date.Value) > dt));
                    //}
                    //if (enddate != "")
                    //{
                    //    DateTime dt = Convert.ToDateTime(enddate);
                    //    query.Where(d => EntityFunctions.TruncateTime(d.Created_Date.Value) < dt);

                    //}
                    //if (vid > 0)
                    //{
                    //    query.Concat(query.Where( d => d.USER_REGISTRATION.Pk_Register_Id == vid));
                    //}
                    //if (ponumber != "")
                    //{
                    //    query.Where(d=>d.PO_Number==ponumber);
                    //}
                    //return query.Where(c => c.Is_Deleted == false).OrderByDescending(d => d.Created_Date).Skip(display).Take(lenght).ToList();
                }
                else
                {
                    return dbCon.PURCHASE_ORDER.Where(c => c.Is_Deleted == false && c.Created_by == id).OrderByDescending(d => d.Created_Date).Skip(display).Take(lenght).ToList();
                }
            }
        }

        public string EditPO(PURCHASE_ORDER PO)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                PURCHASE_ORDER dalPO = dbCon.PURCHASE_ORDER.SingleOrDefault(c => c.Pk_PurchaseOrder_Id == PO.Pk_PurchaseOrder_Id);
                dalPO.Checked_By = PO.Checked_By;
                dalPO.Document_Req = PO.Document_Req;
                dalPO.Grand_Total = PO.Grand_Total;
                dalPO.Payment_Terms = PO.Payment_Terms;
                dalPO.PO_Date = PO.PO_Date;
                dalPO.PO_Number = PO.PO_Number;
                dalPO.Ship_Address = PO.Ship_Address;
                dalPO.Ship_City = PO.Ship_City;
                dalPO.Ship_Compny = PO.Ship_Compny;
                dalPO.Ship_Name = PO.Ship_Name;
                dalPO.Ship_Phone = PO.Ship_Phone;
                dalPO.Ship_State = PO.Ship_State;
                dalPO.Ship_Country = PO.Ship_Country;
                dalPO.Ship_Zip_code = PO.Ship_Zip_code;
                dalPO.Shipment_Detail = PO.Shipment_Detail;
                dalPO.Transport = PO.Transport;
                dalPO.Modified_Date = System.DateTime.Now;
                dalPO.Fk_Vendor_Id = PO.Fk_Vendor_Id;
                dalPO.DeliveryTerms = PO.DeliveryTerms;
                dalPO.Note = PO.Note;
                dbCon.SaveChanges();
                return "Updated";
            }
        }

        public string DeletePO(long id)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                PURCHASE_ORDER dalPO = dbCon.PURCHASE_ORDER.SingleOrDefault(c => c.Pk_PurchaseOrder_Id == id);
                dalPO.Is_Deleted = true;
                dalPO.Modified_Date = System.DateTime.Now;
                var varlist = dbCon.INVENTORies.Where(d => d.Fk_PO_Id == dalPO.Pk_PurchaseOrder_Id).ToList();
                foreach (var item in varlist)
                {
                    item.Is_Deleted = true;
                    
                }
                dbCon.SaveChanges();

                return "Deleted";
            }
        }


        public PURCHASE_ORDER GetPOById(long id)
        {
            using (var dbContext = new OzoneDBEntities())
            {
                return dbContext.PURCHASE_ORDER.SingleOrDefault(c => c.Pk_PurchaseOrder_Id == id && c.Is_Deleted == false);
            }
        }

        public int GetPOCount(long id, string type, string startdate, string enddate, int vid, string ponumber)
        {
            using (var dbcon = new OzoneDBEntities())
            {
                if (type == "Admin" || type == "Vendor")
                {
                    DateTime stdt = startdate == "" ? System.DateTime.Now : Convert.ToDateTime(startdate);
                    DateTime enddt = enddate == "" ? System.DateTime.Now : Convert.ToDateTime(enddate);

                    return  dbcon.PURCHASE_ORDER.Include(c => c.USER_REGISTRATION).Where(c =>
                        c.Is_Deleted == false && (ponumber != "" ? c.PO_Number == ponumber : c.PO_Number == c.PO_Number) &&
                        (startdate != "" ? EntityFunctions.TruncateTime(c.Created_Date.Value) > stdt : c.Created_Date == c.Created_Date)
                        && (enddate != "" ? EntityFunctions.TruncateTime(c.Created_Date.Value) < enddt : c.Created_Date == c.Created_Date) && (vid > 0 ? c.USER_REGISTRATION.Pk_Register_Id == vid : true)
                        ).Count();
                }
                else
                {
                    return dbcon.PURCHASE_ORDER.Where(c => c.Is_Deleted == false && c.Created_by == id).Count();
                }
            }
        }

        public List<PURCHASEORDER_ITEMS> UpdateStatus(List<PURCHASEORDER_ITEMS> lst)
        {
            try
            {
                List<PURCHASEORDER_ITEMS> lstpur = new List<PURCHASEORDER_ITEMS>();
                using (var dbCon = new OzoneDBEntities())
                {
                   long? id = null;
                   string grnnumber=GenerateGRN();
                    foreach (var item in lst)
                    {
                        PURCHASEORDER_ITEMS poItem = dbCon.PURCHASEORDER_ITEMS.SingleOrDefault(d => d.Pk_PO_Item_Id == item.Pk_PO_Item_Id);
                        item.GRN = grnnumber;
                        item.Created_by = poItem.Created_by;
                        item.Created_Date = poItem.Created_Date;
                        item.Fk_Package_Id = poItem.Fk_Package_Id;
                        item.Fk_PO_Id = poItem.Fk_PO_Id;
                        item.Fk_Product_Id = poItem.Fk_Product_Id;
                        item.Is_Deleted = poItem.Is_Deleted;
                        item.Modified_By = poItem.Modified_By;
                        item.Modified_Date = poItem.Modified_Date;
                        item.Packaging = poItem.Packaging;
                        item.Pk_PO_Item_Id = poItem.Pk_PO_Item_Id;
                        item.Product_Code = poItem.Product_Code;
                        item.Product_Description = poItem.Product_Description;
                        item.ReceivedDate = poItem.ReceivedDate;
                        item.Total_Amt = poItem.Total_Amt;
                        item.Unit_Price = poItem.Unit_Price;
                        item.Units = poItem.Units;
                        item.CurrencyCode = item.CurrencyCode;
                        item.Total_Qty = item.Total_Qty;
                        lstpur.Add(item);
                        id = poItem.Fk_PO_Id;
                        INVENTORY inv = new INVENTORY();
                        if (item.CurrencyId >0)
                        {
                            inv = new INVENTORY();
                            inv.Account_Type = 2;
                            inv.Fk_PO_Id = poItem.Fk_PO_Id;
                            inv.Created_By = item.Created_by;
                            inv.Created_Date = System.DateTime.Now;
                            inv.Fk_Product_Id = item.Fk_Product_Id;
                            inv.Product_Qty = item.CurrencyId;
                            inv.Order_Type = "PurchaseOrder";
                            inv.Package = poItem.Packaging;
                            inv.Units = poItem.Units;
                            inv.Fk_Package_Id = poItem.Fk_Package_Id;
                            inv.Is_Deleted = false;
                            dbCon.INVENTORies.Add(inv);
                            //dbCon.SaveChanges();
                        }
                        else
                        {
                            item.CurrencyId = 0;
                        }
                        inv = new INVENTORY();
                        inv.Account_Type = 0;
                        inv.Created_By = item.Created_by;
                        inv.Created_Date = System.DateTime.Now;                      
                        inv.Fk_Product_Id = item.Fk_Product_Id;
                        inv.Fk_PO_Id = poItem.Fk_PO_Id;
                        inv.Product_Qty = item.Total_Qty ;
                        inv.Order_Type = "PurchaseOrder";
                        inv.Package = poItem.Packaging;
                        inv.Units = poItem.Units;
                        inv.Fk_Package_Id = poItem.Fk_Package_Id;
                        inv.Is_Deleted = false;
                        inv.GRNNo = grnnumber;
                       
                        dbCon.INVENTORies.Add(inv);
                        //dbCon.SaveChanges();

                       
                    }
                    dbCon.SaveChanges();
                    PURCHASE_ORDER dalPO = dbCon.PURCHASE_ORDER.SingleOrDefault(c => c.Pk_PurchaseOrder_Id == id);
                    bool ispartial=false;
                   var totalitems =dbCon.PURCHASEORDER_ITEMS.Where(d=>d.Fk_PO_Id==id).ToList();
                    foreach (var item in totalitems)
	{
		 var count = dbCon.INVENTORies.Where(d=>d.Fk_PO_Id==item.Fk_PO_Id && d.Fk_Product_Id==item.Fk_Product_Id && d.Fk_Package_Id==item.Fk_Package_Id).Sum(d=>d.Product_Qty);
if(count==item.Total_Qty){
    ispartial=false;
}
else{
    ispartial=true;
}

	}
                    if(ispartial){
                         dalPO.PO_Status = "Partially Received";
                    }
                    else{
                         dalPO.PO_Status = "Received";
                    }
                   
                    dalPO.Modified_Date = System.DateTime.Now;
                    dbCon.SaveChanges();
                    return lstpur;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       

        public string UpdatePOStatus(string number,string status)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                PURCHASE_ORDER po = dbCon.PURCHASE_ORDER.SingleOrDefault(c => c.PO_Number == number);
                po.PO_Status = status;
                po.Modified_Date = System.DateTime.Now;
                dbCon.SaveChanges();
                return "Status updated successfully.";
            }
        }
        public string GenerateGRN()
        {
            try
            {
                long num = Convert.ToInt64(new InvenortyDAL().GetLastGRN().Substring(7)) + 1;

                return "GRN" + DateTime.Now.Year + num.ToString("D" + 5);
            }
            
            catch(Exception e){
                return "GRN" + DateTime.Now.Year + "00001";
            }

        }
        public static long GetLastId()
        {
            try
            {
                using (var db = new OzoneDBEntities())
                {
                    return db.PURCHASE_ORDER.Max(c => c.Pk_PurchaseOrder_Id) + 1;
                }
            }
            catch (Exception)
            {
                return 1;
            }
            
        }
        public List<PURCHASEORDER_ITEMS> ListGRN(int display, int lenght, long id)
        {
            List<PURCHASEORDER_ITEMS> lstuitems = new List<PURCHASEORDER_ITEMS>();
            using (var db = new OzoneDBEntities())
            {
                db.Configuration.ProxyCreationEnabled = false;
               // var lstitems = db.INVENTORies.Where(d => d.Fk_PO_Id == id && d.Account_Type==0).Include(d=>d.PRODUCT_DETAILS).Include(d=>d.PACKAGE_MASTER).OrderBy(d=>d.Created_Date) .Skip(display).Take(lenght).Select(d=>d new INVENTORY { Fk_Product_Id= ;
                var lstitems = (from u in db.INVENTORies.Include(u => u.PRODUCT_DETAILS).Include(d=>d.PACKAGE_MASTER).AsEnumerable()
                               where u.Fk_PO_Id ==  id && u.Account_Type==0
                               select new 
                               {
                                  Fk_Product_Id= u.Fk_Product_Id,
                                  GRNNo= u.GRNNo,
                                  Product_Qty= u.Product_Qty,
                                  ProductName = u.PRODUCT_DETAILS.Product_Title,
                                  PackageName=u.PACKAGE_MASTER.Package_Title,
                                  Created_Date =u.Created_Date
                               }).OrderBy(d=>d.Fk_Product_Id).Skip(display).Take(lenght).ToList();
                //lstuitems = (from td in db.INVENTORies.Include(d => d.PRODUCT_DETAILS).Include(d => d.PACKAGE_MASTER).Select(d => new { d. }).AsEnumerable() join td2 in db.PURCHASE_ORDER.Include(d => d.USER_REGISTRATION).AsEnumerable() on td.Fk_PO_Id equals td2.Pk_PurchaseOrder_Id where td.Fk_PO_Id == id && td.Account_Type == 0 select new PURCHASEORDER_ITEMS { Fk_PO_Id = id, Fk_Product_Id = td.Fk_Product_Id.Value, GRN = td.GRNNo, Total_Qty = td.Product_Qty.Value, PRODUCT_DETAILS = td.PRODUCT_DETAILS, PACKAGE_MASTER = td.PACKAGE_MASTER, Created_Date = td.Created_Date, PURCHASE_ORDER = td2 }).Skip(display).Take(lenght).ToList();
                 
                foreach (var item in lstitems)
                {
                    var purchaseorder = db.PURCHASE_ORDER.Where(d=>d.Pk_PurchaseOrder_Id==id).Include(d=>d.USER_REGISTRATION).FirstOrDefault();

                    lstuitems.Add(new PURCHASEORDER_ITEMS { Fk_PO_Id = id, Fk_Product_Id = item.Fk_Product_Id.Value, GRN = item.GRNNo, Total_Qty = item.Product_Qty.Value, PRODUCT_DETAILS = new PRODUCT_DETAILS { Product_Title = item.ProductName }, PACKAGE_MASTER = new PACKAGE_MASTER { Package_Title= item.PackageName }, Created_Date = item.Created_Date, PURCHASE_ORDER = purchaseorder });

                }
                
                
                
            }
            return lstuitems;
        }

        public int GetGRNCount(long id)
        {
            int count;
            using (var db= new OzoneDBEntities())
            {
                count = db.INVENTORies.Where(d => d.Fk_PO_Id == id && d.Account_Type == 0).Count();

                
            }
            return count;
        }
    }
}
