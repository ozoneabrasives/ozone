﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Ozone.DAL
{
    public class ShoppingCartDAL
    {
        public ShoppingCartDAL()
        {

        }

        public ShoppingCartDAL(long id)
        {
            using (var dbContext = new OzoneDBEntities())
            {
                SHOPPING_CART coutnry = dbContext.SHOPPING_CART.SingleOrDefault(c => c.Pk_UserCart_Id == id && c.Is_Deleted == false);
            }
        }

        public string InsertCart(SHOPPING_CART cart)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                dbCon.SHOPPING_CART.Add(cart);
                dbCon.SaveChanges();
                return "Success";
            }
        }

        public static string UpdateCart(SHOPPING_CART cart)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                SHOPPING_CART dalCart = dbCon.SHOPPING_CART.SingleOrDefault(c => c.Pk_UserCart_Id == cart.Pk_UserCart_Id);
                dalCart.Modified_By = cart.Create_By;
                dalCart.Card_Cvv = cart.Card_Cvv;
                dalCart.Card_Detail = cart.Card_Detail;
                dalCart.Card_Exp_Month = cart.Card_Exp_Month;
                dalCart.Card_Exp_Year = cart.Card_Exp_Year;
                dalCart.Card_No = cart.Card_No;
                dalCart.Del_Address = cart.Del_Address;
                dalCart.Del_City = cart.Del_City;
                dalCart.Del_Country = cart.Del_Country;
                dalCart.Del_State = cart.Del_State;
                dalCart.Del_Zipcode = cart.Del_Zipcode;
                dalCart.Full_Name = cart.Full_Name;
                dalCart.Is_Deleted = false;
                dalCart.Name_On_Card = cart.Name_On_Card;
                dalCart.Order_Status = cart.Order_Status;
                dalCart.Modified_Date = cart.Modified_Date;
                dalCart.Payment_Amt = cart.Payment_Amt;
                dalCart.Payment_Card = cart.Payment_Card;
                dalCart.Payment_Status = "Pending";
                dalCart.Phone_No = cart.Phone_No;
                // dalCart.Order_Number = cart.Order_Number;
                dbCon.SaveChanges();
                return "Updated";
            }
        }

        public List<SHOPPING_CART> ListCart(int display, int lenght, string userType, long id,string status)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                dbCon.Configuration.ProxyCreationEnabled = false;
                if (userType == "Admin")
                {
                    if (status != "")
                        return dbCon.SHOPPING_CART.Where(c => c.Is_Deleted == false && c.Order_Status != "Extra" && c.Payment_Status == status).Include(c=> c.USER_REGISTRATION).Include(c => c.SHOPPING_CART_ITEMS).OrderByDescending(d => d.Create_Date).Skip(display).Take(lenght).ToList();
                    else
                        return dbCon.SHOPPING_CART.Where(c => c.Is_Deleted == false && c.Order_Status != "Extra").Include(c => c.SHOPPING_CART_ITEMS).Include(c=> c.USER_REGISTRATION).OrderByDescending(d => d.Create_Date).Skip(display).Take(lenght).ToList();
                }
                else
                {
                    return dbCon.SHOPPING_CART.Where(c => c.Is_Deleted == false && c.Create_By == id && c.Order_Status != "Extra").Include(c => c.SHOPPING_CART_ITEMS).OrderByDescending(d => d.Create_Date).Skip(display).Take(lenght).ToList();
                }
            }
        }

        public List<SHOPPING_CART> ListExtraCart(int display, int lenght, string userType, long id)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                dbCon.Configuration.ProxyCreationEnabled = false;
                if (userType == "Admin")
                {
                    return dbCon.SHOPPING_CART.Where(c => c.Is_Deleted == false && c.Order_Status == "Extra").Include(c => c.SHOPPING_CART_ITEMS).OrderByDescending(d => d.Create_Date).Skip(display).Take(lenght).ToList();
                }
                else
                {
                    return dbCon.SHOPPING_CART.Where(c => c.Is_Deleted == false && c.Create_By == id && c.Order_Status == "Extra").Include(c => c.SHOPPING_CART_ITEMS).OrderByDescending(d => d.Create_Date).Skip(display).Take(lenght).ToList();
                }
            }
        }

        public SHOPPING_CART GetById(long id)
        {
            using (var dbContext = new OzoneDBEntities())
            {
                dbContext.Configuration.ProxyCreationEnabled = false;
                return dbContext.SHOPPING_CART.Include(d => d.COUNTRY_MASTER).Include(d => d.STATE_MASTER).Include(d => d.SHOPPING_CART_ITEMS).Include(d=>d.USER_REGISTRATION) .SingleOrDefault(c => c.Pk_UserCart_Id == id && c.Is_Deleted == false);
            }
        }

        public int GetProductCount(string userType, long id, string status)
        {
            using (var dbcon = new OzoneDBEntities())
            {
                if (userType == "Admin")
                {
                    if (status != "")
                        return dbcon.SHOPPING_CART.Where(c => c.Is_Deleted == false && c.Order_Status != "Extra" && c.Payment_Status == status).Count();
                    else
                        return dbcon.SHOPPING_CART.Where(c => c.Is_Deleted == false && c.Order_Status != "Extra").Count();
                }
                else
                {
                    return dbcon.SHOPPING_CART.Where(c => c.Is_Deleted == false && c.Create_By == id && c.Order_Status != "Extra").Count();
                }
            }
        }

        public string UpdateStatus(string number, string status, string notes, string pay)
        {
            try
            {
                string result = "Stauts Updated Successfully";
                using (var dbCon = new OzoneDBEntities())
                {
                    SHOPPING_CART dalPO = dbCon.SHOPPING_CART.SingleOrDefault(c => c.Order_Number == number);
                    dalPO.Order_Status = status;
                    dalPO.Order_Note = notes;
                    if (status == "Accepted")
                    {
                        dalPO.Accepted_Note = notes;
                    }
                    else if (status == "Delivered")
                    {
                        dalPO.Delivery_Note = notes;
                    }
                    else if (status == "Processed")
                    {
                        dalPO.Processed_Note = notes;
                    }
                    else if (status == "Rejected")
                    {
                        dalPO.Rejected_Note = notes;
                    }
                    else if (status == "Shipped")
                    {
                        dalPO.Shipped_Note = notes;
                    }
                    if (status == "Rejected")
                    {
                        dalPO.Payment_Status = "Cancelled";
                    }
                    else
                    {
                        dalPO.Payment_Status = pay;
                    }
                    dbCon.SaveChanges();
                    if (status == "Processed")
                    {
                        //foreach (var item in dalPO.SHOPPING_CART_ITEMS)
                        //{
                        //    INVENTORY inv = new INVENTORY();
                        //    inv.Account_Type = 1;
                        //    inv.Created_By = dalPO.Create_By;
                        //    inv.Created_Date = System.DateTime.Now;
                        //    inv.Fk_PO_Id = dalPO.Pk_UserCart_Id;
                        //    inv.Fk_Product_Id = item.Fk_Product_Id;
                        //    inv.Fk_Package_Id = item.Fk_Package_Id;
                        //    inv.Product_Qty = item.Quantity;
                        //    inv.Order_Type = "SalesOrder";
                        //    inv.Package = item.PACKAGE_MASTER.Package_Title;
                        //    inv.Units = item.PACKAGE_MASTER.Package_Unit;
                        //    inv.Is_Deleted = false;
                        //    dbCon.INVENTORies.Add(inv);
                        //    dbCon.SaveChanges();
                        //}
                        result = "Process";
                    }
                  
                }
                return result;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public List<SHOPPING_CART> ListCreditCart(int display, int lenght, string userType, long id)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                dbCon.Configuration.ProxyCreationEnabled = false;
                if (id == 0)
                {
                    return dbCon.SHOPPING_CART.Where(c => c.Is_Deleted == false && c.Payment_Status == "Credited").Include(c => c.SHOPPING_CART_ITEMS).OrderByDescending(d => d.Create_Date).Skip(display).Take(lenght).ToList();
                }
                else
                {
                    return dbCon.SHOPPING_CART.Where(c => c.Is_Deleted == false && c.Create_By == id && c.Payment_Status == "Credited").Include(c => c.SHOPPING_CART_ITEMS).OrderByDescending(d => d.Create_Date).Skip(display).Take(lenght).ToList();
                }
            }
        }

        public static long GetLastId()
        {
            try
            {
                using (var db = new OzoneDBEntities())
                {
                    return db.SHOPPING_CART.Where(d => d.Is_Deleted == false).Max(c => c.Pk_UserCart_Id)+1;
                }
            }
            catch (Exception)
            {
                return 1;
            }
            
        }

        public List<SHOPPING_CART> GetSummary(string srch, long? ByCNo, long? ByClient, string ByStatus)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                dbCon.Configuration.ProxyCreationEnabled = false;

                List<SHOPPING_CART> lst = new List<SHOPPING_CART>();
                string query = "";
                if (ByStatus == "" && srch=="" && ByCNo == null && ByClient == null )
                {
                    lst = dbCon.SHOPPING_CART.Include(c => c.USER_REGISTRATION).ToList();
                }
                if (ByCNo != null)
                {
                    //if (ByClient != null)
                    //    lst = dbCon.SHOPPING_CART.Include(c => c.USER_REGISTRATION).Where(c => ((c.USER_REGISTRATION.Pk_Register_Id == ByCNo) && (c.USER_REGISTRATION.Pk_Register_Id == ByClient)) && (c.Payment_Status.Contains(srch) || c.USER_REGISTRATION.Company_Name.Contains(srch) || c.USER_REGISTRATION.Customer_ID.Contains(srch))).ToList();
                    //else if(ByStatus!="")
                    //    lst = dbCon.SHOPPING_CART.Include(c => c.USER_REGISTRATION).Where(c => ((c.USER_REGISTRATION.Pk_Register_Id == ByCNo) && (c.Payment_Status == ByStatus)) && (c.Payment_Status.Contains(srch) || c.USER_REGISTRATION.Company_Name.Contains(srch) || c.USER_REGISTRATION.Customer_ID.Contains(srch))).ToList();
                    //else if(ByClient!=null && ByStatus!="")
                    //    lst = dbCon.SHOPPING_CART.Include(c => c.USER_REGISTRATION).Where(c => ((c.USER_REGISTRATION.Pk_Register_Id == ByCNo) && (c.USER_REGISTRATION.Pk_Register_Id == ByClient) && (c.Payment_Status == ByStatus)) && (c.Payment_Status.Contains(srch) || c.USER_REGISTRATION.Company_Name.Contains(srch) || c.USER_REGISTRATION.Customer_ID.Contains(srch))).ToList();
                    //else
                        lst = dbCon.SHOPPING_CART.Include(c => c.USER_REGISTRATION).Where(c => ((c.USER_REGISTRATION.Pk_Register_Id == ByCNo)) && (c.Payment_Status.Contains(srch) || c.USER_REGISTRATION.Company_Name.Contains(srch) || c.USER_REGISTRATION.Customer_ID.Contains(srch))).ToList();
                }
                if (ByClient != null)
                {
                    if (ByCNo != null)
                        lst = dbCon.SHOPPING_CART.Include(c => c.USER_REGISTRATION).Where(c => (c.USER_REGISTRATION.Pk_Register_Id == ByCNo) && (c.USER_REGISTRATION.Pk_Register_Id == ByClient) && (c.Payment_Status.Contains(srch) || c.USER_REGISTRATION.Company_Name.Contains(srch) || c.USER_REGISTRATION.Customer_ID.Contains(srch))).ToList();
                    //else if (ByStatus != "")
                    //    lst = dbCon.SHOPPING_CART.Include(c => c.USER_REGISTRATION).Where(c => ((c.USER_REGISTRATION.Pk_Register_Id == ByClient) && (c.Payment_Status == ByStatus)) && (c.Payment_Status.Contains(srch) || c.USER_REGISTRATION.Company_Name.Contains(srch) || c.USER_REGISTRATION.Customer_ID.Contains(srch))).ToList();
                    //else if (ByCNo != null && ByStatus != "")
                    //    lst = dbCon.SHOPPING_CART.Include(c => c.USER_REGISTRATION).Where(c => ((c.USER_REGISTRATION.Pk_Register_Id == ByCNo) && (c.USER_REGISTRATION.Pk_Register_Id == ByClient) && (c.Payment_Status == ByStatus)) && (c.Payment_Status.Contains(srch) || c.USER_REGISTRATION.Company_Name.Contains(srch) || c.USER_REGISTRATION.Customer_ID.Contains(srch))).ToList();
                    else
                        lst = dbCon.SHOPPING_CART.Include(c => c.USER_REGISTRATION).Where(c => (c.USER_REGISTRATION.Pk_Register_Id == ByClient) && (c.Payment_Status.Contains(srch) || c.USER_REGISTRATION.Company_Name.Contains(srch) || c.USER_REGISTRATION.Customer_ID.Contains(srch))).ToList();

                }
                if (ByStatus != "")
                {
                    if (ByCNo != null)
                        lst = dbCon.SHOPPING_CART.Include(c => c.USER_REGISTRATION).Where(c => (c.Payment_Status == ByStatus) && (c.USER_REGISTRATION.Pk_Register_Id == ByCNo) && (c.Payment_Status.Contains(srch) || c.USER_REGISTRATION.Company_Name.Contains(srch) || c.USER_REGISTRATION.Customer_ID.Contains(srch))).ToList();
                    else if (ByClient != null)
                        lst = dbCon.SHOPPING_CART.Include(c => c.USER_REGISTRATION).Where(c => (c.Payment_Status == ByStatus) && (c.USER_REGISTRATION.Pk_Register_Id == ByClient) && (c.Payment_Status.Contains(srch) || c.USER_REGISTRATION.Company_Name.Contains(srch) || c.USER_REGISTRATION.Customer_ID.Contains(srch))).ToList();
                    else if (ByCNo != null && ByClient != null)
                        lst = dbCon.SHOPPING_CART.Include(c => c.USER_REGISTRATION).Where(c => ((c.USER_REGISTRATION.Pk_Register_Id == ByCNo) && (c.USER_REGISTRATION.Pk_Register_Id == ByClient) && (c.Payment_Status == ByStatus)) && (c.Payment_Status.Contains(srch) || c.USER_REGISTRATION.Company_Name.Contains(srch) || c.USER_REGISTRATION.Customer_ID.Contains(srch))).ToList();
                    else
                        lst = dbCon.SHOPPING_CART.Include(c => c.USER_REGISTRATION).Where(c => (c.Payment_Status == ByStatus) && (c.Payment_Status.Contains(srch) || c.USER_REGISTRATION.Company_Name.Contains(srch) || c.USER_REGISTRATION.Customer_ID.Contains(srch))).ToList();
                }
                else if(srch !="")
                {
                    lst = dbCon.SHOPPING_CART.Include(c => c.USER_REGISTRATION).Where(c => (c.Payment_Status.Contains(srch) || c.USER_REGISTRATION.Company_Name.Contains(srch) || c.USER_REGISTRATION.Customer_ID.Contains(srch))).ToList();
                }
                return lst.AsEnumerable().GroupBy(o => new { o.Create_By, o.Payment_Status })
                       .Select(g => new SHOPPING_CART() { CustomerId = dbCon.USER_REGISTRATION.SingleOrDefault(d => d.Pk_Register_Id == g.Key.Create_By).Customer_ID, Full_Name = dbCon.USER_REGISTRATION.SingleOrDefault(d => d.Pk_Register_Id == g.Key.Create_By).Company_Name, Payment_Status = g.Key.Payment_Status, Payment_Amt = g.Sum(i => i.Payment_Amt) }).OrderBy(c => c.Full_Name).ToList();
                //return lst;
            }
        }

        public int GetSummaryCount(string srch, long? ByCNo, long? ByClient, string ByStatus)
        {
            using (var db = new OzoneDBEntities())
            {
                return db.SHOPPING_CART.Include(c => c.USER_REGISTRATION).Where(c => c.Payment_Status.Contains(srch) || c.USER_REGISTRATION.Company_Name.Contains(srch) || c.USER_REGISTRATION.Customer_ID.Contains(srch)).AsEnumerable().GroupBy(o => new { o.Create_By, o.Payment_Status }).Count();
            }
        }

        public int GetExtraCount(string userType, long id)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                dbCon.Configuration.ProxyCreationEnabled = false;
                if (userType == "Admin")
                {
                    return dbCon.SHOPPING_CART.Where(c => c.Is_Deleted == false && c.Order_Status == "Extra").Count();
                }
                else
                {
                    return dbCon.SHOPPING_CART.Where(c => c.Is_Deleted == false && c.Create_By == id && c.Order_Status == "Extra").Count();
                }
            }
        }

        public int GetCreditCount(string userType, long id)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                dbCon.Configuration.ProxyCreationEnabled = false;
                if (id == 0)
                {
                    return dbCon.SHOPPING_CART.Where(c => c.Is_Deleted == false && c.Payment_Status == "Credited").Count();
                }
                else
                {
                    return dbCon.SHOPPING_CART.Where(c => c.Is_Deleted == false && c.Create_By == id && c.Payment_Status == "Credited").Count();
                }
            }
        }
        public List<SHOPPING_CART> getListByPaymentStatus()
        {
            using (var dbcon = new OzoneDBEntities())
            {
                var lst = (from td in dbcon.SHOPPING_CART group td by td.Payment_Status into g select new  { Payment_Status = g.Key, Payment_Amt = g.Sum(t => t.Payment_Amt) }).ToList().Select(d=> new SHOPPING_CART() { Payment_Status= d.Payment_Status, Payment_Amt=d.Payment_Amt}).ToList(); //dbcon.SHOPPING_CART.OrderBy(d=>d.Payment_Status).ToList().Select(g=> new SHOPPING_CART() { Payment_Status=g.Payment_Status , Payment_Amt=g.s})
                return lst;
            }
        }
        public SHOPPING_CART GetDobyNumber(string deliveryorder)
        {
            SHOPPING_CART objshop = new SHOPPING_CART();
            using (var dbcon = new OzoneDBEntities())
            {
                dbcon.Configuration.ProxyCreationEnabled = false;
                objshop = dbcon.SHOPPING_CART.Where(d => d.Order_Number == deliveryorder).Include(d => d.SHOPPING_CART_ITEMS).Include(d => d.USER_REGISTRATION).Include("SHOPPING_CART_ITEMS.PACKAGE_MASTER").Include("SHOPPING_CART_ITEMS.PRODUCT_DETAILS").FirstOrDefault();
                
            }
            return objshop;
        }
    }
}
