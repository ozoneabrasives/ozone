﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Ozone.DAL
{
    public class POItemsDAL
    {
        public POItemsDAL()
        {

        }

        public POItemsDAL(long id)
        {
            using (var dbContext = new OzoneDBEntities())
            {
                PURCHASEORDER_ITEMS coutnry = dbContext.PURCHASEORDER_ITEMS.SingleOrDefault(c => c.Pk_PO_Item_Id == id && c.Is_Deleted == false);
            }
        }

        public string InsertPOItem(PURCHASEORDER_ITEMS POItem)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                
                dbCon.PURCHASEORDER_ITEMS.Add(POItem);
                dbCon.SaveChanges();
                return "Success";
            }
        }

        public string InsertPOItem(List< PURCHASEORDER_ITEMS> lstPOItem)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                foreach (var item in lstPOItem)
                {
                    dbCon.PURCHASEORDER_ITEMS.Add(item);
                    dbCon.SaveChanges();
                }
               
                return "Success";
            }
        }

        public List<PURCHASEORDER_ITEMS> ListPOItem(int display, int lenght,long poId)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                dbCon.Configuration.ProxyCreationEnabled = false;
                return dbCon.PURCHASEORDER_ITEMS.Include(c => c.PURCHASE_ORDER).Include(c => c.PRODUCT_DETAILS).Where(c => c.Is_Deleted == false && c.Fk_PO_Id == poId).OrderBy(d => d.Created_Date).Skip(display).Take(lenght).ToList();
            }
        }

        public List<PURCHASEORDER_ITEMS> ListItems(int display, int lenght)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                dbCon.Configuration.ProxyCreationEnabled = false;
                return dbCon.PURCHASEORDER_ITEMS.Include(c => c.PURCHASE_ORDER).Include(c => c.PRODUCT_DETAILS).Where(c => c.Is_Deleted == false && c.PURCHASE_ORDER.PO_Status == "Extra").OrderBy(d => d.Created_Date).Skip(display).Take(lenght).ToList();
            }
        }

        public string EditPOItem(PURCHASEORDER_ITEMS POItem)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                PURCHASEORDER_ITEMS dalPOItem = dbCon.PURCHASEORDER_ITEMS.SingleOrDefault(c => c.Pk_PO_Item_Id == POItem.Pk_PO_Item_Id);
                dalPOItem.Fk_PO_Id = POItem.Fk_PO_Id;
                dalPOItem.Fk_Product_Id = POItem.Fk_Product_Id;
                dalPOItem.Product_Description = POItem.Product_Description;
                dalPOItem.Packaging = POItem.Packaging;
                dalPOItem.Product_Code = POItem.Product_Code;
                dalPOItem.Total_Amt = POItem.Total_Amt;
                dalPOItem.Total_Qty = POItem.Total_Qty;
                dalPOItem.Unit_Price = POItem.Unit_Price;
                dalPOItem.Fk_Package_Id = POItem.Fk_Package_Id;
                dalPOItem.Units = POItem.Units;
                dalPOItem.Modified_Date = System.DateTime.Now;
                dbCon.SaveChanges();
                return "Updated";
            }
        }

        public string DeletePOItem(long id)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                PURCHASEORDER_ITEMS dalPOItem = dbCon.PURCHASEORDER_ITEMS.SingleOrDefault(c => c.Pk_PO_Item_Id == id);
                dalPOItem.Is_Deleted = true;
                dalPOItem.Modified_Date = System.DateTime.Now;
                dbCon.SaveChanges();
                return "Deleted";
            }
        }
        
        public PURCHASEORDER_ITEMS GetPOItemById(long id)
        {
            using (var dbContext = new OzoneDBEntities())
            {
                return dbContext.PURCHASEORDER_ITEMS.SingleOrDefault(c => c.Pk_PO_Item_Id == id && c.Is_Deleted == false);
            }
        }

        public int GetPOItemCount()
        {
            using (var dbcon = new OzoneDBEntities())
            {
                return dbcon.PURCHASEORDER_ITEMS.Where(c => c.Is_Deleted == false).Count();

            }
        }

        public int GetExtItemCount()
        {
            using (var dbcon = new OzoneDBEntities())
            {
                return dbcon.PURCHASEORDER_ITEMS.Where(c => c.Is_Deleted == false && c.PURCHASE_ORDER.PO_Status == "Extra").Count();

            }
        }
        public List<PURCHASEORDER_ITEMS> ListPOItemsByNullGRN(long poid)
        {
            List<PURCHASEORDER_ITEMS> lstpoitems = new List<PURCHASEORDER_ITEMS>();
            List<PURCHASEORDER_ITEMS> lstfinallist = new List<PURCHASEORDER_ITEMS>();

            using (var dbCon = new OzoneDBEntities())
            {
                dbCon.Configuration.ProxyCreationEnabled = false;
                 lstpoitems= dbCon.PURCHASEORDER_ITEMS.Include(c => c.PURCHASE_ORDER).Include(c => c.PRODUCT_DETAILS).Where(c => c.Is_Deleted == false && c.Fk_PO_Id == poid ).OrderBy(d => d.Created_Date).ToList();
                foreach (var item in lstpoitems)
                {
                    var inventor = dbCon.INVENTORies.Where(d => d.Fk_PO_Id == item.Fk_PO_Id && d.Fk_Product_Id == item.Fk_Product_Id && d.Fk_Package_Id == item.Fk_Package_Id).Sum(d => d.Product_Qty);
                    if (item.Total_Qty != inventor)
                    {
                        item.Unit_Price = (item.Total_Qty - (inventor==null?0:inventor));
                        lstfinallist.Add(item);
                    }
                }
                
            }
            return lstfinallist;
        }
    }
}
