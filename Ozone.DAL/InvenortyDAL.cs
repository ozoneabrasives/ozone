﻿using Ozone.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Ozone.DAL
{
    public class InvenortyDAL
    {
        public List<InventoryDetailsModel> ListInventory(int display, int lenght)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                dbCon.Configuration.ProxyCreationEnabled = false;
               // List<INVENTORY> lst = dbCon.INVENTORies.AsEnumerable().GroupBy(o => new { o.Fk_Product_Id, o.Account_Type})
                    //.Select(g => new INVENTORY() { Product_Qty= g.Where(c=> c.Account_Type == g.Key.Account_Type).Sum(c=> c.Product_Qty)  }).OrderBy(c => c.Fk_Product_Id).ToList();

                var productlist = dbCon.PACKAGE_MASTER.Include(d=>d.PRODUCT_DETAILS). Where(d => d.Is_Deleted == false).ToList();
                List<InventoryDetailsModel> lstinvlist = new List<InventoryDetailsModel>();

                foreach (var item in productlist)
                {
                    var requqty = (from td in dbCon.PURCHASEORDER_ITEMS where td.Fk_Package_Id == item.Pk_Package_Id && td.PURCHASE_ORDER.PO_Status == "Acknowledge" && td.Is_Deleted == false select td.Total_Qty).Sum();
                    var mins=(from td in dbCon.INVENTORies where td.Fk_Package_Id == item.Pk_Package_Id && td.Account_Type == 1 && td.Is_Deleted==false select td.Product_Qty).Sum();
                    var availableqty = (from td in dbCon.INVENTORies where td.Fk_Package_Id == item.Pk_Package_Id && td.Account_Type == 0 && td.Is_Deleted==false select td.Product_Qty).Sum() - (mins == null ? 0 : mins);
                    var Requestesstock = (from td in dbCon.SHOPPING_CART_ITEMS where td.Fk_Package_Id == item.Pk_Package_Id && td.SHOPPING_CART.Order_Status == "Processed" && td.Is_Deleted == false select td.Quantity).Sum();
                    InventoryDetailsModel objinv = new InventoryDetailsModel();
                    objinv.AvailableStock = availableqty==null?0: availableqty.Value;
                    objinv.OrderQty = requqty==null?0:requqty.Value;
                    objinv.Productid = item.Fk_Product_Id.Value;
                    objinv.Productname = item.PRODUCT_DETAILS.Product_Title + " - " + item.Package_Title ;
                    objinv.RequiredStock = Requestesstock==null?0: Requestesstock.Value;
                    objinv.productcode = item.PRODUCT_DETAILS.Product_Code;


                    lstinvlist.Add(objinv);
                    
                }


                return lstinvlist;
            }
        }

        public List<INVENTORY> ListDeadStock(int display,int length)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                dbCon.Configuration.ProxyCreationEnabled = false;
                return dbCon.INVENTORies.Include(c=> c.PRODUCT_DETAILS).AsEnumerable().Where(c => c.Account_Type == 2 && c.Is_Deleted == false).Select(d => new INVENTORY()
                {
                    Pk_Inventory_Id = d.Pk_Inventory_Id,
                    Package = d.Package,
                    Units = d.Units,
                    Product_Qty = d.Product_Qty,
                   PRODUCT_DETAILS = dbCon.PRODUCT_DETAILS.SingleOrDefault(c=> c.Pk_Product_Id == d.Fk_Product_Id),
                    Order_Type = dbCon.PURCHASE_ORDER.SingleOrDefault(c=> c.Pk_PurchaseOrder_Id == d.Fk_PO_Id).PO_Number
                }).ToList();
            }
        }

        public int CountStock()
        {
            using (var dbCon = new OzoneDBEntities())
            {
                return dbCon.INVENTORies.Where(c => c.Account_Type == 2 && c.Is_Deleted == false).Count();
            }
        }

        public PURCHASEORDER_ITEMS TransferStock(long id,int qty)
        {
            PURCHASEORDER_ITEMS poitemd = new PURCHASEORDER_ITEMS();
            using (var dbCon = new OzoneDBEntities())
            {
                INVENTORY inv = dbCon.INVENTORies.SingleOrDefault(c => c.Pk_Inventory_Id == id);
                var poitm = dbCon.PURCHASEORDER_ITEMS.Where(d => d.Fk_PO_Id == inv.Fk_PO_Id && d.Fk_Package_Id == inv.Fk_Package_Id && d.Fk_Product_Id == inv.Fk_Product_Id).FirstOrDefault();
                if (inv.Product_Qty==qty)
                {
                    inv.Account_Type = 0;
                    inv.GRNNo= new PurchaseOrderDAL().GenerateGRN();
                    inv.Modified_Date = System.DateTime.Now;
                    poitemd.GRN = inv.GRNNo;
                   

                    poitemd.Created_by = poitm.Created_by;
                    poitemd.Created_Date = poitm.Created_Date;
                    poitemd.Fk_Package_Id = poitm.Fk_Package_Id;
                    poitemd.Fk_PO_Id = poitm.Fk_PO_Id;
                    poitemd.Fk_Product_Id = poitm.Fk_Product_Id;
                    poitemd.Is_Deleted = poitm.Is_Deleted;
                    poitemd.Modified_By = poitm.Modified_By;
                    poitemd.Modified_Date = poitm.Modified_Date;
                    poitemd.Packaging = poitm.Packaging;
                    poitemd.Pk_PO_Item_Id = poitm.Pk_PO_Item_Id;
                    poitemd.Product_Code = poitm.Product_Code;
                    poitemd.Product_Description = poitm.Product_Description;
                    poitemd.ReceivedDate = poitm.ReceivedDate;
                    poitemd.Total_Amt = poitm.Total_Amt;
                    poitemd.Unit_Price = poitm.Unit_Price;
                    poitemd.Units = poitm.Units;
                    poitemd.CurrencyCode = new CurrencyDAL().getbyid(poitm.CurrencyId.Value).Symbol;

                    poitemd.Total_Qty = qty;
                    dbCon.SaveChanges();
                    
                }
                else
                {
                    inv.Product_Qty = inv.Product_Qty - qty;
                    inv.Modified_Date = System.DateTime.Now;
                    dbCon.SaveChanges();
                   INVENTORY newinv = new INVENTORY();
                   newinv.Account_Type = 0;
                   newinv.Fk_PO_Id = poitm.Fk_PO_Id;
                   newinv.Created_By = 0;
                   newinv.Created_Date = System.DateTime.Now;
                   newinv.Fk_Product_Id = poitm.Fk_Product_Id;
                   newinv.Product_Qty = qty;
                   newinv.Order_Type = "PurchaseOrder";
                   newinv.Package = poitm.Packaging;
                   newinv.Units = poitm.Units;
                   newinv.Fk_Package_Id = poitm.Fk_Package_Id;
                   newinv.Is_Deleted = false;
                   newinv.GRNNo = new PurchaseOrderDAL().GenerateGRN();
                   dbCon.INVENTORies.Add(newinv);
                    dbCon.SaveChanges();
                    poitemd.Created_by = poitm.Created_by;
                    poitemd.Created_Date = poitm.Created_Date;
                    poitemd.Fk_Package_Id = poitm.Fk_Package_Id;
                    poitemd.Fk_PO_Id = poitm.Fk_PO_Id;
                    poitemd.Fk_Product_Id = poitm.Fk_Product_Id;
                    poitemd.Is_Deleted = poitm.Is_Deleted;
                    poitemd.Modified_By = poitm.Modified_By;
                    poitemd.Modified_Date = poitm.Modified_Date;
                    poitemd.Packaging = poitm.Packaging;
                    poitemd.Pk_PO_Item_Id = poitm.Pk_PO_Item_Id;
                    poitemd.Product_Code = poitm.Product_Code;
                    poitemd.Product_Description = poitm.Product_Description;
                    poitemd.ReceivedDate = poitm.ReceivedDate;
                    poitemd.Total_Amt = poitm.Total_Amt;
                    poitemd.Unit_Price = poitm.Unit_Price;
                    poitemd.Units = poitm.Units;
                    poitemd.GRN = newinv.GRNNo;
                    poitemd.CurrencyCode = new CurrencyDAL().getbyid(poitm.CurrencyId.Value).Symbol;

                    poitemd.Total_Qty = qty;
                    
                }


                return poitemd;
            }
        }
        public long GetLastID()
        {
            try
            {
                using (var db = new OzoneDBEntities())
                {
                    return db.INVENTORies.Where(d => d.Is_Deleted == false).Max(c => c.Pk_Inventory_Id) + 1;
                }
            }
            catch (Exception)
            {
                return 1;
            }
        }
        public string GetLastGRN()
        {
            try
            {
                using (var db = new OzoneDBEntities())
                {
                    return db.INVENTORies.Where(d => d.Is_Deleted == false && d.GRNNo.StartsWith("GRN")).Max(c => c.GRNNo) ;
                }
            }
            catch (Exception)
            {
                return "GRN" +DateTime.Now.Year +"00001";
            }
        }
        public string GetLastInventory()
        {
            string Invoice;
            using (var db = new OzoneDBEntities())
            {
                var data = db.INVENTORies.Where(d => d.Is_Deleted == false && d.GRNNo.StartsWith("DO")).Max(d => d.GRNNo);
                if (data==null)
                {
                    Invoice= "DO" + DateTime.Now.Year + "0000";
                    
                }
                else
                {
                   Invoice= data;
                }
            }
            return Invoice;
        }
        public string GetLastInvoice()
        {
            string invoice;
            using (var db = new OzoneDBEntities())
            {
                var data = db.InvoiceTabs.Max(d => d.InvoiceNumber);
                if (data==null)
                {
                    invoice = "INV" + System.DateTime.Now.Year + "0000";
                    
                }
                else
                {
                    invoice = data;
                }

            }
            return invoice;
        }
        public string MoveDeadStocktoNull()
        {
            using (var db = new OzoneDBEntities())
            {
               db.INVENTORies.Where(d => d.Is_Deleted == false && d.Account_Type == 2).ToList().ForEach(d => { d.Account_Type = 3; d.Modified_Date = System.DateTime.Now; });
                db.SaveChanges();
            }
            return "Success";
        }
    }
}
