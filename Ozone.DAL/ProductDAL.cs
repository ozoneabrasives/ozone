﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Ozone.DAL
{
    public class ProductDAL
    {
        public ProductDAL()
        {

        }
        public string InsertProduct(PRODUCT_DETAILS Product)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                dbCon.PRODUCT_DETAILS.Add(Product);
                dbCon.SaveChanges();
                return "Success";
            }
        }

        public List<PRODUCT_DETAILS> ListProduct(int display, int lenght,string search)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                dbCon.Configuration.ProxyCreationEnabled = false;
                if (search != "")
                {
                    return dbCon.PRODUCT_DETAILS.Include(c => c.CATEGORY_MASTER).Where(c => c.Is_Deleted == false &&  c.Product_Title.Contains(search)).OrderBy(d => d.Created_Date).Skip(display).Take(lenght).ToList();
                }
                else
                {
                    return dbCon.PRODUCT_DETAILS.Include(c => c.CATEGORY_MASTER).Where(c => c.Is_Deleted == false).OrderBy(d => d.Created_Date).Skip(display).Take(lenght).ToList();
                }
                
            }
        }

        public string EditProduct(PRODUCT_DETAILS Product)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                PRODUCT_DETAILS dalProduct = dbCon.PRODUCT_DETAILS.SingleOrDefault(c => c.Pk_Product_Id == Product.Pk_Product_Id);
                dalProduct.Product_Code = Product.Product_Code;
                dalProduct.Product_Title = Product.Product_Title;
                dalProduct.Product_Description = Product.Product_Description;
                dalProduct.Clearing_Rate = Product.Clearing_Rate;
                dalProduct.Consumption_Rate = Product.Consumption_Rate;
                dalProduct.Fk_Category_Id = Product.Fk_Category_Id;
                dalProduct.Is_Deleted = false;
                dalProduct.Manufactur_Date = Product.Manufactur_Date;
                dalProduct.Modified_By = 1;
                dalProduct.Modified_Date = System.DateTime.Now;
                dalProduct.Packaging = Product.Packaging;
                dalProduct.Price = Product.Price;
                dalProduct.Product_Img = Product.Product_Img;
                dalProduct.Surface_Profile = Product.Surface_Profile;

                dbCon.SaveChanges();
                return "Updated";
            }
        }

        public string DeleteProduct(long id)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                PRODUCT_DETAILS dalProduct = dbCon.PRODUCT_DETAILS.SingleOrDefault(c => c.Pk_Product_Id == id);
                dalProduct.Is_Deleted = true;
                dalProduct.Modified_Date = System.DateTime.Now;
                dbCon.SaveChanges();
                return "Deleted";
            }
        }

        public List<PRODUCT_DETAILS> DDLProduct()
        {
            using (var dbCon = new OzoneDBEntities())
            {
                return dbCon.PRODUCT_DETAILS.Where(c => c.Is_Deleted == false).AsEnumerable().Select(c => new PRODUCT_DETAILS { Product_Title = c.Product_Title, Pk_Product_Id = c.Pk_Product_Id }).ToList();
            }
        }

        public PRODUCT_DETAILS GetProductById(long id)
        {
            using (var dbContext = new OzoneDBEntities())
            {
                dbContext.Configuration.ProxyCreationEnabled = false;
                return dbContext.PRODUCT_DETAILS.Include(c=> c.PACKAGE_MASTER).SingleOrDefault(c => c.Pk_Product_Id == id && c.Is_Deleted == false);
            }
        }

        public int GetProductCount()
        {
            using (var dbcon = new OzoneDBEntities())
            {
                return dbcon.PRODUCT_DETAILS.Where(c => c.Is_Deleted == false).Count();

            }
        }

        public List<PRODUCT_DETAILS> ProductForSale()
        {
            using (var dbCon = new OzoneDBEntities())
            {
                dbCon.Configuration.ProxyCreationEnabled = false;
                return dbCon.PRODUCT_DETAILS.Include(c => c.INVENTORies).Include(c=> c.PACKAGE_MASTER).Include(c => c.CATEGORY_MASTER).Where(c => c.Is_Deleted == false )
                    .OrderBy(d => d.Created_Date).AsEnumerable().ToList();
            }
        }
    }
}
