//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ozone.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class COUNTRY_MASTER
    {
        public COUNTRY_MASTER()
        {
            this.STATE_MASTER = new HashSet<STATE_MASTER>();
            this.TaxMasters = new HashSet<TaxMaster>();
            this.SHOPPING_CART = new HashSet<SHOPPING_CART>();
        }
    
        public long Pk_Country_Id { get; set; }
        public string Country_Name { get; set; }
        public string Country_Code { get; set; }
        public Nullable<long> Created_by { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<long> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public Nullable<bool> Is_Deleted { get; set; }
    
        public virtual ICollection<STATE_MASTER> STATE_MASTER { get; set; }
        public virtual ICollection<TaxMaster> TaxMasters { get; set; }
        public virtual ICollection<SHOPPING_CART> SHOPPING_CART { get; set; }
    }
}
