//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ozone.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class CurrencyTab
    {
        public CurrencyTab()
        {
            this.PURCHASEORDER_ITEMS = new HashSet<PURCHASEORDER_ITEMS>();
        }
    
        public long Currencyid { get; set; }
        public string Symbol { get; set; }
        public string CurrencyName { get; set; }
    
        public virtual ICollection<PURCHASEORDER_ITEMS> PURCHASEORDER_ITEMS { get; set; }
    }
}
