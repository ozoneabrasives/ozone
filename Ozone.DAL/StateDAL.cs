﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Ozone.DAL
{
    public class StateDAL
    {
        public StateDAL()
        {

        }

        public StateDAL(long id)
        {
            using (var dbContext = new OzoneDBEntities())
            {
                STATE_MASTER coutnry = dbContext.STATE_MASTER.SingleOrDefault(c => c.Pk_State_Id == id && c.Is_Deleted == false);
            }
        }

        public string InsertState(STATE_MASTER State)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                dbCon.STATE_MASTER.Add(State);
                dbCon.SaveChanges();
                return "Success";
            }
        }

        public List<STATE_MASTER> ListState(int display, int lenght,string search)
        {
            List<STATE_MASTER> lststate = new List<STATE_MASTER>();
            using (var dbCon = new OzoneDBEntities())
            {
                dbCon.Configuration.ProxyCreationEnabled = false;
                if (search!="")
                {
                    lststate= dbCon.STATE_MASTER.Where(c => c.COUNTRY_MASTER.Country_Code.Contains(search) || c.COUNTRY_MASTER.Country_Name.Contains(search) || c.State_Code.Contains(search) || c.State_Name.Contains(search) ).Where(d=>d.Is_Deleted==false).Include(c => c.COUNTRY_MASTER).OrderBy(d => d.Created_Date).Skip(display).Take(lenght).ToList();
                }
                else
                {
                    lststate = dbCon.STATE_MASTER.Include(c => c.COUNTRY_MASTER).Where(c => c.Is_Deleted == false).OrderBy(d => d.Created_Date).Skip(display).Take(lenght).ToList();
                }
               
            }
            return lststate;
        }

        public string EditState(STATE_MASTER State)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                STATE_MASTER dalState = dbCon.STATE_MASTER.SingleOrDefault(c => c.Pk_State_Id == State.Pk_State_Id);
                dalState.State_Code = State.State_Code;
                dalState.State_Name = State.State_Name;
                dalState.Fk_Country_Id = State.Fk_Country_Id;
                dalState.Modified_Date = System.DateTime.Now;
                dbCon.SaveChanges();
                return "Updated";
            }
        }

        public string DeleteState(long id)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                STATE_MASTER dalState = dbCon.STATE_MASTER.SingleOrDefault(c => c.Pk_State_Id == id);
                dalState.Is_Deleted = true;
                dalState.Modified_Date = System.DateTime.Now;
                dbCon.SaveChanges();
                return "Deleted";
            }
        }

        public List<STATE_MASTER> DDLState(long id)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                return dbCon.STATE_MASTER.Where(c => c.Is_Deleted == false && c.Fk_Country_Id == id).AsEnumerable().Select(c => new STATE_MASTER { State_Name = c.State_Name, Pk_State_Id = c.Pk_State_Id }).ToList();
            }
        }

        public STATE_MASTER GetStateById(long id)
        {
            using (var dbContext = new OzoneDBEntities())
            {
                return dbContext.STATE_MASTER.SingleOrDefault(c => c.Pk_State_Id == id && c.Is_Deleted == false);
            }
        }

        public int GetStateCount(string search)
        {
            using (var dbcon = new OzoneDBEntities())
            {
                if (search!="")
                {
                    return dbcon.STATE_MASTER.Where(c => c.Is_Deleted == false && c.COUNTRY_MASTER.Country_Code.Contains(search) || c.COUNTRY_MASTER.Country_Name.Contains(search) || c.State_Code.Contains(search) || c.State_Name.Contains(search)).Count();    
                }
                else
                {
                    return dbcon.STATE_MASTER.Where(c => c.Is_Deleted == false).Count();
                }
                

            }
        }
    }
}
