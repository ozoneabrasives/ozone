﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ozone.DAL
{
    public class CountryDAL
    {
        public CountryDAL()
        {

        }

        public COUNTRY_MASTER GetCountryById(long id)
        {
            using (var dbContext = new OzoneDBEntities())
            {
                return dbContext.COUNTRY_MASTER.SingleOrDefault(c => c.Pk_Country_Id == id && c.Is_Deleted == false);
            }
        }

        public string InsertCountry(COUNTRY_MASTER country)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                dbCon.COUNTRY_MASTER.Add(country);
                dbCon.SaveChanges();
                return "Success";
            }
        }

        public List<COUNTRY_MASTER> ListCountry(int display, int lenght,string search)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                dbCon.Configuration.ProxyCreationEnabled = false;
                if (search != "")
                {
                    return dbCon.COUNTRY_MASTER.Where(c => c.Is_Deleted == false && c.Country_Code.ToLower().Contains(search.ToLower()) || c.Country_Name.ToLower() .Contains(search.ToLower())).OrderBy(d => d.Created_Date).Skip(display).Take(lenght).ToList();
                }
                else
                {
                    return dbCon.COUNTRY_MASTER.Where(c => c.Is_Deleted == false).OrderBy(d => d.Created_Date).Skip(display).Take(lenght).ToList();
                }
                
            }
        }

        public string EditCountry(COUNTRY_MASTER country)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                COUNTRY_MASTER dalCountry = dbCon.COUNTRY_MASTER.SingleOrDefault(c => c.Pk_Country_Id == country.Pk_Country_Id);
                dalCountry.Country_Code = country.Country_Code;
                dalCountry.Country_Name = country.Country_Name;
                dalCountry.Modified_Date = System.DateTime.Now;
                dbCon.SaveChanges();
                return "Updated";
            }
        }

        public string DeleteCountry(long id)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                COUNTRY_MASTER dalCountry = dbCon.COUNTRY_MASTER.SingleOrDefault(c => c.Pk_Country_Id == id);
                dalCountry.Is_Deleted = true;
                dalCountry.Modified_Date = System.DateTime.Now;
                dbCon.SaveChanges();
                return "Deleted";
            }
        }

        public List<COUNTRY_MASTER> DDLCountry()
        {
            using (var dbCon = new OzoneDBEntities())
            {
                return dbCon.COUNTRY_MASTER.Where(c => c.Is_Deleted == false).AsEnumerable().Select(c => new COUNTRY_MASTER { Country_Name = c.Country_Name, Pk_Country_Id = c.Pk_Country_Id }).ToList();
            }
        }
        public int GetCountryCount(string Search)
        {
            using (var dbcon= new OzoneDBEntities())
            {
                if (Search != "")
                {
                    return dbcon.COUNTRY_MASTER.Where(c => c.Is_Deleted == false && c.Country_Code.ToLower().Contains(Search.ToLower()) || c.Country_Name.ToLower() .Contains(Search.ToLower())).Count();
                }
                else
                {
                    return dbcon.COUNTRY_MASTER.Where(c => c.Is_Deleted == false).Count();
                }
               

            }
        }
    }
}
