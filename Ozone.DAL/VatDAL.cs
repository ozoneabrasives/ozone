﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ozone.DAL
{
    public class VatDAL
    {
         public VatDAL()
        {

        }

         public VatDAL(long id)
        {
            using (var dbContext = new OzoneDBEntities())
            {
                VAT_PERCENT coutnry = dbContext.VAT_PERCENT.SingleOrDefault(c => c.Pk_VAT_Id== id);
            }
        }

        public string InsertVAT(VAT_PERCENT VAT)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                //dbCon.VAT_PERCENT.Add(VAT);
                VAT_PERCENT dalVAT = dbCon.VAT_PERCENT.SingleOrDefault(c => c.Pk_VAT_Id == 1);
                dalVAT.VAT_Percent1 = VAT.VAT_Percent1;
                dalVAT.Modified_Date = System.DateTime.Now;
                dbCon.SaveChanges();
                return "Updated";
            }
        }

        public List<VAT_PERCENT> ListVAT(int display, int lenght)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                dbCon.Configuration.ProxyCreationEnabled = false;
                return dbCon.VAT_PERCENT.OrderBy(d => d.Created_Date).Skip(display).Take(lenght).ToList();
            }
        }

        public string EditVAT(VAT_PERCENT VAT)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                VAT_PERCENT dalVAT = dbCon.VAT_PERCENT.SingleOrDefault(c => c.Pk_VAT_Id == VAT.Pk_VAT_Id);
                dalVAT.VAT_Percent1 = VAT.VAT_Percent1;               
                dalVAT.Modified_Date = System.DateTime.Now;
                dbCon.SaveChanges();
                return "Updated";
            }
        }

        public string DeleteVAT(long id)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                VAT_PERCENT dalVAT = dbCon.VAT_PERCENT.SingleOrDefault(c => c.Pk_VAT_Id == id);
                
                dalVAT.Modified_Date = System.DateTime.Now;
                dbCon.SaveChanges();
                return "Deleted";
            }
        }

        public List<VAT_PERCENT> DDLVAT()
        {
            using (var dbCon = new OzoneDBEntities())
            {
                return dbCon.VAT_PERCENT.AsEnumerable().Select(c => new VAT_PERCENT { VAT_Percent1 = c.VAT_Percent1, Pk_VAT_Id = c.Pk_VAT_Id }).ToList();
            }
        }

        public VAT_PERCENT GetVATById(long id)
        {
            using (var dbContext = new OzoneDBEntities())
            {
                return dbContext.VAT_PERCENT.SingleOrDefault(c => c.Pk_VAT_Id == id);
            }
        }

        public int GetVATCount()
        {
            using (var dbcon = new OzoneDBEntities())
            {
                return dbcon.VAT_PERCENT.Count();

            }
        }
    }
}
