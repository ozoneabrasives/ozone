﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Ozone.DAL.Models;

namespace Ozone.DAL
{
    public class CartItemsDAL
    {
        public string InsertCartItem(List<SHOPPING_CART_ITEMS> lstItem)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                foreach (var item in lstItem)
                {
                    dbCon.SHOPPING_CART_ITEMS.Add(item);
                    dbCon.SaveChanges();
                }
                return "Success";
            }
        }

        public List<SHOPPING_CART_ITEMS> ListCart(int display, int lenght, string userType, long id)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                dbCon.Configuration.ProxyCreationEnabled = false;
                if (userType == "Admin")
                {
                    return dbCon.SHOPPING_CART_ITEMS.Where(c => c.Is_Deleted == false && c.Is_Extra == true).Include(c => c.SHOPPING_CART).Include(c => c.PACKAGE_MASTER).Include(c => c.PRODUCT_DETAILS).OrderBy(d => d.Create_Date).Skip(display).Take(lenght).ToList();
                }
                else
                {
                    return dbCon.SHOPPING_CART_ITEMS.Where(c => c.Is_Deleted == false && c.Is_Extra == true && c.Create_By == id).Include(c => c.SHOPPING_CART).Include(c => c.PACKAGE_MASTER).Include(c => c.PRODUCT_DETAILS).OrderBy(d => d.Create_Date).Skip(display).Take(lenght).ToList();
                }
            }
        }

        public int GetExtraCount(string userType, long id)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                if (userType == "Admin")
                {
                    return dbCon.SHOPPING_CART_ITEMS.Where(c => c.Is_Deleted == false && c.Is_Extra == true).Count();
                }
                else
                {
                    return dbCon.SHOPPING_CART_ITEMS.Where(c => c.Is_Deleted == false && c.Is_Extra == true && c.Create_By == id).Count();
                }

            }
        }

        public List<SHOPPING_CART_ITEMS> GetListById(long id)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                dbCon.Configuration.ProxyCreationEnabled = false;
                return dbCon.SHOPPING_CART_ITEMS.Where(c => c.Is_Deleted == false && c.Fk_Shopping_Cart_Id == id).Include(c => c.PRODUCT_DETAILS).Include(c => c.PACKAGE_MASTER).ToList();
            }
        }

        public SHOPPING_CART_ITEMS GetCartItemById(long id)
        {
            using (var dbContext = new OzoneDBEntities())
            {
                return dbContext.SHOPPING_CART_ITEMS.Include(c => c.PACKAGE_MASTER).Include(c => c.PRODUCT_DETAILS).Where(c => c.Pk_Shopping_Cart_Item_Id == id && c.Is_Deleted == false).First();
            }
        }

        public static string EditCart(SHOPPING_CART_ITEMS item)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                SHOPPING_CART_ITEMS dalItem = dbCon.SHOPPING_CART_ITEMS.SingleOrDefault(c => c.Pk_Shopping_Cart_Item_Id == item.Pk_Shopping_Cart_Item_Id);
                dalItem.Fk_Package_Id = item.Fk_Package_Id;
                dalItem.Fk_Product_Id = item.Fk_Product_Id;
                dalItem.Fk_Shopping_Cart_Id = item.Fk_Shopping_Cart_Id;
                dalItem.Modified_By = item.Modified_By;
                dalItem.Modified_Date = item.Modified_Date;
                dalItem.Quantity = item.Quantity;
                dalItem.Total_Amt = item.Total_Amt;
                dalItem.Unit_Price = item.Unit_Price;
                dbCon.SaveChanges();
                return "Updated";
            }
        }

        public string InsertCartItem(SHOPPING_CART_ITEMS Item)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                dbCon.SHOPPING_CART_ITEMS.Add(Item);
                dbCon.SaveChanges();
                return "Success";
            }
        }

        public string DeleteItem(long id)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                SHOPPING_CART_ITEMS dalItem = dbCon.SHOPPING_CART_ITEMS.SingleOrDefault(c => c.Pk_Shopping_Cart_Item_Id == id);
                dalItem.Is_Deleted = true;
                dbCon.SaveChanges();
                return "Deleted";
            }
        }
        public List<DeliveryOrderUpdateModel> CartItemsByGRN(long salesorder)
        {
            List<SHOPPING_CART_ITEMS> lstpoitems = new List<SHOPPING_CART_ITEMS>();
            List<DeliveryOrderUpdateModel> lstfinallist = new List<DeliveryOrderUpdateModel>();

            using (var dbCon = new OzoneDBEntities())
            {
                dbCon.Configuration.ProxyCreationEnabled = false;
                lstpoitems = dbCon.SHOPPING_CART_ITEMS.Include(c => c.SHOPPING_CART).Include(c => c.PRODUCT_DETAILS).Include(d=>d.PACKAGE_MASTER) .Where(c => c.Is_Deleted == false && c.Fk_Shopping_Cart_Id == salesorder).OrderBy(d => d.Create_Date).ToList();
                foreach (var item in lstpoitems)
                {
                    var inventor = dbCon.INVENTORies.Where(d => d.Fk_PO_Id == item.Fk_Shopping_Cart_Id && d.Fk_Product_Id == item.Fk_Product_Id && d.Fk_Package_Id == item.Fk_Package_Id && d.Order_Type == "SalesOrder").Sum(d => d.Product_Qty);
                    var mins = (from td in dbCon.INVENTORies where td.Fk_Package_Id == item.Fk_Package_Id && td.Account_Type == 1 && td.Is_Deleted == false select td.Product_Qty).Sum();
                    var availableqty = (from td in dbCon.INVENTORies where td.Fk_Package_Id == item.Fk_Package_Id && td.Account_Type == 0 && td.Is_Deleted == false select td.Product_Qty).Sum() - (mins == null ? 0 : mins);
                    if (item.Quantity != inventor)
                    {
                        DeliveryOrderUpdateModel objdel= new DeliveryOrderUpdateModel();

                        objdel.ProductCode = item.PRODUCT_DETAILS.Product_Code;
                        objdel.Packaging = item.PACKAGE_MASTER.Package_Title;
                        objdel.OrderQty = item.Quantity.Value;
                        objdel.DeliverQty = Convert.ToInt32( inventor == null ? 0 : inventor);
                        objdel.RemainQty = Convert.ToInt32(item.Quantity - (inventor == null ? 0 : inventor));
                        objdel.InStock =Convert.ToInt32( availableqty);
                        objdel.Fk_Product_Id = item.Fk_Product_Id.Value;
                        objdel.Pk_Shopping_Cart_Item_Id = item.Pk_Shopping_Cart_Item_Id;
                        objdel.packagingunits = item.PACKAGE_MASTER.Package_Amt + " " + item.PACKAGE_MASTER.Package_Unit;

                       
                        lstfinallist.Add(objdel);
                    }
                }

            }
            return lstfinallist;

        }
        public List<SHOPPING_CART_ITEMS> LpdateItemToInventory(List<SHOPPING_CART_ITEMS> lstshopingitem)
        {

            List<SHOPPING_CART_ITEMS> lstpur = new List<SHOPPING_CART_ITEMS>();
            string grnnumber = GenerateGRN();
            decimal totoalcost=0;
                using (var dbCon = new OzoneDBEntities())
                {
                    long? id = null;
                   
                    foreach (var item in lstshopingitem.Where(d=>d.Quantity>0).ToList())
                    {
                        SHOPPING_CART_ITEMS SOItems = dbCon.SHOPPING_CART_ITEMS.SingleOrDefault(d => d.Pk_Shopping_Cart_Item_Id == item.Pk_Shopping_Cart_Item_Id);
                        item.GRNNumber = grnnumber;
                        item.Create_By = SOItems.Create_By;
                        item.Create_Date = SOItems.Create_Date;
                        item.Fk_Package_Id = SOItems.Fk_Package_Id;
                        item.Fk_Product_Id = SOItems.Fk_Product_Id;
                        item.Fk_Shopping_Cart_Id = SOItems.Fk_Shopping_Cart_Id;
                        item.PACKAGE_MASTER = SOItems.PACKAGE_MASTER;
                        item.Pk_Shopping_Cart_Item_Id = SOItems.Pk_Shopping_Cart_Item_Id;
                        item.PRODUCT_DETAILS = SOItems.PRODUCT_DETAILS;
                        item.Total_Amt = SOItems.Unit_Price * item.Quantity;
                        item.Unit_Price = SOItems.Unit_Price;
                        
                        item.Quantity = item.Quantity;
                        lstpur.Add(item);

                        id = SOItems.Fk_Shopping_Cart_Id;
                        INVENTORY inv = new INVENTORY();
                        inv.Account_Type = 1;
                        inv.Created_By = item.Create_By;
                        inv.Created_Date = System.DateTime.Now;
                        inv.Fk_PO_Id = SOItems.Fk_Shopping_Cart_Id;
                        inv.Fk_Product_Id = item.Fk_Product_Id;
                        inv.Fk_Package_Id = SOItems.Fk_Package_Id;
                        inv.Product_Qty = item.Quantity;
                        inv.Order_Type = "SalesOrder";
                        inv.Package = SOItems.PACKAGE_MASTER.Package_Title;
                        inv.Units = SOItems.PACKAGE_MASTER.Package_Unit;
                        inv.GRNNo = grnnumber;
                        inv.Is_Deleted = false;

                        totoalcost += (item.Unit_Price.Value * item.Quantity.Value);
                      

                        dbCon.INVENTORies.Add(inv);
                        //dbCon.SaveChanges();


                    }
                    InvoiceTab objinvoice = new InvoiceTab();
                    objinvoice.DoNumber = grnnumber;
                    objinvoice.DueDate = System.DateTime.Now.AddDays(lstshopingitem[0].Duedays);
                    objinvoice.FK_ShoppingCartid = lstpur[0].Fk_Shopping_Cart_Id;
                    objinvoice.InvoiceDate = System.DateTime.Now;
                    objinvoice.InvoiceNumber = GenerateInvoice();
                    objinvoice.RemeainCost = totoalcost;
                    objinvoice.ShippingRate = lstpur[0].shippingcharges;
                    objinvoice.TotalCost = totoalcost;
                    objinvoice.Vat = Convert.ToDecimal( (((totoalcost + lstpur[0].shippingcharges) * Convert.ToInt32(new VatDAL().GetVATById(1).VAT_Percent1)) / 100));
                    dbCon.InvoiceTabs.Add(objinvoice);
                    

                    dbCon.SaveChanges();
                    lstpur[0].pkinvoiceid = objinvoice.PK_Invoice_Id;
                    SHOPPING_CART dalPO = dbCon.SHOPPING_CART.SingleOrDefault(c => c.Pk_UserCart_Id == id);
                    bool ispartial = false;
                    var totalitems = dalPO.SHOPPING_CART_ITEMS;
                    foreach (var item in totalitems)
                    {
                        var count = dbCon.INVENTORies.Where(d => d.Fk_PO_Id == item.Fk_Shopping_Cart_Id && d.Fk_Product_Id == item.Fk_Product_Id && d.Fk_Package_Id == item.Fk_Package_Id).Sum(d => d.Product_Qty);
                        if (count == item.Quantity)
                        {
                            ispartial = false;
                        }
                        else
                        {
                            ispartial = true;
                        }

                    }
                    if (ispartial)
                    {
                        dalPO.Order_Status = "Partially Shipped";
                    }
                    else
                    {
                        dalPO.Order_Status = "Shipped";
                    }

                    dalPO.Modified_Date = System.DateTime.Now;
                    dbCon.SaveChanges();
                    return lstpur;
                
            }
        }
        public string GenerateGRN()
        {
            try
            {
                long num = Convert.ToInt64(new InvenortyDAL().GetLastInventory().Substring(6)) + 1;

                return "DO" + DateTime.Now.Year + num.ToString("D" + 5);
            }

            catch (Exception e)
            {
                return "DO" + DateTime.Now.Year + "00001";
            }

        }
        //public string GenerateGRN()
        //{
        //    try
        //    {
        //        long num = Convert.ToInt64(new InvenortyDAL().GetLastInventory().Substring(6)) + 1;

        //        return "DO" + DateTime.Now.Year + num.ToString("D" + 5);
        //    }

        //    catch (Exception e)
        //    {
        //        return "DO" + DateTime.Now.Year + "00001";
        //    }

        //}
        public string GenerateInvoice() {
            try
            {
                long num = Convert.ToInt64(new InvenortyDAL().GetLastInventory().Substring(7)) + 1;
                return "INV" + System.DateTime.Now.Year + num.ToString("D" + 5);


            }
            catch (Exception e)
            {
                return "INV" + System.DateTime.Now.Year + "00001";
            }
        }
        public List<SHOPPING_CART_ITEMS> ListGRN(int display, int lenght, long id)
        {
            List<SHOPPING_CART_ITEMS> lstuitems = new List<SHOPPING_CART_ITEMS>();
            using (var db = new OzoneDBEntities())
            {
                db.Configuration.ProxyCreationEnabled = false;
               // var lstitems = db.INVENTORies.Where(d => d.Fk_PO_Id == id && d.Account_Type == 1).Include(d => d.PRODUCT_DETAILS).Include(d => d.PACKAGE_MASTER).ToList();
                var lstitems = (from u in db.INVENTORies.Include(u => u.PRODUCT_DETAILS).Include(d => d.PACKAGE_MASTER).AsEnumerable()
                                where u.Fk_PO_Id == id && u.Account_Type == 1
                                select new
                                {
                                    Fk_Product_Id = u.Fk_Product_Id,
                                    GRNNo = u.GRNNo,
                                    Product_Qty = u.Product_Qty,
                                    ProductName = u.PRODUCT_DETAILS.Product_Title,
                                    PackageName = u.PACKAGE_MASTER.Package_Title,
                                    Created_Date = u.Created_Date
                                }).OrderBy(d => d.Fk_Product_Id).Skip(display).Take(lenght).ToList();
                foreach (var item in lstitems)
                {
                    var purchaseorder = db.SHOPPING_CART.Where(d => d.Pk_UserCart_Id == id).Include(d => d.USER_REGISTRATION).FirstOrDefault();

                    lstuitems.Add(new SHOPPING_CART_ITEMS { Fk_Shopping_Cart_Id = id, Fk_Product_Id = item.Fk_Product_Id.Value, GRNNumber = item.GRNNo, Quantity = Convert.ToInt32(item.Product_Qty), PRODUCT_DETAILS = new PRODUCT_DETAILS { Product_Title = item.ProductName }, PACKAGE_MASTER = new PACKAGE_MASTER { Package_Title = item.PackageName }, Create_Date = item.Created_Date.Value, SHOPPING_CART = purchaseorder });

                }



            }
            return lstuitems;
        }

        public List<ListofinvoceModel> ListInvoice(int display, int lenght, long id)
        {
            List<ListofinvoceModel> lstuitems = new List<ListofinvoceModel>();
            using (var db = new OzoneDBEntities())
            {
                db.Configuration.ProxyCreationEnabled = false;
                // var lstitems = db.INVENTORies.Where(d => d.Fk_PO_Id == id && d.Account_Type == 1).Include(d => d.PRODUCT_DETAILS).Include(d => d.PACKAGE_MASTER).ToList();
                var invoices = db.InvoiceTabs.Where(d => d.FK_ShoppingCartid == id).Include(d=>d.SHOPPING_CART).Include(d=>d.SHOPPING_CART.USER_REGISTRATION).Include(d=>d.INV_PAYMENT_TAB) .OrderByDescending(d=>d.PK_Invoice_Id).Skip(display). Take(lenght) .ToList();
                foreach (var item in invoices)
                {
                    lstuitems.Add(new ListofinvoceModel { DeliveryOrder = item.DoNumber, DueDate = item.DueDate.Value.ToShortDateString(), inventoryid = item.PK_Invoice_Id, InvoiceOrder = item.InvoiceNumber, orderid = item.FK_ShoppingCartid.Value, Salesordernumber = item.SHOPPING_CART.Order_Number, folderpath = "/Invoice_Generate/" + item.SHOPPING_CART.USER_REGISTRATION.Company_Name + "/" + item.SHOPPING_CART.Order_Number + "/" + item.InvoiceNumber + ".pdf", paymentdue = item.RemeainCost.ToString(), ReceivePayment = item.INV_PAYMENT_TAB.Sum(d => d.Amount).ToString(), TotalPayment = (item.TotalCost + item.ShippingRate).ToString() });
                }



            }
            return lstuitems;
        }
        public int GetGRNCount(long id)
        {
            int count;
            using (var db = new OzoneDBEntities())
            {
                count = db.INVENTORies.Where(d => d.Fk_PO_Id == id && d.Account_Type == 1 ).Count();


            }
            return count;
        }
        public int GetInvoiceCount(long id)
        {
            int count;
            using (var db = new OzoneDBEntities())
            {
                count = db.InvoiceTabs.Where(d => d.FK_ShoppingCartid == id).Count();


            }
            return count;
        }
    }
}
