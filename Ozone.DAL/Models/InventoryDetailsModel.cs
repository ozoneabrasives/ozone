﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ozone.DAL.Models
{
  public  class InventoryDetailsModel
    {
      public String Productname { get; set; }
      public String Package { get; set; }
      public String Units { get; set; }
      public long OrderQty { get; set; }
      public long AvailableStock { get; set; }
      public long RequiredStock { get; set; }
      public long Productid { get; set; }
      public string productcode { get; set; }
      
    }
}
