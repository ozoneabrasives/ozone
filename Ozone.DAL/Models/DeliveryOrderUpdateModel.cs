﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ozone.DAL.Models
{
    public class DeliveryOrderUpdateModel
    {
        public string ProductCode { get; set; }
        public string Packaging { get; set; }
        public string packagingunits { get; set; }
        public int OrderQty { get; set; }
        public int DeliverQty { get; set; }
        public int RemainQty { get; set; }
        public int InStock { get; set; }
        public long Fk_Product_Id { get; set; }
        public long Pk_Shopping_Cart_Item_Id { get; set; }

    }
}
