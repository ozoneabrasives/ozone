﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ozone.DAL.Models
{
  public  class ListofinvoceModel
    {
      public string Salesordernumber { get; set; }
      public long orderid { get; set; }
      public long inventoryid { get; set; }
      public string DeliveryOrder { get; set; }
      public string InvoiceOrder { get; set; }
      public string DueDate { get; set; }
      public string folderpath { get; set; }
      public string TotalPayment { get; set; }
      public string ReceivePayment { get; set; }
      public string paymentdue { get; set; }
      public string totalorderpayment { get; set; }
      public string UserName { get; set; }
      public long PK_User_ID { get; set; }
      public DateTime orderdate { get; set; }
    }
}
