﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ozone.DAL.Models
{
  public  class CityModel
    {
      public long CityId { get; set; }
      public string CityName { get; set; }
      public string State { get; set; }
      public string Country { get; set; }
      public long Stateid { get; set; }
      public long Countryid { get; set; }
    }
}
