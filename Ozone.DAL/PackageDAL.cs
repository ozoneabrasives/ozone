﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Ozone.DAL
{
    public class PackageDAL
    {
        public PackageDAL()
        {

        }

        public PackageDAL(long id)
        {
            using (var dbContext = new OzoneDBEntities())
            {
                PACKAGE_MASTER coutnry = dbContext.PACKAGE_MASTER.SingleOrDefault(c => c.Pk_Package_Id == id && c.Is_Deleted == false);
            }
        }

        public string InsertPackage(PACKAGE_MASTER Package)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                dbCon.PACKAGE_MASTER.Add(Package);
                dbCon.SaveChanges();
                return "Success";
            }
        }

        public List<PACKAGE_MASTER> ListPackage(int display, int lenght,string search)
        {
            List<PACKAGE_MASTER> lstpackage = new List<PACKAGE_MASTER>();
            using (var dbCon = new OzoneDBEntities())
            {
                dbCon.Configuration.ProxyCreationEnabled = false;
                if (search!="")
                {
                    lstpackage=dbCon.PACKAGE_MASTER.Include(c => c.PRODUCT_DETAILS).Where(c => c.Is_Deleted == false && c.PRODUCT_DETAILS.Product_Code.Contains(search) || c.PRODUCT_DETAILS.Product_Title.Contains(search) || c.Package_Title.Contains(search)).OrderBy(d => d.Created_Date).Skip(display).Take(lenght).ToList();
                }
                else
                {
                    lstpackage=dbCon.PACKAGE_MASTER.Include(c => c.PRODUCT_DETAILS).Where(c => c.Is_Deleted == false).OrderBy(d => d.Created_Date).Skip(display).Take(lenght).ToList();
                }
                
            }
            return lstpackage;
        }

        public string EditPackage(PACKAGE_MASTER Package)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                PACKAGE_MASTER dalPackage = dbCon.PACKAGE_MASTER.SingleOrDefault(c => c.Pk_Package_Id == Package.Pk_Package_Id);
                dalPackage.Package_Title = Package.Package_Title;
                dalPackage.Package_Unit = Package.Package_Unit;
                dalPackage.Package_Amt = Package.Package_Amt;
                dalPackage.Fk_Product_Id = Package.Fk_Product_Id;
                dalPackage.Cost_Price = Package.Cost_Price;
                dalPackage.Sell_Price = Package.Sell_Price;
                dalPackage.Modified_Date = System.DateTime.Now;
                dbCon.SaveChanges();
                return "Updated";
            }
        }

        public string DeletePackage(long id)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                PACKAGE_MASTER dalPackage = dbCon.PACKAGE_MASTER.SingleOrDefault(c => c.Pk_Package_Id == id);
                dalPackage.Is_Deleted = true;
                dalPackage.Modified_Date = System.DateTime.Now;
                dbCon.SaveChanges();
                return "Deleted";
            }
        }

        public List<PACKAGE_MASTER> DDLPackage()
        {
            using (var dbCon = new OzoneDBEntities())
            {
                return dbCon.PACKAGE_MASTER.Where(c => c.Is_Deleted == false).AsEnumerable().Select(c => new PACKAGE_MASTER { Package_Title = c.Package_Title + "-" + c.Package_Amt + " " + c.Package_Unit, Pk_Package_Id = c.Pk_Package_Id }).ToList();
            }
        }

        public PACKAGE_MASTER GetPackageById(long id)
        {
            using (var dbContext = new OzoneDBEntities())
            {
                return dbContext.PACKAGE_MASTER.SingleOrDefault(c => c.Pk_Package_Id == id && c.Is_Deleted == false);
            }
        }

        public int GetPackageCount()
        {
            using (var dbcon = new OzoneDBEntities())
            {
                return dbcon.PACKAGE_MASTER.Where(c => c.Is_Deleted == false).Count();

            }
        }

        public List<PACKAGE_MASTER> DDLUnit(string pack)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                return dbCon.PACKAGE_MASTER.Where(c => c.Is_Deleted == false && c.Package_Title == pack).AsEnumerable().Select(c => new PACKAGE_MASTER { Package_Unit = c.Package_Unit, Pk_Package_Id = c.Pk_Package_Id }).ToList();
            }
        }

        public decimal? BindPrice(long id)
        {

            using (var dbCon = new OzoneDBEntities())
            {
                return dbCon.PACKAGE_MASTER.SingleOrDefault(c => c.Is_Deleted == false && c.Pk_Package_Id == id).Sell_Price;
            }
        }

        public decimal? BindBPrice(long id)
        {
            using (var dbCon = new OzoneDBEntities())
            {
                return dbCon.PACKAGE_MASTER.SingleOrDefault(c => c.Is_Deleted == false && c.Pk_Package_Id == id).Cost_Price;
            }
        }
        public List<PACKAGE_MASTER> GetPackageByProduct(long Prodid)
        {
            List<PACKAGE_MASTER> lstpackages = new List<PACKAGE_MASTER>();
            using (var dbcon = new OzoneDBEntities())
            {
                dbcon.Configuration.ProxyCreationEnabled = false;
                return dbcon.PACKAGE_MASTER.Where(d => d.Is_Deleted == false && d.Fk_Product_Id == Prodid).AsEnumerable().ToList();
                
            }
            
        }
    }
}
