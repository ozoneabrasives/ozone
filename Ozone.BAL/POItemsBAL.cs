﻿using Ozone.DAL;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ozone.BAL
{
    public class POItemsBAL
    {
        public long Pk_PO_Item_Id { get; set; }
        public long Fk_Product_Id { get; set; }
        public string Product_Code { get; set; }
        public string Product_Description { get; set; }
        public string Packaging { get; set; }
        public string Units { get; set; }
        public Nullable<long> Total_Qty { get; set; }
        public Nullable<decimal> Unit_Price { get; set; }
        public Nullable<decimal> Total_Amt { get; set; }
        public Nullable<long> Created_by { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<long> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public Nullable<bool> Is_Deleted { get; set; }
        public Nullable<long> Fk_PO_Id { get; set; }
        public Nullable<long> Fk_Package_Id { get; set; }
        public Nullable<long> CurrencyId { get; set; }
        public List<PURCHASEORDER_ITEMS> ListPOItem(int display, int lenght,long poId)
        {
            return new POItemsDAL().ListPOItem(display, lenght,poId);
        }
        public List<PURCHASEORDER_ITEMS> ListPOItemsByNullGRN(long poid)
        {
            return new POItemsDAL().ListPOItemsByNullGRN(poid);
        }

        public List<PURCHASEORDER_ITEMS> ListItems(int display, int lenght)
        {
            return new POItemsDAL().ListItems(display, lenght);
        }

        public static string InsertPOItem(POItemsBAL balPOItem)
        {
            try
            {
                POItemsDAL dalC = new POItemsDAL();
                PURCHASEORDER_ITEMS POItem = new PURCHASEORDER_ITEMS();               
                POItem.Is_Deleted = false;
                POItem.Created_Date = System.DateTime.Now;
                POItem.Fk_PO_Id = balPOItem.Fk_PO_Id;
                POItem.Fk_Product_Id = balPOItem.Fk_Product_Id;
                POItem.Product_Description = balPOItem.Product_Description;
                POItem.Packaging = balPOItem.Packaging;
                POItem.Product_Code = balPOItem.Product_Code;
                POItem.Total_Amt = balPOItem.Total_Amt;
                POItem.Fk_Package_Id = balPOItem.Fk_Package_Id;
                POItem.Total_Qty = balPOItem.Total_Qty;
                POItem.Unit_Price = balPOItem.Unit_Price;
                POItem.Units = balPOItem.Units;
                POItem.CurrencyId = balPOItem.CurrencyId;
                
                return dalC.InsertPOItem(POItem);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string EditPOItem(POItemsBAL balPOItem)
        {
            try
            {
                POItemsDAL dalC = new POItemsDAL();
                PURCHASEORDER_ITEMS POItem = new PURCHASEORDER_ITEMS();
                POItem.Pk_PO_Item_Id = balPOItem.Pk_PO_Item_Id;
                POItem.Modified_Date = System.DateTime.Now;
                POItem.Fk_PO_Id = balPOItem.Fk_PO_Id;
                POItem.Fk_Product_Id = balPOItem.Fk_Product_Id;
                POItem.Product_Description = balPOItem.Product_Description;
                POItem.Packaging = balPOItem.Packaging;
                POItem.Product_Code = balPOItem.Product_Code;
                POItem.Total_Amt = balPOItem.Total_Amt;
                POItem.Fk_Package_Id = balPOItem.Fk_Package_Id;
                POItem.Total_Qty = balPOItem.Total_Qty;
                POItem.Unit_Price = balPOItem.Unit_Price;
                POItem.Units = balPOItem.Units;
                POItem.Modified_By = balPOItem.Modified_By;
                POItem.Modified_Date = balPOItem.Modified_Date;
                POItem.CurrencyId = balPOItem.CurrencyId;
                return dalC.EditPOItem(POItem);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string DeletePOItem(long id)
        {
            return new POItemsDAL().DeletePOItem(id);
        }

       

        public PURCHASEORDER_ITEMS GetPOItemById(long id)
        {
            return new POItemsDAL().GetPOItemById(id);
        }

        public int GetPOItemCount()
        {
            return new POItemsDAL().GetPOItemCount();
        }

        public int GetExtItemCount()
        {
            return new POItemsDAL().GetExtItemCount();
        }

        public static string InsertPOItem(PurchaseOrderBAL po)
        {
            try
            {
               
                OzoneDBEntities dbcon = new OzoneDBEntities();
                POItemsDAL dalC = new POItemsDAL();
                List<PURCHASEORDER_ITEMS> lstPOI = new List<PURCHASEORDER_ITEMS>();
                PURCHASE_ORDER PO = new PURCHASE_ORDER();
              //  DateTime date = DateTime.Parse(po.PO_Date, (IFormatProvider)CultureInfo.InvariantCulture);
                DateTime date = DateTime.ParseExact(po.PO_Date,
                                    "dd/MM/yyyy",
                                    CultureInfo.InvariantCulture);
                PO.Checked_By = po.Checked_By;
                PO.Document_Req = po.Document_Req;
                PO.Grand_Total = po.Grand_Total;
                PO.Payment_Terms = po.Payment_Terms;
                PO.PO_Date = date;
                PO.PO_Number = po.PO_Number;
                PO.Ship_Address = po.Ship_Address;
                PO.Ship_City = po.Ship_City;
                PO.Ship_Compny = po.Ship_Compny;
                PO.Ship_Name = po.Ship_Name;
                PO.Ship_Phone = po.Ship_Phone;
                PO.Ship_State = po.Ship_State;
                PO.Ship_Zip_code = po.Ship_Zip_code;
                PO.Shipment_Detail = po.Shipment_Detail;
                PO.Transport = po.Transport;
                PO.Created_by = po.Created_by;
                PO.Created_Date = System.DateTime.Now;
                PO.Is_Deleted = false;
                PO.Ship_Country = po.Ship_Country;
                PO.PO_Status = po.PO_Status;
                PO.Fk_Vendor_Id = po.Fk_Vendor_Id;
                PO.DocumentDetais = po.DocumentDetais;
                PO.Shipper = po.Shipper;
                PO.Consignee = po.Consignee;
                PO.NotifyParty = po.NotifyParty;
                PO.PortLoading = po.PortLoading;
                PO.PortDischarge = po.PortDischarge;
                PO.PortDestination = po.PortDestination;
                PO.IsPartial = po.IsPartial;
                PO.TransShip = po.TransShip;
                PO.LstDateofShip = po.LstDateofShip;
                PO.DeliveryTerms = po.DeliveryTerms;
                foreach (var item in po.lstPOI)
                {
                    PURCHASEORDER_ITEMS POItem = new PURCHASEORDER_ITEMS();
                    POItem.Is_Deleted = false;
                    POItem.Created_Date = System.DateTime.Now;
                    //POItem.Fk_PO_Id = PO.Pk_PurchaseOrder_Id;
                    POItem.Fk_Product_Id = item.Fk_Product_Id;
                    POItem.Product_Description = item.Product_Description;
                    POItem.Packaging = item.Packaging;
                    POItem.Product_Code = item.Product_Code;
                    POItem.Total_Amt = item.Total_Amt;
                    POItem.Total_Qty = item.Total_Qty;
                    POItem.Unit_Price = item.Unit_Price;
                    POItem.Fk_Package_Id = item.Fk_Package_Id;
                    POItem.Units = item.Units;
                    POItem.PURCHASE_ORDER = PO;
                    POItem.Created_by = item.Created_by;
                    POItem.Created_Date = System.DateTime.Now;
                    POItem.CurrencyId = item.CurrencyId;
                    lstPOI.Add(POItem);
                }

              
                return dalC.InsertPOItem(lstPOI);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
