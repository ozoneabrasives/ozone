﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ozone.DAL;

namespace Ozone.BAL
{
   public class CurrencyBAL
    {
       public  List<CurrencyTab> getlist()
       {
           return new CurrencyDAL().getlist();
       }
       public CurrencyTab GetCurrencybyid(long curencyid)
       {
           return new CurrencyDAL().getbyid(curencyid);
       }
    }
}
