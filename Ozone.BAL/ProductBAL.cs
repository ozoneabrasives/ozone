﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ozone.DAL;

namespace Ozone.BAL
{
    public class ProductBAL
    {
       
        public long Pk_Product_Id { get; set; }
        public string Product_Title { get; set; }
        public string Product_Description { get; set; }
        public string Clearing_Rate { get; set; }
        public string Consumption_Rate { get; set; }
        public string Surface_Profile { get; set; }
        public string Packaging { get; set; }
        public Nullable<System.DateTime> Manufactur_Date { get; set; }
        public string Product_Code { get; set; }
        public Nullable<decimal> Price { get; set; }
        public string Product_Img { get; set; }
        public Nullable<long> Created_by { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<long> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public Nullable<bool> Is_Deleted { get; set; }
        public Nullable<long> Fk_Category_Id { get; set; }

        public List<PRODUCT_DETAILS> ListProduct(int display, int lenght,string serch)
        {
            return new ProductDAL().ListProduct(display, lenght,serch);
        }

        public static string InsertProduct(ProductBAL balProduct)
        {
            try
            {
                ProductDAL dalC = new ProductDAL();
                PRODUCT_DETAILS Product = new PRODUCT_DETAILS();
                Product.Product_Code = balProduct.Product_Code;
                Product.Product_Title = balProduct.Product_Title;
                Product.Is_Deleted = false;
                Product.Created_Date = System.DateTime.Now;
                Product.Fk_Category_Id = balProduct.Fk_Category_Id;
                Product.Clearing_Rate = balProduct.Clearing_Rate;
                Product.Consumption_Rate = balProduct.Consumption_Rate;
                Product.Manufactur_Date = balProduct.Manufactur_Date;
                Product.Packaging = balProduct.Packaging;
                Product.Price = balProduct.Price;
                Product.Product_Description = balProduct.Product_Description;
                Product.Product_Img = balProduct.Product_Img;
                Product.Surface_Profile = balProduct.Surface_Profile;
                return dalC.InsertProduct(Product);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string EditProduct(ProductBAL balProduct)
        {
            try
            {
                ProductDAL dalC = new ProductDAL();
                PRODUCT_DETAILS Product = new PRODUCT_DETAILS();
                Product.Pk_Product_Id = balProduct.Pk_Product_Id;
                Product.Product_Code = balProduct.Product_Code;
                Product.Product_Title = balProduct.Product_Title;
                Product.Is_Deleted = false;
                Product.Created_Date = System.DateTime.Now;
                Product.Fk_Category_Id = balProduct.Fk_Category_Id;
                Product.Clearing_Rate = balProduct.Clearing_Rate;
                Product.Consumption_Rate = balProduct.Consumption_Rate;
                Product.Manufactur_Date = balProduct.Manufactur_Date;
                Product.Packaging = balProduct.Packaging;
                Product.Price = balProduct.Price;
                Product.Product_Description = balProduct.Product_Description;
                Product.Product_Img = balProduct.Product_Img;
                Product.Surface_Profile = balProduct.Surface_Profile;
                return dalC.EditProduct(Product);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string DeleteProduct(long id)
        {
            return new ProductDAL().DeleteProduct(id);
        }

        public List<PRODUCT_DETAILS> DDLProduct()
        {
            try
            {
                ProductDAL dal = new ProductDAL();
                return dal.DDLProduct();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public PRODUCT_DETAILS GetProductById(long id)
        {
            return new ProductDAL().GetProductById(id);
        }

        public int GetProductCount()
        {
            return new ProductDAL().GetProductCount();
        }

        public List<PRODUCT_DETAILS> ProductToSale()
        {
            return new ProductDAL().ProductForSale();
        }
    }
}
