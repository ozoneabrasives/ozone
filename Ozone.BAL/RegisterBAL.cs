﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ozone.DAL;

namespace Ozone.BAL
{
    public class RegisterBAL
    {
        public long Pk_Register_Id { get; set; }
        public string Register_Type { get; set; }
        public string Company_Name { get; set; }
        public string Permanent_Add { get; set; }
        public string Correspond_Add { get; set; }
        public string Company_Reg_No { get; set; }
        public string VAT_No { get; set; }
        public string Email_ID { get; set; }
        public string Landline_No { get; set; }
        public string Mobile_No { get; set; }
        public string Fax { get; set; }
        public string Auth_Officer_Name { get; set; }
        public string Delivery_Type { get; set; }
        public string Delivery_Detail { get; set; }
        public string Payment_Terms { get; set; }
        public Nullable<bool> Is_Approved { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<long> Fk_Country_Id { get; set; }
        public Nullable<long> Fk_State_Id { get; set; }
        public string Fk_City_Id { get; set; }
        public string User_Name { get; set; }
        public string User_Password { get; set; }
        public bool Have_Credit { get; set; }
        public string Customer_ID { get; set; }
        public string User_Type { get; set; }
        public int Currecnyid { get; set; }
        public long InsertRegister(RegisterBAL regUsr)
        {
            try
            {
                
                RegisterDAL dalReg = new RegisterDAL();
                USER_REGISTRATION usr = new USER_REGISTRATION();
                usr.Auth_Officer_Name = regUsr.Auth_Officer_Name;
                usr.Company_Name = regUsr.Company_Name;
                usr.Company_Reg_No = regUsr.Company_Reg_No;
                usr.Correspond_Add = regUsr.Correspond_Add;
                
                usr.Created_Date = System.DateTime.Now;
                usr.Delivery_Detail = regUsr.Delivery_Detail;
                usr.Delivery_Type = regUsr.Delivery_Type;
                usr.Email_ID = regUsr.Email_ID;
                usr.Fax = regUsr.Fax;
                usr.Fk_City_Id = regUsr.Fk_City_Id;
                usr.Fk_Country_Id = regUsr.Fk_Country_Id;
                usr.Fk_State_Id = regUsr.Fk_State_Id;
                usr.Is_Approved = regUsr.Is_Approved;
                usr.Landline_No = regUsr.Landline_No;
                usr.Mobile_No = regUsr.Mobile_No;
                usr.Payment_Terms = regUsr.Payment_Terms;
                usr.Permanent_Add = regUsr.Permanent_Add;
                usr.Register_Type = regUsr.Register_Type;
                usr.User_Name = regUsr.User_Name;
                usr.User_Password = regUsr.User_Password;
                usr.VAT_No = regUsr.VAT_No;
                usr.User_Type = regUsr.User_Type;
                usr.Have_Credit = regUsr.Have_Credit;
                usr.Customer_ID = regUsr.Customer_ID;
                usr.CurrencyID = regUsr.Currecnyid;
                if (regUsr.Pk_Register_Id == 0)
                    return dalReg.InsertRegister(usr);
                else
                {
                    usr.Pk_Register_Id = regUsr.Pk_Register_Id;
                    return dalReg.UpdateRegister(usr);
                }
            }
            catch (Exception ex)
            {                
                throw ex;
            }
            
        }

        public USER_REGISTRATION CheckUser(RegisterBAL bal)
        {
            try
            {
                RegisterDAL dalReg = new RegisterDAL();
                USER_REGISTRATION usr = new USER_REGISTRATION();
                usr.User_Password = bal.User_Password;
                usr.User_Name = bal.User_Name;
                return dalReg.CheckUser(usr);
            }
            catch (Exception ex)
            {                
                throw ex;
            }
        }


        public List<USER_REGISTRATION> ListUsers(int display, int lenght,string usertype)
        {
            return new RegisterDAL().ListUsers(display, lenght,usertype);
        }

        public int GetUsersCount(string usertype)
        {
            return new RegisterDAL().GetUsersCount(usertype);
        }

        public USER_REGISTRATION BindById(long id)
        {
            return new RegisterDAL().GetById(id);
        }

        public List<USER_REGISTRATION> UpdateCredit(List<USER_REGISTRATION> lst)
        {
            return new RegisterDAL().UpdateCredit(lst);
        }
        public List<USER_REGISTRATION> DDLVendor()
        {
            return new RegisterDAL().DDLVendor();
        }
        public List<USER_REGISTRATION> getuserbyids(List<long> lstlong)
        {
            return new RegisterDAL().getuserlistbyId(lstlong);
        }
        public USER_REGISTRATION getuserByemail(string email)
        {
            return new RegisterDAL().GetUserByEmail(email);
        }
        
    }
}
