﻿using Ozone.DAL;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ozone.BAL
{
    public class PurchaseOrderBAL
    {
        public long Pk_PurchaseOrder_Id { get; set; }
        public string PO_Date { get; set; }
        public string PO_Number { get; set; }
        public string Payment_Terms { get; set; }
        public string Transport { get; set; }
        public string Ship_Name { get; set; }
        public string Ship_Address { get; set; }
        public string Ship_Compny { get; set; }
        public string Ship_Country { get; set; }
        public string Ship_City { get; set; }
        public string Ship_State { get; set; }
        public string Ship_Zip_code { get; set; }
        public Nullable<long> Ship_Phone { get; set; }
        public Nullable<bool> Document_Req { get; set; }
        public string Shipment_Detail { get; set; }
        public Nullable<decimal> Grand_Total { get; set; }
        public string Checked_By { get; set; }
        public Nullable<long> Created_by { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<long> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public Nullable<bool> Is_Deleted { get; set; }
        public string PO_Status { get; set; }
        public string DocumentDetais { get; set; }
        public string Shipper { get; set; }
        public string Consignee { get; set; }
        public string NotifyParty { get; set; }
        public string PortLoading { get; set; }
        public string PortDischarge { get; set; }
        public string PortDestination { get; set; }
        public Nullable<bool> IsPartial { get; set; }
        public Nullable<bool> TransShip { get; set; }
        public Nullable<System.DateTime> LstDateofShip { get; set; }
        public string DeliveryTerms { get; set; }
        public string Note { get; set; }


        public Nullable<long> Fk_Vendor_Id { get; set; } 

        // public string IsPDF { get; set; }
        public List<PURCHASEORDER_ITEMS> lstPOI { get; set; }

        public List<PURCHASE_ORDER> ListPO(int display, int lenght, long id, string type, string startdate, string enddate, int vid, string ponumber)
        {
            return new PurchaseOrderDAL().ListPO(display, lenght, id,type,startdate,enddate,vid,ponumber);
        }
        public List<PURCHASEORDER_ITEMS> ListGRN(int display, int lenght, long id)
        {
            return new PurchaseOrderDAL().ListGRN(display, lenght, id);
        }
        public int GetGrnCount(long id)
        {
            return new PurchaseOrderDAL().GetGRNCount(id);
        }

        public static string InsertPO(PurchaseOrderBAL balPO)
        {
            try
            {
                DateTime date = DateTime.Parse(balPO.PO_Date, (IFormatProvider)CultureInfo.InvariantCulture);// DateTime.ParseExact(balPO.PO_Date, "dd/MM/yyyy", null);
                PurchaseOrderDAL dalC = new PurchaseOrderDAL();
                PURCHASE_ORDER PO = new PURCHASE_ORDER();
                PO.Checked_By = balPO.Checked_By;
                PO.Document_Req = balPO.Document_Req;
                PO.Grand_Total = balPO.Grand_Total;
                PO.Payment_Terms = balPO.Payment_Terms;
                PO.PO_Date = date;/// Convert.ToDateTime(balPO.PO_Date);
                PO.PO_Number = balPO.PO_Number;
                PO.Ship_Address = balPO.Ship_Address;
                PO.Ship_City = balPO.Ship_City;
                PO.Ship_Compny = balPO.Ship_Compny;
                PO.Ship_Country = balPO.Ship_Country;
                PO.Ship_Name = balPO.Ship_Name;
                PO.Ship_Phone = balPO.Ship_Phone;
                PO.Ship_State = balPO.Ship_State;
                PO.Ship_Zip_code = balPO.Ship_Zip_code;
                PO.Shipment_Detail = balPO.Shipment_Detail;
                PO.Transport = balPO.Transport;
                PO.PO_Status = "Pending";
                PO.Created_by = 1;
                PO.Created_Date = System.DateTime.Now;
                PO.DeliveryTerms = balPO.DeliveryTerms;
                PO.Note = balPO.Note;
                PO.Fk_Vendor_Id = balPO.Fk_Vendor_Id;
                PO.Is_Deleted = false;
                PO.PURCHASEORDER_ITEMS = balPO.lstPOI;
                return dalC.InsertPO(PO);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string EditPO(PurchaseOrderBAL balPO)
        {
            try
            {
                PurchaseOrderDAL dalC = new PurchaseOrderDAL();
                PURCHASE_ORDER PO = new PURCHASE_ORDER();
                PO.Pk_PurchaseOrder_Id = balPO.Pk_PurchaseOrder_Id;
                PO.Checked_By = balPO.Checked_By;
                PO.Document_Req = balPO.Document_Req;
                PO.Grand_Total = balPO.Grand_Total;
                PO.Payment_Terms = balPO.Payment_Terms;
                PO.PO_Date = Convert.ToDateTime(balPO.PO_Date);
                PO.PO_Number = balPO.PO_Number;
                PO.Ship_Address = balPO.Ship_Address;
                PO.Ship_City = balPO.Ship_City;
                PO.Ship_Compny = balPO.Ship_Compny;
                PO.Ship_Country = balPO.Ship_Country;
                PO.Ship_Name = balPO.Ship_Name;
                PO.Ship_Phone = balPO.Ship_Phone;
                PO.Ship_State = balPO.Ship_State;
                PO.Ship_Zip_code = balPO.Ship_Zip_code;
                PO.Shipment_Detail = balPO.Shipment_Detail;
                PO.Transport = balPO.Transport;
                PO.PO_Status = balPO.PO_Status;
                PO.Modified_Date = System.DateTime.Now;
                PO.Modified_By = balPO.Modified_By;
                PO.Fk_Vendor_Id = balPO.Fk_Vendor_Id;
                PO.Shipper = balPO.Shipper;
                PO.Consignee = balPO.Consignee;
                PO.NotifyParty = balPO.NotifyParty;
                PO.PortLoading = balPO.PortLoading;
                PO.PortDischarge = balPO.PortDischarge;
                PO.PortDestination = balPO.PortDestination;
                PO.IsPartial = balPO.IsPartial;
                PO.TransShip = balPO.TransShip;
                PO.LstDateofShip = balPO.LstDateofShip;
                PO.DeliveryTerms = balPO.DeliveryTerms;
                PO.Note = balPO.Note;

                return dalC.EditPO(PO);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string DeletePO(long id)
        {
            return new PurchaseOrderDAL().DeletePO(id);
        }


        public PURCHASE_ORDER GetPOById(long id)
        {
            return new PurchaseOrderDAL().GetPOById(id);
        }

        public int GetPOCount(long id, string type, string startdate, string enddate, int vid, string ponumber)
        {
            return new PurchaseOrderDAL().GetPOCount(id,type,startdate,enddate,vid,ponumber);
        }


        public List<PURCHASEORDER_ITEMS> UpdateStatus(List<PURCHASEORDER_ITEMS> lst)
        {

            return new PurchaseOrderDAL().UpdateStatus(lst);
        }

        public string UpdatePOStatus(string number,string status)
        {
            return new PurchaseOrderDAL().UpdatePOStatus(number,status);
        }

    }
}
