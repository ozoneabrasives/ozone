﻿using Ozone.DAL;
using Ozone.DAL.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ozone.BAL
{
    public class CartItemBAL
    {
        public long Pk_Shopping_Cart_Item_Id { get; set; }
        public Nullable<long> Fk_Shopping_Cart_Id { get; set; }
        public Nullable<long> Fk_Product_Id { get; set; }
        public Nullable<long> Fk_Package_Id { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<long> Create_By { get; set; }
        public System.DateTime Create_Date { get; set; }
        public Nullable<long> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public Nullable<bool> Is_Deleted { get; set; }
        public Nullable<decimal> Unit_Price { get; set; }
        public Nullable<decimal> Total_Amt { get; set; }
        public Nullable<bool> Is_Extra { get; set; }

        public string Product_Code { get; set; }
        public string Product_Description { get; set; }


        public virtual PACKAGE_MASTER PACKAGE_MASTER { get; set; }
        public virtual PRODUCT_DETAILS PRODUCT_DETAILS { get; set; }
        public virtual SHOPPING_CART SHOPPING_CART { get; set; }

        public static string InsertCartItem(ShoppingCartBAL sc)
        {
            try
            {

                OzoneDBEntities dbcon = new OzoneDBEntities();
                CartItemsDAL dalC = new CartItemsDAL();
                List<SHOPPING_CART_ITEMS> lstSCI = new List<SHOPPING_CART_ITEMS>();
                SHOPPING_CART PO = new SHOPPING_CART();
                //DateTime date = DateTime.Parse(DateTime.Now.ToShortDateString(), (IFormatProvider)CultureInfo.InvariantCulture);
                PO.Create_By = sc.Create_By;
                PO.Card_Cvv = sc.Card_Cvv;
                PO.Card_Detail = sc.Card_Detail;
                PO.Card_Exp_Month = sc.Card_Exp_Month;
                PO.Card_Exp_Year = sc.Card_Exp_Year;
                PO.Card_No = sc.Card_No;
                PO.Del_Address = sc.Del_Address;
                PO.Del_City = sc.Del_City;
                PO.Del_Country = sc.Del_Country;
                PO.Del_State = sc.Del_State;
                PO.Del_Zipcode = sc.Del_Zipcode;
                PO.Full_Name = sc.Full_Name;
                PO.Is_Deleted = false;
                PO.Name_On_Card = sc.Name_On_Card;
                PO.Order_Status = sc.Order_Status;
                PO.Create_Date = System.DateTime.Now;
                PO.Payment_Amt = sc.Payment_Amt;
                PO.Payment_Card = sc.Payment_Card;
                PO.Payment_Status = sc.Payment_Status;
                PO.Phone_No = sc.Phone_No;
                PO.Order_Number = sc.Order_Number;
                PO.VAT_Amount = sc.VAT_Amount;
                PO.Shipp_Rate = sc.ShipRate;
                PO.Paypal_Transaction_Id = sc.Paypalid;
                foreach (var item in sc.lstItems)
                {
                    SHOPPING_CART_ITEMS POItem = new SHOPPING_CART_ITEMS();
                    POItem.Is_Deleted = false;
                    POItem.Create_Date = System.DateTime.Now;
                    //POItem.Fk_PO_Id = PO.Pk_PurchaseOrder_Id;
                    POItem.Fk_Product_Id = item.Fk_Product_Id;
                    POItem.Create_By = item.Create_By;
                    POItem.Fk_Package_Id = item.Fk_Package_Id;
                    POItem.Quantity = item.Quantity;
                    POItem.SHOPPING_CART = PO;
                    POItem.Is_Extra = item.Is_Extra;
                    POItem.Unit_Price = item.Unit_Price;
                    POItem.Total_Amt = item.Total_Amt;
                    lstSCI.Add(POItem);
                }
                return dalC.InsertCartItem(lstSCI);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string UpdateCart(ShoppingCartBAL scBAl)
        {
            try
            {
                SHOPPING_CART PO = new SHOPPING_CART();
                PO.Pk_UserCart_Id = scBAl.Pk_UserCart_Id;
                //DateTime date = DateTime.Parse(DateTime.Now.ToShortDateString(), (IFormatProvider)CultureInfo.InvariantCulture);
                PO.Modified_By = scBAl.Modified_By;
                PO.Card_Cvv = scBAl.Card_Cvv;
                PO.Card_Detail = scBAl.Card_Detail;
                PO.Card_Exp_Month = scBAl.Card_Exp_Month;
                PO.Card_Exp_Year = scBAl.Card_Exp_Year;
                PO.Card_No = scBAl.Card_No;
                PO.Del_Address = scBAl.Del_Address;
                PO.Del_City = scBAl.Del_City;
                PO.Del_Country = scBAl.Del_Country;
                PO.Del_State = scBAl.Del_State;
                PO.Del_Zipcode = scBAl.Del_Zipcode;
                PO.Full_Name = scBAl.Full_Name;
                PO.Is_Deleted = false;
                PO.Name_On_Card = scBAl.Name_On_Card;
                PO.Order_Status = scBAl.Order_Status;
                PO.Modified_Date = System.DateTime.Now;
                PO.Payment_Amt = scBAl.Payment_Amt;
                PO.Payment_Card = scBAl.Payment_Card;
                PO.Payment_Status = scBAl.Payment_Status;
                PO.Phone_No = scBAl.Phone_No;
                PO.Order_Number = scBAl.Order_Number;
                return ShoppingCartDAL.UpdateCart(PO);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        public static string AddExtraItems(List<SHOPPING_CART_ITEMS> lst)
        {
            return new CartItemsDAL().InsertCartItem(lst);
        }

        public List<SHOPPING_CART_ITEMS> GetList(int display, int lenght, string userType, long id)
        {
            return new CartItemsDAL().ListCart(display, lenght, userType, id);
        }

        public int GetExtraCount(string userType, long id)
        {
            return new CartItemsDAL().GetExtraCount(userType, id);
        }

        public List<SHOPPING_CART_ITEMS> GetListById(long id)
        {
            return new CartItemsDAL().GetListById(id);
        }

        public SHOPPING_CART_ITEMS GetItemById(long id)
        {
            return new CartItemsDAL().GetCartItemById(id);
        }

        public static string EditItems(SHOPPING_CART_ITEMS cart)
        {
            return  CartItemsDAL.EditCart(cart);
        }

        public static string AddItem(SHOPPING_CART_ITEMS item)
        {
            return new CartItemsDAL().InsertCartItem(item);
        }

        public string DeleteItem(long id)
        {
            return new CartItemsDAL().DeleteItem(id);
        }
        public List<SHOPPING_CART_ITEMS> UpdateInventory(List<SHOPPING_CART_ITEMS> lstitems)
        {
            return new CartItemsDAL().LpdateItemToInventory(lstitems);
        }
        public List<SHOPPING_CART_ITEMS> ListGRN(int display, int lenght, long id)
        {
            return new CartItemsDAL().ListGRN(display, lenght, id);
        }
        public List<ListofinvoceModel> ListInvoice(int disply, int lenght, long id)
        {
            return new CartItemsDAL().ListInvoice(disply,lenght,id);
        }
        public int GetGrnCount(long id)
        {
            return new CartItemsDAL().GetGRNCount(id);
        }
        public int GetInvoiceCount(long id)
        {
            return new CartItemsDAL().GetInvoiceCount(id);
        }
    }
}
