﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ozone.DAL;
using Ozone.DAL.Models;

namespace Ozone.BAL
{
  public  class InvoiceBAL
    {
      public static InvoiceTab GetInvByID(long invid)
      {
          return new InvoiceDAL().Getinvoicebyid(invid);
      }
      public static string Addpayment(InvoiceTab invtab)
      {
          return new InvoiceDAL().AddPayment(invtab);
      }
      public static List<INV_PAYMENT_TAB> lstofpayment(long invid)
      {
          return new InvoiceDAL().ListofPayments(invid);
      }
      public static List<ListofinvoceModel> GetStatement(int display, int lenght)
      {
          return new InvoiceDAL().GetStatement(display, lenght);
      }
      public static int GetStatementcount()
      {
          return new InvoiceDAL().Statementcount();
      }
      public static List<ListofinvoceModel> GetListofinvbydate()
      {
          return new InvoiceDAL().Getexpringinvoice();
      }
    }
}
