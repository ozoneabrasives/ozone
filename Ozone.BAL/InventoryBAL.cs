﻿using Ozone.DAL;
using Ozone.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ozone.BAL
{
   public class InventoryBAL
    {
       public  List<InventoryDetailsModel> Getlist(int display, int lenght)
       {
           return new InvenortyDAL().ListInventory(display,lenght);
       }

       public List<INVENTORY> GetDeadStock(int display, int lenght)
       {
           return new InvenortyDAL().ListDeadStock(display,lenght);
       }

       public int CountDeadStock()
       {
           return new InvenortyDAL().CountStock();
       }

       public PURCHASEORDER_ITEMS TransferStock(long id,int qty)
       {
           return new InvenortyDAL().TransferStock(id,qty);
       }
       public string MoveDeadStocktoNull()
       {
           return new InvenortyDAL().MoveDeadStocktoNull();
       }
      
    }
}
