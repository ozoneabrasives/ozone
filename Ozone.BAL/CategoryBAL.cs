﻿using Ozone.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ozone.BAL
{
    public class CategoryBAL
    {
        public long Pk_Category_Id { get; set; }
        public string Category_Name { get; set; }
        public string Category_Description { get; set; }
        public Nullable<long> Created_by { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<long> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public Nullable<bool> Is_Deleted { get; set; }

        public List<CATEGORY_MASTER> ListCategory(int display, int lenght, string search)
        {
            return new CategoryDAL().ListCategory(display, lenght,search);
        }

        public static string InsertCategory(CategoryBAL balCategory)
        {
            try
            {
                CategoryDAL dalC = new CategoryDAL();
                CATEGORY_MASTER Category = new CATEGORY_MASTER();
                Category.Category_Name = balCategory.Category_Name;
                Category.Category_Description = balCategory.Category_Description;
                Category.Is_Deleted = false;
                Category.Created_Date = System.DateTime.Now;
                Category.Created_by = balCategory.Created_by;
                return dalC.InsertCategory(Category);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string EditCategory(CategoryBAL balCategory)
        {
            try
            {
                CategoryDAL dalC = new CategoryDAL();
                CATEGORY_MASTER Category = new CATEGORY_MASTER();
                Category.Pk_Category_Id = balCategory.Pk_Category_Id;
                Category.Category_Description = balCategory.Category_Description;
                Category.Category_Name = balCategory.Category_Name;
                Category.Modified_By = balCategory.Modified_By;
                Category.Modified_Date = System.DateTime.Now;
                return dalC.EditCategory(Category);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string DeleteCategory(long id)
        {
            return new CategoryDAL().DeleteCategory(id);
        }

        public List<CATEGORY_MASTER> DDLCategory()
        {
            try
            {
                CategoryDAL dal = new CategoryDAL();
                return dal.DDLCategory();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CATEGORY_MASTER GetCategoryById(long id)
        {
            return new CategoryDAL().GetCategoryById(id);
        }

        public int GetCategoryCount(string search)
        {
            return new CategoryDAL().GetCategoryCount(search);
        }
    }
}
