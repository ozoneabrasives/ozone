﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ozone.DAL;

namespace Ozone.BAL
{
    public class StateBAL
    {
        public long Pk_State_Id { get; set; }
        public Nullable<long> Fk_Country_Id { get; set; }
        public string State_Name { get; set; }
        public string State_Code { get; set; }
        public Nullable<long> Created_by { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<long> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public Nullable<bool> Is_Deleted { get; set; }

        public List<STATE_MASTER> ListState(int display, int lenght,string search)
        {
            return new StateDAL().ListState(display, lenght,search);
        }

        public static string InsertState(StateBAL balState)
        {
            try
            {
                StateDAL dalC = new StateDAL();
                STATE_MASTER State = new STATE_MASTER();
                State.State_Code = balState.State_Code;
                State.State_Name = balState.State_Name;
                State.Is_Deleted = false;
                State.Created_Date = System.DateTime.Now;
                State.Fk_Country_Id = balState.Fk_Country_Id;
                return dalC.InsertState(State);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string EditState(StateBAL balState)
        {
            try
            {
                StateDAL dalC = new StateDAL();
                STATE_MASTER State = new STATE_MASTER();
                State.Pk_State_Id = balState.Pk_State_Id;
                State.State_Code = balState.State_Code;
                State.State_Name = balState.State_Name;
                State.Fk_Country_Id = balState.Fk_Country_Id;
                State.Created_Date = System.DateTime.Now;
                return dalC.EditState(State);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string DeleteState(long id)
        {
            return new StateDAL().DeleteState(id);
        }

        public List<STATE_MASTER> DDLState(long id)
        {
            try
            {
                StateDAL dal = new StateDAL();
                return dal.DDLState(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public STATE_MASTER GetStateById(long id)
        {
            return new StateDAL().GetStateById(id);
        }

        public int GetStateCount(string search )
        {
            return new StateDAL().GetStateCount(search);
        }
    }
}
