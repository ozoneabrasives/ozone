﻿using Ozone.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ozone.BAL
{
    public class VatBAL
    {

        public long Pk_VAT_Id { get; set; }
        public string VAT_Percent1 { get; set; }
        public Nullable<long> Created_By { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<long> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }


        public List<VAT_PERCENT> ListVAT(int display, int lenght)
        {
            return new VatDAL().ListVAT(display, lenght);
        }

        public static string InsertVAT(VatBAL balVAT)
        {
            try
            {
                VatDAL dalC = new VatDAL();
                VAT_PERCENT VAT = new VAT_PERCENT();
                VAT.VAT_Percent1 = balVAT.VAT_Percent1;
                 
               
                VAT.Created_Date = System.DateTime.Now;
                VAT.Created_By = balVAT.Created_By;
                return dalC.InsertVAT(VAT);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string EditVAT(VatBAL balVAT)
        {
            try
            {
                VatDAL dalC = new VatDAL();
                VAT_PERCENT VAT = new VAT_PERCENT();
                VAT.Pk_VAT_Id = balVAT.Pk_VAT_Id;
                VAT.VAT_Percent1 = balVAT.VAT_Percent1;
               
                VAT.Modified_By = balVAT.Modified_By;
                VAT.Modified_Date = System.DateTime.Now;
                return dalC.EditVAT(VAT);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string DeleteVAT(long id)
        {
            return new VatDAL().DeleteVAT(id);
        }

        public List<VAT_PERCENT> DDLVAT()
        {
            try
            {
                VatDAL dal = new VatDAL();
                return dal.DDLVAT();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public VAT_PERCENT GetVATById(long id)
        {
            return new VatDAL().GetVATById(id);
        }

        public int GetVATCount()
        {
            return new VatDAL().GetVATCount();
        }
    }
}
