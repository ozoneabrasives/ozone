﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ozone.DAL;

namespace Ozone.BAL
{
    public class CountryBAL
    {
        public long Pk_Country_Id { get; set; }
        public string Country_Name { get; set; }
        public string Country_Code { get; set; }
        public Nullable<long> Created_by { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<long> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public Nullable<bool> Is_Deleted { get; set; }

        public List<COUNTRY_MASTER> ListCountry(int display, int lenght,string Search)
        {
            return new CountryDAL().ListCountry(display,lenght,Search);
        }
        public int GetCountryCount(string Search)
        {
            return new CountryDAL().GetCountryCount(Search);
        }

        public static string InsertCountry(CountryBAL balCountry)
        {
            try
            {
                CountryDAL dalC = new CountryDAL();
                COUNTRY_MASTER country = new COUNTRY_MASTER();
                country.Country_Code = balCountry.Country_Code;
                country.Country_Name = balCountry.Country_Name;
                country.Created_Date = System.DateTime.Now;
                country.Created_by = balCountry.Created_by;
                country.Is_Deleted = false;
                return dalC.InsertCountry(country);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string EditCountry(CountryBAL balCountry)
        {
            try
            {
                CountryDAL dalC = new CountryDAL();
                COUNTRY_MASTER country = new COUNTRY_MASTER();
                country.Pk_Country_Id = balCountry.Pk_Country_Id;
                country.Country_Code = balCountry.Country_Code;
                country.Country_Name = balCountry.Country_Name;
                country.Modified_Date = System.DateTime.Now;
                country.Modified_By = balCountry.Modified_By;
                return dalC.EditCountry(country);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string DeleteCountry(long id)
        {
            return new CountryDAL().DeleteCountry(id);
        }

        public List<COUNTRY_MASTER> DDLCountry()
        {
            try
            {
                CountryDAL dal = new CountryDAL();
                return dal.DDLCountry();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public COUNTRY_MASTER GetCountryById(long id)
        {
            return new CountryDAL().GetCountryById(id);
        }
    }
}
