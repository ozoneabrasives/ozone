﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ozone.DAL;

namespace Ozone.BAL
{
  public  class TaxMasterBAL
    {
      public string Insert(TaxMaster objtax)
      {
          return new TaxMasterDAL().Insert(objtax);
      }
      public string update(TaxMaster objtax)
      {
          return new TaxMasterDAL().Update(objtax);
      }
      public TaxMaster Selectbyid(long id)
      {
          return new TaxMasterDAL().GetByid(id);
      }
      public List<TaxMaster> ListTax(int display, int lenght)
      {
          return new TaxMasterDAL().ListTax(display, lenght);
      }
      public int GetCount()
      {
          return new TaxMasterDAL().GetCount();
      }
      public decimal GetTaxByUserid(long userid, int qty)
      {
          return new TaxMasterDAL().GetTaxByUserid(userid,qty);
      }
    }
}
