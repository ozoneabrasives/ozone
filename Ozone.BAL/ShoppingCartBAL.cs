﻿using Ozone.DAL;
using Ozone.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ozone.BAL
{
    public class ShoppingCartBAL
    {
        public long Pk_UserCart_Id { get; set; }
        public long Fk_Product_Id { get; set; }
        public long Fk_Package_Id { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<long> Create_By { get; set; }
        public System.DateTime Create_Date { get; set; }
        public Nullable<long> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public Nullable<bool> Is_Deleted { get; set; }
        public decimal TAX { get; set; }
        public string Full_Name { get; set; }
        public string Del_Address { get; set; }
        public Nullable<long> Del_Country { get; set; }
        public Nullable<long> Del_State { get; set; }
        public string Del_City { get; set; }
        public string Del_Zipcode { get; set; }
        public string Phone_No { get; set; }
        public string Payment_Card { get; set; }
        public string Card_Detail { get; set; }
        public string Name_On_Card { get; set; }
        public Nullable<long> Card_No { get; set; }
        public Nullable<int> Card_Exp_Month { get; set; }
        public Nullable<int> Card_Exp_Year { get; set; }
        public Nullable<int> Card_Cvv { get; set; }

        public decimal Payment_Amt { get; set; }

        public string Order_Status { get; set; }
        public string Order_Note { get; set; }
        public string Order_Number { get; set; }
        public string Accepted_Note { get; set; }
        public string Rejected_Note { get; set; }
        public string Processed_Note { get; set; }
        public string Shipped_Note { get; set; }
        public string Delivery_Note { get; set; }

        public Nullable<decimal> VAT_Amount { get; set; }
        public decimal ShipRate { get; set; }

        public string Payment_Status { get; set; }
        public virtual COUNTRY_MASTER COUNTRY_MASTER { get; set; }
        public virtual STATE_MASTER STATE_MASTER { get; set; }

        public virtual PACKAGE_MASTER PACKAGE_MASTER { get; set; }
        public virtual PRODUCT_DETAILS PRODUCT_DETAILS { get; set; }

        public List<SHOPPING_CART_ITEMS> lstItems { get; set; }


        public string ProductCode { get; set; }
        public string PackageNM { get; set; }
        public string UnitNM { get; set; }

        public string ProductImg { get; set; }
        public int Price { get; set; }
        public int Total { get; set; }
        public string Paypalid { get; set; }
        public int unit { get; set; }

        public SHOPPING_CART GetById(long id)
        {
            return new ShoppingCartDAL().GetById(id);
        }

        public List<SHOPPING_CART> ListCart(int display, int lenght,string userType,long id,string status)
        {
            return new ShoppingCartDAL().ListCart(display, lenght,userType,id,status);
        }

        public List<SHOPPING_CART> ListExtraCart(int display, int lenght, string userType, long id)
        {
            return new ShoppingCartDAL().ListExtraCart(display, lenght, userType, id);
        }

        public List<SHOPPING_CART> ListCreditCart(int display, int lenght, string userType, long id)
        {
            return new ShoppingCartDAL().ListCreditCart(display, lenght, userType, id);
        }
        public List<SHOPPING_CART> getListByPaymentStatus()
        {
            return new ShoppingCartDAL().getListByPaymentStatus();
        }

        public static string InsertCart(ShoppingCartBAL balcart)
        {
            try
            {
                ShoppingCartDAL dalC = new ShoppingCartDAL();
                SHOPPING_CART scart = new SHOPPING_CART();
                scart.Fk_Package_Id = balcart.Fk_Package_Id;
                scart.Fk_Product_Id = balcart.Fk_Product_Id;
                scart.Is_Deleted = false;
                scart.Create_Date = System.DateTime.Now;
                scart.Quantity = balcart.Quantity;
                scart.Create_By = balcart.Create_By;
                return dalC.InsertCart(scart);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GetProductCount(string userType, long id, string status)
        {
            return new ShoppingCartDAL().GetProductCount(userType, id, status);
        }

        public string UpdateStatus(string number, string status,string notes,string pay)
        {
            return new ShoppingCartDAL().UpdateStatus(number, status,notes,pay);
        }

        public List<SHOPPING_CART> GetAccountSum(string srach, long? ByCNo, long? ByClient, string ByStatus)
        {
            var rest = new ShoppingCartDAL().GetSummary(srach, ByCNo, ByClient, ByStatus);
            return rest;
        }
        public int GetAccountCount(string srch, long? ByCNo, long? ByClient, string ByStatus)
        {
            return new ShoppingCartDAL().GetSummaryCount(srch,  ByCNo, ByClient, ByStatus);
        }

        public int GetExtraCount(string userType, long id)
        {
            return new ShoppingCartDAL().GetExtraCount(userType, id);
        }

        public int GetCreditCount(string userType, long id)
        {
            return new ShoppingCartDAL().GetCreditCount(userType, id);
        }
        public List<DeliveryOrderUpdateModel> GetlistbyGRN(long salesid)
        {
            return new CartItemsDAL().CartItemsByGRN(salesid);
        }
        public SHOPPING_CART GetcartByDo(string donumber)
        {
            return new ShoppingCartDAL().GetDobyNumber(donumber);
        }
    }
}
