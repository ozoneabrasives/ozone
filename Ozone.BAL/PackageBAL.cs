﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ozone.DAL;

namespace Ozone.BAL
{
    public class PackageBAL
    {
        public long Pk_Package_Id { get; set; }
        public Nullable<long> Fk_Product_Id { get; set; }
        public string Package_Title { get; set; }
        public string Package_Unit { get; set; }
        public int Package_Amt { get; set; }

        public Nullable<long> Created_by { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<long> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public Nullable<bool> Is_Deleted { get; set; }
        public Nullable<decimal> Cost_Price { get; set; }
        public Nullable<decimal> Sell_Price { get; set; }

        //public List<PRODUCT_DETAILS> lstProduct { get; set; }

        public List<PACKAGE_MASTER> ListPackage(int display, int lenght,string search)
        {
            return new PackageDAL().ListPackage(display, lenght, search);
        }

        public static string InsertPackage(PackageBAL balPackage)
        {
            try
            {
                PackageDAL dalC = new PackageDAL();
                PACKAGE_MASTER Package = new PACKAGE_MASTER();
                Package.Package_Title = balPackage.Package_Title;
                Package.Package_Unit = balPackage.Package_Unit;
                Package.Is_Deleted = false;
                Package.Created_Date = System.DateTime.Now;
                Package.Fk_Product_Id = balPackage.Fk_Product_Id;
                Package.Cost_Price = balPackage.Cost_Price;
                Package.Sell_Price = balPackage.Sell_Price;
                Package.Package_Amt = balPackage.Package_Amt;
                return dalC.InsertPackage(Package);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string EditPackage(PackageBAL balPackage)
        {
            try
            {
                PackageDAL dalC = new PackageDAL();
                PACKAGE_MASTER Package = new PACKAGE_MASTER();
                Package.Pk_Package_Id = balPackage.Pk_Package_Id;
                Package.Package_Title = balPackage.Package_Title;
                Package.Package_Unit = balPackage.Package_Unit;
                Package.Fk_Product_Id = balPackage.Fk_Product_Id;
                Package.Cost_Price = balPackage.Cost_Price;
                Package.Sell_Price = balPackage.Sell_Price;
                Package.Modified_Date = System.DateTime.Now;
                Package.Package_Amt = balPackage.Package_Amt;
                return dalC.EditPackage(Package);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string DeletePackage(long id)
        {
            return new PackageDAL().DeletePackage(id);
        }

        public List<PACKAGE_MASTER> DDLPackage()
        {
            try
            {
                PackageDAL dal = new PackageDAL();
                return dal.DDLPackage();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public PACKAGE_MASTER GetPackageById(long id)
        {
            return new PackageDAL().GetPackageById(id);
        }

        public int GetPackageCount()
        {
            return new PackageDAL().GetPackageCount();
        }

        public List<PACKAGE_MASTER> DDLUnits(string pack)
        {
            try
            {
                PackageDAL dal = new PackageDAL();
                return dal.DDLUnit(pack);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public object BindPrice(long id)
        {
            return new PackageDAL().BindPrice(id);
        }

        public object BindBPrice(long id)
        {
            return new PackageDAL().BindBPrice(id);
        }
        public List<PACKAGE_MASTER> GetPackageByProduct(long productid)
        {
            return new PackageDAL().GetPackageByProduct(productid);
        }
    }
}
