﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ozone.DAL;

namespace Ozone.BAL
{
  public  class ShipRateBAL
    {
      public string Insert(ShipRateTab objshirate)
      {
          return new ShipRateDAL().Insert(objshirate);

      }
      public string Update(ShipRateTab objshiprate)
      {
          return new ShipRateDAL().Update(objshiprate);
      }
      public ShipRateTab SelectByid(long id)
      {
          return new ShipRateDAL().GetBybyid(id);
      }
      public List<ShipRateTab> ListShipRate(int display, int lenght)
      {
          return new ShipRateDAL().ListShipRate(display, lenght);
      }
      public int GetCount()
      {
          return new ShipRateDAL().GetCount();
      }
    }

}
