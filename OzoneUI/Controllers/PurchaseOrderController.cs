﻿using Newtonsoft.Json;
using Ozone.BAL;
using Ozone.DAL;
using OzoneUI.Models;
using Rotativa.MVC;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;


namespace OzoneUI.Controllers
{
    public class PurchaseOrderController : Controller
    {
        //
        // GET: /PurchaseOrder/

        public ActionResult Index()
        {
            UsrRegisterModels modl = new UsrRegisterModels();
            modl.lstUSR = new RegisterBAL().ListUsers(0, 1000,"").Where(c =>  c.User_Type == "Vendor").ToList();
            return View(modl);
        }
        public ActionResult Create(long POid = 0)
         {
            PurchaseOrderItemsController.lstItems = new List<PURCHASEORDER_ITEMS>();
            Session["oidtems"] = null;

            if (POid != null)
            {
                POModels PO = new POModels();
                string PONo = "PO" + DateTime.Now.Year + PurchaseOrderDAL.GetLastId().ToString("D" + 5);
               
                
                PO.lstTransport =  new CommonClass().TransportList();
                PO.lstpartialpayment = new CommonClass().AllowedList();
               /// Random ran = new Random();
                string poNum = PONo; ///"PO_" + ran.Next();
                
                //if (POid != null)
                //{
                //    POItemModels POItem = new POItemModels();
                //    POItem.Fk_PO_Id = POid;
                //    POItem.lstProduct = new ProductBAL().DDLProduct();
                //    POItem.lstPackage = new PackageBAL().DDLPackage();
                //    if (POid > 0)
                //    {
                //        PURCHASEORDER_ITEMS balPOItem = new POItemsBAL().GetPOItemById(POid);
                //        POItem.lstUnits = new PackageBAL().DDLUnits(balPOItem.Packaging);
                //        POItem.Pk_PO_Item_Id = POid;
                //        POItem.Is_Deleted = false;
                //        POItem.Created_Date = System.DateTime.Now;
                //        POItem.Fk_PO_Id = balPOItem.Fk_PO_Id;
                //        POItem.Fk_Product_Id = balPOItem.Fk_Product_Id;
                //        POItem.Product_Description = balPOItem.Product_Description;
                //        POItem.Packaging = balPOItem.Packaging;
                //        POItem.Product_Code = balPOItem.Product_Code;
                //        POItem.Total_Amt = balPOItem.Total_Amt;
                //        POItem.Total_Qty = balPOItem.Total_Qty;
                //        POItem.Unit_Price = balPOItem.Unit_Price;
                //        POItem.Units = balPOItem.Units;
                //        return PartialView(POItem);
                //    }
                //    else
                //    {
                //        POItem.lstUnits = new PackageBAL().DDLUnits("");
                //        return PartialView(POItem);
                //    }
                //}

                //PO.lstProduct = new ProductBAL().DDLProduct();
                PO.lstCountry = new CountryBAL().DDLCountry();
                PO.lstState = new List<STATE_MASTER>();
                PO.lstVendor = new RegisterBAL().DDLVendor();
                if (POid > 0)
                {
                    PURCHASE_ORDER balPO = new PurchaseOrderBAL().GetPOById(POid);
                    PO.lstState = new StateBAL().DDLState(Convert.ToInt32(balPO.Ship_Country));
                    PO.Pk_PurchaseOrder_Id = POid;
                    PO.Checked_By = balPO.Checked_By;
                    PO.Document_Req = balPO.Document_Req;
                    PO.Grand_Total = balPO.Grand_Total;
                    PO.Payment_Terms = balPO.Payment_Terms;
                    PO.PO_Date = balPO.PO_Date.ToShortDateString();
                    PO.PO_Number = balPO.PO_Number;
                    PO.Ship_Address = balPO.Ship_Address;
                    PO.Ship_City = balPO.Ship_City;
                    PO.Ship_Compny = balPO.Ship_Compny;
                    PO.Ship_Country = balPO.Ship_Country;
                    PO.Ship_Name = balPO.Ship_Name;
                    PO.Ship_Phone = balPO.Ship_Phone.ToString();
                    PO.Ship_State = balPO.Ship_State;
                    PO.Ship_Zip_code = balPO.Ship_Zip_code;
                    PO.Shipment_Detail = balPO.Shipment_Detail;
                    PO.Transport = balPO.Transport;
                    PO.PO_Status = balPO.PO_Status;
                    PO.DocumentDetais = balPO.DocumentDetais;
                    PO.Shipper = balPO.Shipper;
                    PO.Consignee = balPO.Consignee;
                    PO.NotifyParty = balPO.NotifyParty;
                    PO.PortLoading = balPO.PortLoading;
                    PO.PortDischarge = balPO.PortDischarge;
                    PO.PortDestination = balPO.PortDestination;
                    PO.IsPartial = balPO.IsPartial;
                    PO.TransShip = balPO.TransShip;
                    PO.LstDateofShip = balPO.LstDateofShip.Value.ToShortDateString();
                    PO.DeliveryTerms = balPO.DeliveryTerms;



                    return PartialView(PO);
                }
                else
                {
                    DateTime dt = System.DateTime.Now;
                    // Sets the CurrentCulture property to U.S. English.
                   // Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                    // Displays dt, formatted using the ShortDatePattern
                    // and the CurrentThread.CurrentCulture.


                    // Creates a CultureInfo for German in Germany.
                    // CultureInfo ci = new CultureInfo("de-DE");
                    //DateTime dt = DateTime.ParseExact(DateTime.Now.ToShortDateString(), "mm/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    //PO.PO_Date = Convert.ToDateTime(dt.ToString("d"));// DateTime.Parse(DateTime.Now.ToShortDateString(), (IFormatProvider)CultureInfo.InvariantCulture);
                    PO.PO_Number = poNum;
                   

                    PO.PO_Date = dt.ToString("d");

                    return PartialView(PO);
                }

            }
            else
            {
                return PartialView();

            }

        }


        public ActionResult ListofGRN(long Poid = 0)
        {
            return View();
        }

        public ContentResult GetGRN(int? start, int length, string startdate, string enddate, int poid)
        {
            int iDisplayStart = start == null ? 0 : start.Value;

            var obj = new PurchaseOrderBAL().ListGRN(iDisplayStart, length, poid);
            //var total = new PurchaseOrderBAL().GetPOCount(Convert.ToInt64(Session["UserId"].ToString()), Convert.ToString(Session["UserType"]));
            var total = new PurchaseOrderBAL().GetGrnCount(poid);

            DatatablePaggeing d = new DatatablePaggeing();
            d.aaData = obj;
            d.iTotalDisplayRecords = total;
            d.iTotalRecords = total;
            d.sEcho = "";

            var list = JsonConvert.SerializeObject(d,
            Formatting.None,
            new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            });

            return Content(list, "application/json");
           // return new JsonResult { Data = d, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public ContentResult GetPO(int? start, int length, string startdate, string enddate, int vid, string ponumber)
        {
            int iDisplayStart = start == null ? 0 : start.Value;

            var obj = new PurchaseOrderBAL().ListPO(iDisplayStart, length, Convert.ToInt64(Session["UserId"].ToString()),Convert.ToString(Session["UserType"]),startdate,enddate,vid,ponumber);
            //var total = new PurchaseOrderBAL().GetPOCount(Convert.ToInt64(Session["UserId"].ToString()), Convert.ToString(Session["UserType"]));
            var total = new PurchaseOrderBAL().GetPOCount(Convert.ToInt64(Session["UserId"].ToString()), Convert.ToString(Session["UserType"]), startdate, enddate, vid, ponumber);

            DatatablePaggeing d = new DatatablePaggeing();
            d.aaData = obj;
            d.iTotalDisplayRecords = total;
            d.iTotalRecords = total;
            d.sEcho = "";

            var list = JsonConvert.SerializeObject(d,
            Formatting.None,
            new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            });
         // retunr  Newtonsoft.Json.JsonConvert.SerializeObject(obj)
            return Content(list, "application/json");
           // return new JsonResult { Data = d, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public ActionResult Edit(long POid)
        {
            return PartialView(new PurchaseOrderBAL().GetPOById(POid));
        }

        public ContentResult Delete(string id)
        {
            string suc;
            suc = new PurchaseOrderBAL().DeletePO(Convert.ToInt64(id));
            if (suc == "Deleted")
            {
                var getpo = new PurchaseOrderBAL().GetPOById(Convert.ToInt64(id));
                string details = "Hi, <br/> Purchase Order # " + getpo.PO_Number + " is Deleted Successfully";
                Utility.SendEmaildynamic("admin@ozoneabrasives.co.uk", "Admin", "PO is Deleted", details);
                //return new JsonResult { Data = new { status = true, message = "PO deleted successfully" } };
                return Content("PO deleted successfully", "application/text");
            }
            else
            {
                //return new JsonResult { Data = new { status = false, message = "Oops!! Some error occured" } };
                return Content("Oops!! Some error occured", "application/text");
            }
        }
        [HttpPost]
        // [ValidateAntiForgeryToken]
        public ActionResult SavePO(PurchaseOrderBAL PO)
        {
            try
            {

                PurchaseOrderItemsController.lstItems = (List<PURCHASEORDER_ITEMS>)Session["oidtems"];
                string suc;
                if (PO.Pk_PurchaseOrder_Id > 0)
                {
                    PO.Modified_By = Convert.ToInt64(Session["UserId"]);
                    suc = new PurchaseOrderBAL().EditPO(PO);
                }
                else
                {
                    PO.Created_by = Convert.ToInt64(Session["UserId"]);
                    var tot = PurchaseOrderItemsController.lstItems.Sum(c => c.Total_Amt);
                    //string st =  Request.QueryString["Ext"];
                    //string st1 = Request.QueryString["POid"];
                    //if (Request.QueryString["Ext"] != null)
                    //{
                    //    PO.PO_Status = "Extra";
                    //}
                    //else
                    //{
                    //    PO.PO_Status = "Pending";
                    //}
                    PO.lstPOI = PurchaseOrderItemsController.lstItems;
                    PO.Grand_Total = tot;
                    suc = POItemsBAL.InsertPOItem(PO); //PurchaseOrderBAL.InsertPO(PO);
                }
                if (suc == "Success")
                {
                    #region PDF Generate Portion
                    string FilePath = "";
                    string FullPath = Path.Combine(Server.MapPath("/PO_Generate/"), PO.Ship_Name + "/" + PO.PO_Number);
                    FilePath = Path.Combine(FullPath, PO.PO_Number + ".pdf");
                    DateTime date = DateTime.ParseExact(PO.PO_Date,
                                  "dd/MM/yyyy",
                                  CultureInfo.InvariantCulture);
                    POModels po = new POModels();
                    var vendor = new RegisterBAL().BindById(PO.Fk_Vendor_Id.Value);

                    po.Grand_Total = PO.Grand_Total;
                    po.Ship_Name = vendor.Company_Name;
                    po.Ship_Address = vendor.Correspond_Add;
                    po.Checked_By = PO.Checked_By;
                    po.Document_Req = PO.Document_Req;
                    po.Payment_Terms = PO.Payment_Terms;
                    po.PO_Date = date.ToShortDateString();
                    po.PO_Number = PO.PO_Number;
                    po.Ship_City = vendor.Fk_City_Id;
                    po.Ship_Compny = vendor.Company_Name;
                    po.Ship_Country = new CountryBAL().GetCountryById(vendor.Fk_Country_Id.Value).Country_Name;
                    po.Ship_Phone = vendor.Mobile_No;
                    po.Ship_State = new StateBAL().GetStateById(vendor.Fk_State_Id.Value).State_Name;
                   
                    po.Shipment_Detail = PO.Shipment_Detail;
                    po.DeliveryTerms = PO.DeliveryTerms;
                    po.Transport = PO.Transport;
                    po.Consignee = PO.Consignee;
                    po.NotifyParty = PO.NotifyParty;
                    po.PortLoading = PO.PortLoading;
                    po.DocumentDetais = PO.DocumentDetais;
                    po.Payment_Terms = PO.Payment_Terms;
                    po.Note = po.Note;
                    po.amountinnumber = NumberToWords(Convert.ToInt32(PO.Grand_Total));

                    po.cureencyName = new CurrencyBAL().GetCurrencybyid(PurchaseOrderItemsController.lstItems.FirstOrDefault().CurrencyId.Value).CurrencyName;
                    po.lstPOI = PurchaseOrderItemsController.lstItems;
                    var actionResult = new ViewAsPdf("GeneratePDF", po);

                    var byteArray = actionResult.BuildPdf(ControllerContext);
                    Directory.CreateDirectory(FullPath);
                    var fileStream = new FileStream(FilePath, FileMode.Create, FileAccess.Write);
                    fileStream.Write(byteArray, 0, byteArray.Length);
                    fileStream.Close();
                    PurchaseOrderItemsController.lstItems = null;
                    #endregion
                    #region Mail Portion
                 

                    #endregion
                    string details = "Hi, <br/> Purchase Order # " + PO.PO_Number + " is Created Successfully";
                    Utility.SendMail("po@ozoneabrasives.co.uk", FilePath,"Vendor",PO);
                    return new JsonResult { Data = new { status = true, message = "PO added successfully" } };
                }
                else if (suc == "Updated")
                {
                    string details = "Hi, <br/> Purchase Order # " + PO.PO_Number + " is Updated Successfully";
                    Utility.SendEmaildynamic("po@ozoneabrasives.co.uk", "Admin", "PO is Created", details);
                    return new JsonResult { Data = new { status = true, message = "PO Updated Successfully" } };
                }
                else
                {
                    return new JsonResult { Data = new { status = false, message = "Oops!! Some error occured" } };
                }

            }
            catch (Exception e)
            {
                return new JsonResult { Data = new { status = false, message = e.Message } };
            }
        }

        public ContentResult GetPOItems(int? start, int length, long POid)
        {
            List<PURCHASEORDER_ITEMS> lst = new List<PURCHASEORDER_ITEMS>();
            OzoneDBEntities ob = new OzoneDBEntities();
            ob.Configuration.ProxyCreationEnabled = false;
            int iDisplayStart = start == null ? 0 : start.Value;
            if (POid > 0)
            {
                lst = new POItemsBAL().ListPOItem(iDisplayStart, length, POid);
            }
            else
            {
                lst = PurchaseOrderItemsController.lstItems;
                if (lst == null)
                    lst = new List<PURCHASEORDER_ITEMS>();
            }
            var obj = lst; //new PurchaseOrderBAL().ListPO(iDisplayStart, length);
            var total = lst == null ? 0 : lst.Count;// new PurchaseOrderBAL().GetPOCount();

            DatatablePaggeing d = new DatatablePaggeing();
            d.aaData = obj;
            d.iTotalDisplayRecords = total;
            d.iTotalRecords = total;
            d.sEcho = "";

            var list = JsonConvert.SerializeObject(d,
            Formatting.None,
            new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            });

            return Content(list, "application/json");

            //return new JsonResult { Data = d, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        public ActionResult ViewPDF(PurchaseOrderBAL PO)
        {
            var tot = PurchaseOrderItemsController.lstItems.Sum(c => c.Total_Amt);
            PO.Grand_Total = tot;

            string FullPath = Path.Combine(Server.MapPath("/PO_Generate/"), PO.Ship_Name +"/"+ PO.PO_Number);
            string FilePath = Path.Combine(FullPath, PO.PO_Number + ".pdf");
            string finalpath = Request.Url.GetLeftPart(UriPartial.Authority) + Url.Content("~") + "/PO_Generate/" + PO.Ship_Name + "/" + PO.PO_Number + "/" + PO.PO_Number + ".pdf";
             DateTime date = DateTime.ParseExact(PO.PO_Date,
                                    "dd/MM/yyyy",
                                    CultureInfo.InvariantCulture);
            POModels po = new POModels();
            var vendor = new RegisterBAL().BindById(PO.Fk_Vendor_Id.Value);

            po.Grand_Total = PO.Grand_Total;
            po.Ship_Name = vendor.Company_Name;
            po.Ship_Address = vendor.Correspond_Add;
            po.Checked_By = PO.Checked_By;
            po.Document_Req = PO.Document_Req;
            po.Payment_Terms = PO.Payment_Terms;
            po.PO_Date = date.ToShortDateString();
            po.PO_Number = PO.PO_Number;
            po.Ship_City = vendor.Fk_City_Id;
            po.Ship_Compny = vendor.Company_Name;
            po.Ship_Country = new CountryBAL().GetCountryById(vendor.Fk_Country_Id.Value).Country_Name;
            po.Ship_Phone = vendor.Mobile_No;
            po.Ship_State = new StateBAL().GetStateById(vendor.Fk_State_Id.Value).State_Name;
                   
            po.Ship_Zip_code = PO.Ship_Zip_code;
            po.Shipment_Detail = PO.Shipment_Detail;
            po.DeliveryTerms = PO.DeliveryTerms;
            po.Transport = PO.Transport;
            po.Consignee = PO.Consignee;
            po.NotifyParty = PO.NotifyParty;
            po.PortLoading = PO.PortLoading;
            po.DocumentDetais = PO.DocumentDetais;
            po.Payment_Terms = PO.Payment_Terms;
            po.Note = PO.Note;
            po.cureencyName = new CurrencyBAL().GetCurrencybyid(PurchaseOrderItemsController.lstItems.FirstOrDefault().CurrencyId.Value).CurrencyName;

            po.amountinnumber = NumberToWords(Convert.ToInt32( PO.Grand_Total));
            Directory.CreateDirectory(FullPath);
            po.lstPOI = PurchaseOrderItemsController.lstItems;
            var actionResult = new ViewAsPdf("GeneratePDF", po);
            
           
            var byteArray = actionResult.BuildPdf(ControllerContext);
            
            var fileStream = new FileStream(FilePath, FileMode.Create, FileAccess.Write);
            fileStream.Write(byteArray, 0, byteArray.Length);
            fileStream.Close();
            return new JsonResult { Data = new { status = true, message = finalpath } };
            // return new Rotativa.ActionAsPdf("GeneratePDF", PO);
            //return new FileStreamResult(stream, "application/pdf");
        }


        public ActionResult GeneratePDF(PurchaseOrderBAL balPO)
        {
            if (balPO.PO_Date != null)
            {
                DateTime date = DateTime.Parse(balPO.PO_Date, (IFormatProvider)CultureInfo.InvariantCulture);
                POModels po = new POModels();
                po.Grand_Total = balPO.Grand_Total;
                po.Ship_Name = balPO.Ship_Name;
                po.Ship_Address = balPO.Ship_Address;
                po.Checked_By = balPO.Checked_By;
                po.Document_Req = balPO.Document_Req;
                po.Payment_Terms = balPO.Payment_Terms;
                po.PO_Date = date.ToShortDateString();
                po.PO_Number = balPO.PO_Number;
                po.Ship_City = balPO.Ship_City;
                po.Ship_Compny = balPO.Ship_Compny;
                po.Ship_Country = balPO.Ship_Country;
                po.Ship_Phone = balPO.Ship_Phone.ToString();
                po.Ship_State = balPO.Ship_State;
                po.Ship_Zip_code = balPO.Ship_Zip_code;
                po.Shipment_Detail = balPO.Shipment_Detail;
                po.Transport = balPO.Transport;
                po.lstPOI = PurchaseOrderItemsController.lstItems;
                return View(po);
            }
            else
                return View();

        }

        public ActionResult GenerateGRN(POModels balPO)
        {
          
                return View(balPO);
        }

        public ActionResult UpdateStatus(string PO_Number, string status)
        {
            POModels po = new POModels();
            po.PO_Number = PO_Number;
            po.PO_Status = status;
            //new PurchaseOrderBAL().UpdatePOStatus(PO_Number,status);
            return PartialView(po);
        }


        public ActionResult ViewItems(long POid)
        {
            POModels po = new POModels();
            po.lstPOI = new POItemsBAL().ListPOItemsByNullGRN(POid);
            return PartialView(po);
        }

        [HttpPost]
        public JsonResult POUpdate(List<POItemUpdateModel> lst)
        {
            List<PURCHASEORDER_ITEMS> lstsend = new List<PURCHASEORDER_ITEMS>();

            foreach (var item in lst)
            {
                if (item.ReceiveQty != 0) { 
                //here we have use currecyid for dead stock
                lstsend.Add(new PURCHASEORDER_ITEMS { Fk_Product_Id = item.Fk_Product_Id, Total_Qty = item.ReceiveQty, CurrencyId = item.deadstock, Pk_PO_Item_Id = item.Pk_PO_Item_Id, Created_by = item.Created_by });
                }
            }

            var result = new PurchaseOrderBAL().UpdateStatus(lstsend);
            var po = new PurchaseOrderBAL().GetPOById(lst[0].POID);
            var vendor = new RegisterBAL().BindById(po.Fk_Vendor_Id.Value);
            POModels pomodel = new POModels();

            pomodel.Checked_By = po.Checked_By;
            pomodel.Consignee = po.Consignee;
            pomodel.Created_by = po.Created_by;
            pomodel.Created_Date = po.Created_Date;
            pomodel.Document_Req = po.Document_Req;
            pomodel.DocumentDetais = po.DocumentDetais;
            pomodel.Fk_Vendor_Id = po.Fk_Vendor_Id;
            pomodel.Grand_Total = po.Grand_Total;
            pomodel.GRN = result[0].GRN;
            pomodel.Is_Deleted = po.Is_Deleted;
            pomodel.IsPartial = po.IsPartial;
            pomodel.LstDateofShip = po.LstDateofShip.Value.ToShortDateString();
            pomodel.Modified_By = po.Modified_By;
            pomodel.Modified_Date = po.Modified_Date;
            pomodel.NotifyParty = po.NotifyParty;
            pomodel.Payment_Terms = po.Payment_Terms;
            pomodel.PO_Date = po.PO_Date.ToShortDateString();
            pomodel.PO_Number = po.PO_Number;
            pomodel.PO_Status = po.PO_Status;
            pomodel.PortDestination = po.PortDestination;
            pomodel.PortDischarge = po.PortDischarge;
            pomodel.PortLoading = po.PortLoading;
            pomodel.Ship_Address = po.Ship_Address;
            pomodel.Ship_City = po.Ship_City;
            pomodel.Ship_Compny = po.Ship_Compny;
            pomodel.Ship_Country = po.Ship_Country;
            pomodel.Ship_Name = po.Ship_Name;
            pomodel.Ship_Phone = po.Ship_Phone.ToString();
            pomodel.Ship_State = po.Ship_State;
            pomodel.Ship_Zip_code = po.Ship_Zip_code;
            pomodel.Shipment_Detail = po.Shipment_Detail;
            pomodel.Shipper = po.Shipper;
            pomodel.Transport = po.Transport;
            pomodel.TransShip = po.TransShip;
            pomodel.lstPOI = result;
            pomodel.Note = po.Note;
            pomodel.DeliveryTerms = po.DeliveryTerms;
            pomodel.Ship_Name = vendor.Company_Name;
            pomodel.Ship_Address = vendor.Correspond_Add;
            pomodel.Ship_City = vendor.Fk_City_Id;
            pomodel.Ship_Compny = vendor.Company_Name;
            pomodel.Ship_Country = new CountryBAL().GetCountryById(vendor.Fk_Country_Id.Value).Country_Name;
            pomodel.Ship_Phone = vendor.Mobile_No;
            pomodel.Ship_State = new StateBAL().GetStateById(vendor.Fk_State_Id.Value).State_Name;
            string FullPath = Path.Combine(Server.MapPath("/PO_Generate/"), vendor.Company_Name + "/" + pomodel.PO_Number);
            string FilePath = Path.Combine(FullPath, pomodel.GRN + ".pdf");
            Directory.CreateDirectory(FullPath);
            var actionResult = new ViewAsPdf("GenerateGRN", pomodel);
            var byteArray = actionResult.BuildPdf(ControllerContext);

            var fileStream = new FileStream(FilePath, FileMode.Create, FileAccess.Write);
            fileStream.Write(byteArray, 0, byteArray.Length);
            fileStream.Close();
            // return Content(result, "application/text");
            return new JsonResult { Data = new { status = true, message = "GRN Generated Successfully" } };
        }

        public JsonResult UpdatePOStatus(string number, string status)
        {
            string result = new PurchaseOrderBAL().UpdatePOStatus(number,status);
            return new JsonResult { Data = new { status = true, message = result } };
        }
        public  string NumberToWords(int number)
        {
            if (number == 0)
                return "zero";

            if (number < 0)
                return "minus " + NumberToWords(Math.Abs(number));

            string words = "";

            if ((number / 1000000) > 0)
            {
                words += NumberToWords(number / 1000000) + " million ";
                number %= 1000000;
            }

            if ((number / 1000) > 0)
            {
                words += NumberToWords(number / 1000) + " thousand ";
                number %= 1000;
            }

            if ((number / 100) > 0)
            {
                words += NumberToWords(number / 100) + " hundred ";
                number %= 100;
            }

            if (number > 0)
            {
                if (words != "")
                    words += "and ";

                var unitsMap = new[] { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
                var tensMap = new[] { "zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

                if (number < 20)
                    words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0)
                        words += "-" + unitsMap[number % 10];
                }
            }

            return words;
        }
    }
}
