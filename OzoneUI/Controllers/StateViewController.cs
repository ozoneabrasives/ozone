﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ozone.BAL;
using OzoneUI.Models;
using Ozone.DAL;
using System.Web.Script.Serialization;
using Newtonsoft.Json;


namespace OzoneUI.Controllers
{
    public class StateViewController : Controller
    {
        //
        // GET: /StateView/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create(long Stateid = 0)
        {
            if (Stateid != null)
            {
                StateModels state = new StateModels();
                state.lstCountry = new CountryBAL().DDLCountry();

                if (Stateid > 0)
                {
                    STATE_MASTER balState = new StateBAL().GetStateById(Stateid);
                    state.Pk_State_Id = Stateid;
                    state.State_Name = balState.State_Name;
                    state.State_Code = balState.State_Code;
                    state.Fk_Country_Id = balState.Fk_Country_Id;
                    return PartialView(state);
                }
                else
                {
                    return PartialView(state);
                }
            }
            else
            {
                return PartialView();

            }

        }

        public ContentResult GetState(int? start, int length,string search)
        {
            int iDisplayStart = start == null ? 0 : start.Value;

            var obj = new StateBAL().ListState(iDisplayStart, length,search);
            var total = new StateBAL().GetStateCount(search);

            DatatablePaggeing d = new DatatablePaggeing();
            d.aaData = obj;
            d.iTotalDisplayRecords = total;
            d.iTotalRecords = total;
            d.sEcho = "";

            var list = JsonConvert.SerializeObject(d,
            Formatting.None,
            new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            });

            return Content(list, "application/json");
            //return new JsonResult { Data = d, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public ActionResult Edit(long Stateid)
        {
            return PartialView(new StateBAL().GetStateById(Stateid));
        }

        public ContentResult Delete(string id)
        {
            string suc;
            suc = new StateBAL().DeleteState(Convert.ToInt64(id));
            if (suc == "Deleted")
            {
                //return new JsonResult { Data = new { status = true, message = "Product deleted successfully" } };
                return Content("State deleted successfully", "application/text");
            }
            else
            {
                //return new JsonResult { Data = new { status = false, message = "Oops!! Some error occured" } };
                return Content("Oops!! Some error occured", "application/text");
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveState(StateBAL State)
        {

            try
            {

                string suc;
                if (new StateBAL().GetStateCount(State.State_Name) > 0)
                {

                    suc = "Exist";
                }
                else
                {
                    if (State.Pk_State_Id > 0)
                    {
                        suc = new StateBAL().EditState(State);
                    }
                    else
                    {
                        suc = StateBAL.InsertState(State);
                    }
                }
                
                if (suc == "Success")
                {
                    return new JsonResult { Data = new { status = true, message = "State added successfully" } };
                }
                else if (suc == "Updated")
                {
                    return new JsonResult { Data = new { status = true, message = "State Updated Successfully" } };
                }
                else if (suc == "Exist")
                {
                    return new JsonResult { Data = new { status = false, message = "State Already Exist" } };
                }
                else
                {
                    return new JsonResult { Data = new { status = false, message = "Oops!! Some error occured" } };
                }

            }
            catch (Exception e)
            {
                return new JsonResult { Data = new { status = false, message = e.Message } };
            }
        }


    }
}
