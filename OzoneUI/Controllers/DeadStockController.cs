﻿using Newtonsoft.Json;
using Ozone.BAL;
using OzoneUI.Models;
using Rotativa.MVC;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OzoneUI.Controllers
{
    public class DeadStockController : Controller
    {
        //
        // GET: /DeadStock/

        public ActionResult Index()
        {
            if (TempData["export"] != null)
            {
                ViewBag.export = "Yes";
                TempData.Remove("export");
            }
            return View();
        }
        [HttpPost]
        public ActionResult ExportData()
        {
            GridView gv = new GridView();
            gv.DataSource = new InventoryBAL().Getlist(0, new ShoppingCartBAL().GetAccountCount("", null, null, ""));
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            string filname = "Stock_ExportDate_" + System.DateTime.Now.ToShortDateString()+".xls";
            Response.AddHeader("content-disposition", "attachment; filename=" + filname);
            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            gv.RenderControl(htw);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
            return RedirectToAction("inded");
        }
        [HttpPost]
        public ActionResult ExportDeadStock()
        {
            GridView gv = new GridView();
            gv.DataSource = new InventoryBAL().GetDeadStock(0, new InventoryBAL().CountDeadStock());
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            string filname = "DeadStock_ExportDate_" + System.DateTime.Now.ToShortDateString() + ".xls";
            Response.AddHeader("content-disposition", "attachment; filename=" + filname);
            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            gv.RenderControl(htw);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
            TempData["export"] = "Yes";
            return RedirectToAction("Index", "DeadStock");
        }
   

        public ContentResult GetDeadStock(int? start, int length)
        {
            int iDisplayStart = start == null ? 0 : start.Value;

            var obj = new InventoryBAL().GetDeadStock(iDisplayStart, length);
            var total = new InventoryBAL().CountDeadStock();

            DatatablePaggeing d = new DatatablePaggeing();
            d.aaData = obj;
            d.iTotalDisplayRecords = total;
            d.iTotalRecords = total;
            d.sEcho = "";

            var list = JsonConvert.SerializeObject(d,
               Formatting.None,
               new JsonSerializerSettings()
               {
                   ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
               });

            return Content(list, "application/json");
        }

        public ActionResult UpdateallDeadStock()
        {
            return new JsonResult { Data = new { status = true, message = new InventoryBAL().MoveDeadStocktoNull() } };
        }

        public ActionResult TransferDS(long id,int qty)
        {
            var item = new InventoryBAL().TransferStock(id,qty);
            var po = new PurchaseOrderBAL().GetPOById(item.Fk_PO_Id.Value);
            var vendor = new RegisterBAL().BindById(po.Fk_Vendor_Id.Value);
            POModels pomodel = new POModels();
            List<Ozone.DAL.PURCHASEORDER_ITEMS> lstfinal = new List<Ozone.DAL.PURCHASEORDER_ITEMS>();
            lstfinal.Add(item);
            pomodel.Checked_By = po.Checked_By;
            pomodel.Consignee = po.Consignee;
            pomodel.Created_by = po.Created_by;
            pomodel.Created_Date = po.Created_Date;
            pomodel.Document_Req = po.Document_Req;
            pomodel.DocumentDetais = po.DocumentDetais;
            pomodel.Fk_Vendor_Id = po.Fk_Vendor_Id;
            pomodel.Grand_Total = po.Grand_Total;
            pomodel.GRN =item.GRN;
            pomodel.Is_Deleted = po.Is_Deleted;
            pomodel.IsPartial = po.IsPartial;
            pomodel.LstDateofShip = po.LstDateofShip.Value.ToShortDateString();
            pomodel.Modified_By = po.Modified_By;
            pomodel.Modified_Date = po.Modified_Date;
            pomodel.NotifyParty = po.NotifyParty;
            pomodel.Payment_Terms = po.Payment_Terms;
            pomodel.PO_Date = po.PO_Date.ToShortDateString();
            pomodel.PO_Number = po.PO_Number;
            pomodel.PO_Status = po.PO_Status;
            pomodel.PortDestination = po.PortDestination;
            pomodel.PortDischarge = po.PortDischarge;
            pomodel.PortLoading = po.PortLoading;
            pomodel.Ship_Address = po.Ship_Address;
            pomodel.Ship_City = po.Ship_City;
            pomodel.Ship_Compny = po.Ship_Compny;
            pomodel.Ship_Country = po.Ship_Country;
            pomodel.Ship_Name = po.Ship_Name;
            pomodel.Ship_Phone = po.Ship_Phone.ToString();
            pomodel.Ship_State = po.Ship_State;
            pomodel.Ship_Zip_code = po.Ship_Zip_code;
            pomodel.Shipment_Detail = po.Shipment_Detail;
            pomodel.Shipper = po.Shipper;
            pomodel.Transport = po.Transport;
            pomodel.TransShip = po.TransShip;
            pomodel.lstPOI = lstfinal;
            pomodel.Note = po.Note;
            pomodel.DeliveryTerms = po.DeliveryTerms;
            pomodel.Ship_Name = vendor.Company_Name;
            pomodel.Ship_Address = vendor.Correspond_Add;
            pomodel.Ship_City = vendor.Fk_City_Id;
            pomodel.Ship_Compny = vendor.Company_Name;
            pomodel.Ship_Country = new CountryBAL().GetCountryById(vendor.Fk_Country_Id.Value).Country_Name;
            pomodel.Ship_Phone = vendor.Mobile_No;
            pomodel.Ship_State = new StateBAL().GetStateById(vendor.Fk_State_Id.Value).State_Name;
            string FullPath = Path.Combine(Server.MapPath("/PO_Generate/"), vendor.Company_Name + "/" + pomodel.PO_Number);
            string FilePath = Path.Combine(FullPath, pomodel.GRN + ".pdf");
            Directory.CreateDirectory(FullPath);
            var actionResult = new ViewAsPdf("GenerateGRN", pomodel);
            var byteArray = actionResult.BuildPdf(ControllerContext);

            var fileStream = new FileStream(FilePath, FileMode.Create, FileAccess.Write);
            fileStream.Write(byteArray, 0, byteArray.Length);
            fileStream.Close();
            return new JsonResult { Data = new { status = true, message ="Deadstock is moved Successfully" } };
        }
        public ActionResult MoveStock(int maxqty,long stockid)
        {
            ViewBag.maxqty = maxqty;
            ViewBag.stockid = stockid;
            return View();
        }
        public ActionResult GenerateGRN(POModels balPO)
        {

            return View(balPO);
        }
    }
}
