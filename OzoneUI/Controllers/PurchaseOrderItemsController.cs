﻿using Newtonsoft.Json;
using Ozone.BAL;
using Ozone.DAL;
using OzoneUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OzoneUI.Controllers
{
    public class PurchaseOrderItemsController : Controller
    {
        //
        // GET: /PurchaseOrderItems/

        public static List<PURCHASEORDER_ITEMS> lstItems { get; set; }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create(long POItemid = 0, long POId = 0)
        {
            if (lstItems != null)
            {

            }
            if (POItemid != null)
            {
                POItemModels POItem = new POItemModels();
                POItem.Fk_PO_Id = POId;
                POItem.lstProduct = new ProductBAL().DDLProduct();
                POItem.lstPackage = new PackageBAL().DDLPackage();
                POItem.lstcurrency = new CurrencyBAL().getlist();
                if (POItemid > 0)
                {
                    PURCHASEORDER_ITEMS balPOItem = new PURCHASEORDER_ITEMS();
                    if (POId > 0)
                    {
                        balPOItem = new POItemsBAL().GetPOItemById(POItemid);
                    }
                    else
                    {
                        balPOItem = lstItems.SingleOrDefault(c => c.Pk_PO_Item_Id == POItemid);
                    }
                    POItem.lstUnits = new PackageBAL().DDLUnits(balPOItem.Packaging);
                    POItem.Pk_PO_Item_Id = POItemid;
                    POItem.Is_Deleted = false;
                    POItem.Created_Date = System.DateTime.Now;
                    POItem.Fk_PO_Id = balPOItem.Fk_PO_Id;
                    POItem.Fk_Product_Id = balPOItem.Fk_Product_Id;
                    POItem.Product_Description = balPOItem.Product_Description;
                    POItem.Packaging = balPOItem.Packaging;
                    POItem.Product_Code = balPOItem.Product_Code;
                    POItem.Total_Amt = balPOItem.Total_Amt;
                    POItem.Total_Qty = balPOItem.Total_Qty;
                    POItem.Unit_Price = balPOItem.Unit_Price;
                    POItem.Units = balPOItem.Units;
                    POItem.CurrencyId = balPOItem.CurrencyId;

                    POItem.CurrencyCode = new CurrencyBAL().GetCurrencybyid(balPOItem.CurrencyId.Value).Symbol;
                    return PartialView(POItem);
                }
                else
                {
                    POItem.lstUnits = new PackageBAL().DDLUnits("");
                    return PartialView(POItem);
                }
            }
            else
            {
                return PartialView();

            }

        }

        public ContentResult GetPOItem(int? start, int length)
        {
            int iDisplayStart = start == null ? 0 : start.Value;

            var obj = new POItemsBAL().ListItems(iDisplayStart, length);
            var total = new POItemsBAL().GetExtItemCount();

            DatatablePaggeing d = new DatatablePaggeing();
            d.aaData = obj;
            d.iTotalDisplayRecords = total;
            d.iTotalRecords = total;
            d.sEcho = "";

            var list = JsonConvert.SerializeObject(d,
            Formatting.None,
            new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            });

            return Content(list, "application/json");
            //return new JsonResult { Data = d, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public ActionResult Edit(long POid)
        {
            return PartialView(new POItemsBAL().GetPOItemById(POid));
        }

        public ActionResult Delete(long id, long poid)
        {
            string suc;
            if (poid == 0)
            {
                var dt = lstItems.SingleOrDefault(c => c.Pk_PO_Item_Id == id);
                lstItems.Remove(dt);
                suc = "Deleted";
            }
            else
            {
                suc = new POItemsBAL().DeletePOItem(id);
            }
            if (suc == "Deleted")
            {
                return new JsonResult { Data = new { status = true, message = "PO Item deleted successfully" } };
            }
            else
            {
                return new JsonResult { Data = new { status = false, message = "Oops!! Some error occured" } };
            }
        }
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult SavePOItem(POItemsBAL PO)
        {

            try
            {
                lstItems = (List<PURCHASEORDER_ITEMS>)Session["oidtems"];

                string suc;
                if (PO.Pk_PO_Item_Id > 0 )
                {
                    if (PO.Fk_PO_Id == null && lstItems.Count > 0)
                    {
                        var data = lstItems.SingleOrDefault(c => c.Pk_PO_Item_Id == PO.Pk_PO_Item_Id);
                        data.Product_Code = PO.Product_Code;
                        data.Packaging = PO.Packaging;
                        data.Units = PO.Units;
                        data.Total_Qty = PO.Total_Qty;
                        data.Unit_Price = PO.Unit_Price;
                        data.Fk_PO_Id = PO.Fk_PO_Id;
                        data.Fk_Package_Id = PO.Fk_Package_Id;
                        data.Product_Description = PO.Product_Description;
                        data.Total_Amt = PO.Total_Amt;
                        data.Created_by = Convert.ToInt64(Session["UserId"]);
                        data.Created_Date = System.DateTime.Now;
                        data.CurrencyId = PO.CurrencyId;
                        suc = "Success";
                    }
                    else
                    {
                        PO.Modified_By = Convert.ToInt64(Session["UserId"]);
                        PO.Modified_Date = System.DateTime.Now;
                        suc = new POItemsBAL().EditPOItem(PO);
                    }
                }
                else
                {
                    if (PO.Fk_PO_Id != 0 && PO.Fk_PO_Id != null)
                    {
                        POItemsBAL item = new POItemsBAL();
                        item.Fk_Product_Id = PO.Fk_Product_Id;
                        item.Product_Code = PO.Product_Code;
                        item.Packaging = PO.Packaging;
                        item.Fk_Package_Id = PO.Fk_Package_Id;
                        item.Units = PO.Units;
                        item.Total_Qty = PO.Total_Qty;
                        item.Unit_Price = PO.Unit_Price;
                        item.Fk_PO_Id = PO.Fk_PO_Id;
                        item.Product_Description = PO.Product_Description;
                        item.Total_Amt = PO.Total_Amt;
                        item.Created_Date = System.DateTime.Now;
                        item.CurrencyId = PO.CurrencyId;
                       
                        POItemsBAL.InsertPOItem(item);
                    }
                    else
                    {
                        if (lstItems == null)
                        {
                            lstItems = new List<PURCHASEORDER_ITEMS>();
                        }
                        PURCHASEORDER_ITEMS item = new PURCHASEORDER_ITEMS();
                        item.Pk_PO_Item_Id = lstItems.Count + 1;
                        item.Fk_Product_Id = PO.Fk_Product_Id;
                        item.Product_Code = PO.Product_Code;
                        item.Product_Description = PO.Product_Description;
                        item.Total_Amt = PO.Total_Amt;
                        item.Packaging = PO.Packaging;
                        item.Fk_Package_Id = PO.Fk_Package_Id;
                        item.Units = PO.Units;
                        item.Total_Qty = PO.Total_Qty;
                        item.Unit_Price = PO.Unit_Price;
                        item.Created_Date = System.DateTime.Now;
                        item.Created_by = Convert.ToInt64(Session["UserId"]);
                        item.Is_Deleted = false;
                        item.CurrencyId = PO.CurrencyId;
                        item.CurrencyCode = new CurrencyBAL().GetCurrencybyid(PO.CurrencyId.Value).Symbol;
                        lstItems.Add(item);
                    }
                    suc = "Success";// POItemsBAL.InsertPOItem(PO);
                }
                Session["oidtems"] = lstItems;
                if (suc == "Success")
                {
                    return new JsonResult { Data = new { status = true, message = "PO Item added successfully" } };
                }
                else if (suc == "Updated")
                {
                    return new JsonResult { Data = new { status = true, message = "PO Item Updated Successfully" } };
                }
                else
                {
                    return new JsonResult { Data = new { status = false, message = "Oops!! Some error occured" } };
                }

            }
            catch (Exception e)
            {
                return new JsonResult { Data = new { status = false, message = e.Message } };
            }
        }

        [HttpGet]
        public JsonResult GetItemByID(long id)
        {
            PRODUCT_DETAILS dal = new ProductBAL().GetProductById(id);
            object[] prod = new object[2];
            prod[0] = dal.Product_Code;            
            prod[1] = dal.Product_Description;
           // prod[2] = dal.PACKAGE_MASTER.Where(c => c.Fk_Product_Id == id && c.Is_Deleted == false).ToList();
            
            //var data = dal.PACKAGE_MASTER.Where(c => c.Fk_Product_Id == id && c.Is_Deleted == false).ToList();

            return Json(prod, JsonRequestBehavior.AllowGet);
        }
    }
}
