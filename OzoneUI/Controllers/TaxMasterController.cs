﻿using Ozone.BAL;
using OzoneUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ozone.DAL;
using Newtonsoft.Json;

namespace OzoneUI.Controllers
{
   
    public class TaxMasterController : Controller
    {
        //
        // GET: /TaxMaster/

       
        public ActionResult Index(long taxid = 0)
        {
            TaxMasterViewModel objmodel = new TaxMasterViewModel();
            objmodel.lstcountries = new CountryBAL().DDLCountry();
            if (taxid > 0)
            {
                objmodel.objtax = new TaxMasterBAL().Selectbyid(taxid);
                objmodel.CountryId = objmodel.objtax.FK_Country_id.Value;
                objmodel.Tax = objmodel.objtax.Charges.Value;
                objmodel.taxid = objmodel.objtax.TaxID;

            }
            
            return View(objmodel);
        }

       
        [HttpPost]
        public ActionResult Index(TaxMasterViewModel objtax)
        {
          if (ModelState.IsValid){

            TaxMaster instax = new TaxMaster();
            instax.Charges = objtax.Tax;
            instax.FK_Country_id = objtax.CountryId;
            instax.TaxID = objtax.taxid;
            instax.IsDeleted = false;
            if (instax.TaxID > 0)
            {
                new TaxMasterBAL().update(instax);
            }
            else
            {
                new TaxMasterBAL().Insert(instax);
            }
            TaxMasterViewModel objmodel = new TaxMasterViewModel();
            objmodel.lstcountries = new CountryBAL().DDLCountry();
          
            return RedirectToAction("Index");
        }
        else{
            objtax.lstcountries = new CountryBAL().DDLCountry();
            return View(objtax);
    }
        }
        
        public ContentResult GetTax(int? start, int length)
        {
            int iDisplayStart = start == null ? 0 : start.Value;

            var obj = new TaxMasterBAL().ListTax(iDisplayStart, length);
            var total = new TaxMasterBAL().GetCount();

            DatatablePaggeing d = new DatatablePaggeing();
            d.aaData = obj;
            d.iTotalDisplayRecords = total;
            d.iTotalRecords = total;
            d.sEcho = "";
            var list = JsonConvert.SerializeObject( d,
    Formatting.None,
    new JsonSerializerSettings()
    {
        ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
    });

            return Content(list, "application/json");
           // return new JsonResult { Data = d, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
    }
}
