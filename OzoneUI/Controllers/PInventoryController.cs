﻿using Newtonsoft.Json;
using Ozone.BAL;
using OzoneUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OzoneUI.Controllers
{
    public class PInventoryController : Controller
    {
        //
        // GET: /PInventory/

        public ActionResult Index()
        {
            return View();
        }

        public ContentResult GetInventory(int? start, int length)
        {
            int iDisplayStart = start == null ? 0 : start.Value;

            var obj = new InventoryBAL().Getlist(iDisplayStart, length);
            var total = new ShoppingCartBAL().GetAccountCount("",null,null,"");


            DatatablePaggeing d = new DatatablePaggeing();
            d.aaData = obj;
            d.iTotalDisplayRecords = total;
            d.iTotalRecords = total;
            d.sEcho = "";

            var list = JsonConvert.SerializeObject(d,
            Formatting.None,
            new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            });

            return Content(list, "application/json");
        }
    }
}
