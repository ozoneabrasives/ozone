﻿using Ozone.BAL;
using Ozone.DAL;
using OzoneUI.Models;
using PayPal;
using PayPal.Api;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Rotativa.MVC;

namespace OzoneUI.Controllers
{
    public class PaypalController : Controller
    {
        //
        // GET: /Paypal/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PaymentWithCreditCard()
        {
            //create and item for which you are taking payment
            //if you need to add more items in the list
            //Then you will need to create multiple item objects or use some loop to instantiate object
            Item item = new Item();
            item.name = "Demo Item";
            item.currency = "USD";
            item.price = "5";
            item.quantity = "1";
            item.sku = "sku";

            //Now make a List of Item and add the above item to it
            //you can create as many items as you want and add to this list
            List<Item> itms = new List<Item>();
            itms.Add(item);
            ItemList itemList = new ItemList();
            itemList.items = itms;

            //Address for the payment
            Address billingAddress = new Address();
            billingAddress.city = "NewYork";
            billingAddress.country_code = "US";
            billingAddress.line1 = "23rd street kew gardens";
            billingAddress.postal_code = "43210";
            billingAddress.state = "NY";


            //Now Create an object of credit card and add above details to it
            CreditCard crdtCard = new CreditCard();
            crdtCard.billing_address = billingAddress;
            crdtCard.cvv2 = "123";
            crdtCard.expire_month = 1;
            crdtCard.expire_year = 2020;
            crdtCard.first_name = "Aman";
            crdtCard.last_name = "Thakur";
            crdtCard.number = "4485613674386079";
            crdtCard.type = "visa";

            // Specify details of your payment amount.
            Details details = new Details();
            details.shipping = "1";
            details.subtotal = "5";
            details.tax = "1";

            // Specify your total payment amount and assign the details object
            Amount amnt = new Amount();
            amnt.currency = "USD";
            // Total = shipping tax + subtotal.
            amnt.total = "7";
            amnt.details = details;

            // Now make a trasaction object and assign the Amount object
            Transaction tran = new Transaction();
            tran.amount = amnt;
            tran.description = "Description about the payment amount.";
            tran.item_list = itemList;
            tran.invoice_number = "your invoice number which you are generating";

            // Now, we have to make a list of trasaction and add the trasactions object
            // to this list. You can create one or more object as per your requirements

            List<Transaction> transactions = new List<Transaction>();
            transactions.Add(tran);

            // Now we need to specify the FundingInstrument of the Payer
            // for credit card payments, set the CreditCard which we made above

            FundingInstrument fundInstrument = new FundingInstrument();
            fundInstrument.credit_card = crdtCard;

            // The Payment creation API requires a list of FundingIntrument

            List<FundingInstrument> fundingInstrumentList = new List<FundingInstrument>();
            fundingInstrumentList.Add(fundInstrument);

            // Now create Payer object and assign the fundinginstrument list to the object
            Payer payr = new Payer();
            payr.funding_instruments = fundingInstrumentList;
            payr.payment_method = "credit_card";

            // finally create the payment object and assign the payer object & transaction list to it
            Payment pymnt = new Payment();
            pymnt.intent = "sale";
            pymnt.payer = payr;
            pymnt.transactions = transactions;

            try
            {
                //getting context from the paypal, basically we are sending the clientID and clientSecret key in this function 
                //to the get the context from the paypal API to make the payment for which we have created the object above.

                //Code for the configuration class is provided next

                // Basically, apiContext has a accesstoken which is sent by the paypal to authenticate the payment to facilitator account. An access token could be an alphanumeric string
                
                APIContext apiContext = Configuration.GetAPIContext();

                // Create is a Payment class function which actually sends the payment details to the paypal API for the payment. The function is passed with the ApiContext which we received above.

                Payment createdPayment = pymnt.Create(apiContext);

                //if the createdPayment.State is "approved" it means the payment was successfull else not

                if (createdPayment.state.ToLower() != "approved")
                {
                    return View("FailureView");
                }
            }
            catch (WebException ex)
            {
                if (ex.Response is HttpWebResponse)
                {
                    HttpStatusCode statusCode = ((HttpWebResponse)ex.Response).StatusCode;
                   // logger.Info("Got " + statusCode.ToString() + " response from server");
                    using (WebResponse wResponse = (HttpWebResponse)ex.Response)
                    {
                        using (Stream data = wResponse.GetResponseStream())
                        {
                            string text = new StreamReader(data).ReadToEnd();
                            
                        }
                    }
                }
                              
               // return View("FailureView");
            }

            return View("SuccessView");
        }

        public ActionResult PaymentWithPaypal()

        {
            ShoppingCartBAL objshopcart = TempData["extradata"] as ShoppingCartBAL;
            List<ShoppingCartBAL> lst = (List<ShoppingCartBAL>)Session["CartList"];
            //getting the apiContext as earlier
            APIContext apiContext = Configuration.GetAPIContext();

            try
            {
                string payerId = Request.Params["PayerID"];

                if (string.IsNullOrEmpty(payerId))
                {
                    //this section will be executed first because PayerID doesn't exist

                    //it is returned by the create function call of the payment class

                    // Creating a payment

                    // baseURL is the url on which paypal sendsback the data.

                    // So we have provided URL of this controller only

                    string baseURI = Request.Url.Scheme + "://" + Request.Url.Authority + "/Paypal/PaymentWithPayPal?";

                    //guid we are generating for storing the paymentID received in session

                    //after calling the create function and it is used in the payment execution

                    var guid = Convert.ToString((new Random()).Next(100000));

                    //CreatePayment function gives us the payment approval url

                    //on which payer is redirected for paypal acccount payment

                    var createdPayment = this.CreatePayment(apiContext, baseURI + "guid=" + guid, objshopcart);

                    //get links returned from paypal in response to Create function call

                    var links = createdPayment.links.GetEnumerator();

                    string paypalRedirectUrl = null;

                    while (links.MoveNext())
                    {
                        Links lnk = links.Current;

                        if (lnk.rel.ToLower().Trim().Equals("approval_url"))
                        {
                            //saving the payapalredirect URL to which user will be redirected for payment
                            paypalRedirectUrl = lnk.href;
                        }
                    }

                    // saving the paymentID in the key guid
                    Session.Add(guid, createdPayment.id);

                    return Redirect(paypalRedirectUrl);
                }
                else
                {
                    // This section is executed when we have received all the payments parameters

                    // from the previous call to the function Create

                    // Executing a payment

                    var guid = Request.Params["guid"];

                    var executedPayment = ExecutePayment(apiContext, payerId, Session[guid] as string);

                    

                    if (executedPayment.state.ToLower() != "approved")
                    {
                        Session["payment"] = "failed";
                        RedirectToAction("CheckOutProduct", "UserCart");
                    }
                    else
                    {
                        List<SHOPPING_CART_ITEMS> lstItems = new List<SHOPPING_CART_ITEMS>();
                        foreach (var item in lst)
                        {
                            SHOPPING_CART_ITEMS items = new SHOPPING_CART_ITEMS();
                            items.Create_By = Convert.ToInt64(Session["UserId"]);
                            items.Create_Date = System.DateTime.Now;
                            items.Fk_Package_Id = item.Fk_Package_Id;
                            items.Fk_Product_Id = item.Fk_Product_Id;
                            items.Is_Deleted = false;
                            items.Quantity = item.Quantity;
                            items.Unit_Price = item.Price;
                            items.Total_Amt = item.Total;
                            items.PRODUCT_DETAILS = new ProductBAL().GetProductById(item.Fk_Product_Id);
                            items.PACKAGE_MASTER = new PackageBAL().GetPackageById(item.Fk_Package_Id);
                            lstItems.Add(items);
                        }
                        string orderNo = "SO" + DateTime.Now.Year + ShoppingCartDAL.GetLastId().ToString("D" + 5);
                        string r = Request["rgrp"];
                        objshopcart.Paypalid = executedPayment.id;

                        Random ran = new Random();
                        string Num = "I_" + ran.Next();
                        objshopcart.Order_Number = orderNo;
                        objshopcart.Payment_Status = "Paid";
                       
                        objshopcart.Order_Status = "Requested";
                        objshopcart.Create_By = Convert.ToInt64(Session["UserId"]);
                        objshopcart.Fk_Product_Id = 1;
                        objshopcart.Fk_Package_Id = 1;
                        objshopcart.lstItems = lstItems;
                        String str = CartItemBAL.InsertCartItem(objshopcart);
                        SHOPPING_CART objshop = new ShoppingCartBAL().GetcartByDo(orderNo);

                        string FullPath = Path.Combine(Server.MapPath("/Invoice_Generate/"), objshop.USER_REGISTRATION.Company_Name + "/" + objshop.Order_Number);
                        string FilePath = Path.Combine(FullPath, objshop.Order_Number + ".pdf");
                        Directory.CreateDirectory(FullPath);
                        var actionResult = new ViewAsPdf("GenerateDO", objshop);
                        var byteArray = actionResult.BuildPdf(ControllerContext);

                        var fileStream = new FileStream(FilePath, FileMode.Create, FileAccess.Write);
                        fileStream.Write(byteArray, 0, byteArray.Length);
                        fileStream.Close();
                        Session.Remove("CartList");
                        Session.Remove("PopUp");
                       // return Redirect("../../UserCart/GetCart");
                        return RedirectToAction("GetCart", "UserCart");
                    }

                }
            }
            catch (ConnectionException ex)
            {
                Session["payment"] = "failed";
              return  RedirectToAction("CheckOutProduct", "UserCart");
                // ex.Response contains the response body details.
            }
            catch (PayPalException ex)
            {
                Session["payment"] = "failed";
              return  RedirectToAction("CheckOutProduct", "UserCart");
                // ...
            }
            Session["payment"] = "failed";
        return    RedirectToAction("CheckOutProduct", "UserCart");
            // View("SuccessView");
        }

        private PayPal.Api.Payment payment;

        private Payment ExecutePayment(APIContext apiContext, string payerId, string paymentId)
        {
            var paymentExecution = new PaymentExecution() { payer_id = payerId };
            this.payment = new Payment() { id = paymentId };
            return this.payment.Execute(apiContext, paymentExecution);
        }
        public ActionResult GenerateDO(SHOPPING_CART orderNo)
        {
            return View(orderNo);

        }
        private Payment CreatePayment(APIContext apiContext, string redirectUrl,ShoppingCartBAL objcart)
        {
            List<ShoppingCartBAL> lst = (List<ShoppingCartBAL>)Session["CartList"];
            string orderNo = "SO" + DateTime.Now.Year + ShoppingCartDAL.GetLastId().ToString("D" + 5);

            //similar to credit card create itemlist and add item objects to it
            var itemList = new ItemList() { items = new List<Item>() };

            foreach (var it in lst)
            {
                itemList.items.Add(new Item() { name = it.PackageNM, currency = "GBP", price = it.Price.ToString(), quantity = it.Quantity.ToString() });
            }
           
            Address billingAddress = new Address();
            billingAddress.city = objcart.Del_City;
            billingAddress.country_code = new CountryBAL().GetCountryById(objcart.Del_Country.Value).Country_Code;
            billingAddress.line1 = objcart.Del_Address;
            billingAddress.postal_code = objcart.Del_Zipcode;
            billingAddress.state = new StateBAL().GetStateById(objcart.Del_State.Value).State_Code ;//payer_info = new PayerInfo { billing_address = billingAddress } 
            var payer = new Payer() { payment_method = "paypal", payer_info = new PayerInfo { billing_address = billingAddress ,first_name= objcart.Full_Name } };

            // Configure Redirect Urls here with RedirectUrls object
            var redirUrls = new RedirectUrls()
            {
                cancel_url = redirectUrl,
                return_url = redirectUrl
            };
           
            // similar as we did for credit card, do here and create details object
            var details = new Details()
            {
                tax = objcart.VAT_Amount.ToString(),
                shipping = objcart.ShipRate.ToString(),
                subtotal = (objcart.Payment_Amt - objcart.VAT_Amount - objcart.ShipRate ).ToString()
               
                
                
            };

            // similar as we did for credit card, do here and create amount object
            var amount = new Amount()
            {
                currency = "GBP",
                total = objcart.Payment_Amt.ToString(), // Total must be equal to sum of shipping, tax and subtotal.
                details = details
                
            };

            var transactionList = new List<Transaction>();

            transactionList.Add(new Transaction()
            {
                description = "Transaction description.",
                invoice_number = orderNo,
                amount = amount,
                item_list = itemList
                
                
               
            });

            this.payment = new Payment()
            {
                intent = "sale",
                payer = payer,
                transactions = transactionList,
                redirect_urls = redirUrls,
                
               
            };

            // Create a payment using a APIContext
            return this.payment.Create(apiContext);

        }
    }
}
