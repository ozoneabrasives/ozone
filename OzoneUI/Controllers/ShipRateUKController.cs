﻿using OzoneUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ozone.BAL;
using Ozone.DAL;
using Newtonsoft.Json;

namespace OzoneUI.Controllers
{
    public class ShipRateUKController : Controller
    {
        //
        // GET: /ShipRateUK/

        public ActionResult Index(long Rateid=0)
        {
            ShipRateUKModel objrate = new ShipRateUKModel();
            if (Rateid>0)
            {
                var datas = new ShipRateBAL().SelectByid(Rateid);
                objrate.Qty = datas.Qty.Value;
                objrate.Rate = datas.Rate.Value;
                objrate.Rateid = datas.ShipRateId;
                return View(objrate);
                
            }
            else
            {
                return View();
            }
            

            
        }
        [HttpPost]
        public ActionResult Index(ShipRateUKModel objmodel)
        {
           
                ShipRateTab objship = new ShipRateTab();
                objship.Qty = objmodel.Qty;
                objship.Rate = objmodel.Rate;
                if (objmodel.Rateid>0)
                {
                    objship.ShipRateId = objmodel.Rateid;
                    new ShipRateBAL().Update(objship);

                    
                }
                else
                {
                    objship.IsDeleted = false;
                    new ShipRateBAL().Insert(objship);

                }
               return RedirectToAction("index");
             
            

        }
         public ContentResult GetShipRate(int? start, int length)
        {
            int iDisplayStart = start == null ? 0 : start.Value;

            var obj = new ShipRateBAL().ListShipRate(iDisplayStart, length);
            var total = new ShipRateBAL().GetCount();

            DatatablePaggeing d = new DatatablePaggeing();
            d.aaData = obj;
            d.iTotalDisplayRecords = total;
            d.iTotalRecords = total;
            d.sEcho = "";
            var list = JsonConvert.SerializeObject( d,
    Formatting.None,
    new JsonSerializerSettings()
    {
        ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
    });

            return Content(list, "application/json");
           // return new JsonResult { Data = d, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
    

    }
}
