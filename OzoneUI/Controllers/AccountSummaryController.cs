﻿using Newtonsoft.Json;
using Ozone.BAL;
using OzoneUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OzoneUI.Controllers
{
    public class AccountSummaryController : Controller
    {
        //
        // GET: /AccountSummary/

        public ActionResult Index()
        {
            UsrRegisterModels modl = new UsrRegisterModels();
            modl.lstUSR = new RegisterBAL().ListUsers(0, 1000,"").Where(c=> c.User_Type !="Admin" && c.User_Type!="Vendor").ToList();            
            return View(modl);
        }
        

        public ContentResult GetAccountSum(int? start, int length, string search, long? ByCNo, long? ByClient, string ByStatus)
        {
            int iDisplayStart = start == null ? 0 : start.Value;
            var obj = new ShoppingCartBAL().GetAccountSum(search,ByCNo,ByClient,ByStatus);
            var total = obj.Count; //new ShoppingCartBAL().GetAccountCount(search, ByCNo, ByClient, ByStatus);

            DatatablePaggeing d = new DatatablePaggeing();
            d.aaData = obj;
            d.iTotalDisplayRecords = total;
            d.iTotalRecords = total;
            d.sEcho = "";

            var list = JsonConvert.SerializeObject(d,
            Formatting.None,
            new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            });

            return Content(list, "application/json");
        }
        public ActionResult GetStatement()
        {
            return View();
        }
        public ContentResult GetStatementData(int? start, int length)
        {
            int iDisplayStart = start == null ? 0 : start.Value;
            var obj = InvoiceBAL.GetStatement(iDisplayStart, length);
            var total = InvoiceBAL.GetStatementcount(); //new ShoppingCartBAL().GetAccountCount(search, ByCNo, ByClient, ByStatus);

            DatatablePaggeing d = new DatatablePaggeing();
            d.aaData = obj;
            d.iTotalDisplayRecords = total;
            d.iTotalRecords = total;
            d.sEcho = "";

            var list = JsonConvert.SerializeObject(d,
            Formatting.None,
            new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            });

            return Content(list, "application/json");
        }

    }
}
