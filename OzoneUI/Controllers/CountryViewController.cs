﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ozone.BAL;
using OzoneUI.Models;
using Ozone.DAL;
using System.Web.Script.Serialization;

namespace OzoneUI.Controllers
{
    public class CountryViewController : Controller
    {
        //
        // GET: /CountryView/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create(long countryid = 0)
        {
            if (countryid != null)
            {
                CountryModels country = new CountryModels();
                if (countryid > 0)
                {
                    COUNTRY_MASTER cou = new CountryBAL().GetCountryById(countryid);
                    country.Country_Code = cou.Country_Code;
                    country.Country_Name = cou.Country_Name;
                    country.Pk_Country_Id = cou.Pk_Country_Id;
                    return PartialView(country);
                }
                else
                {
                    return PartialView();
                }
            }
            else
            {
                return PartialView();

            }

        }
        public JsonResult GetCountry(int? start, int length ,string Search)
        {
            int iDisplayStart = start == null ? 0 : start.Value;

            var obj = new CountryBAL().ListCountry(iDisplayStart, length,Search);
            var total = new CountryBAL().GetCountryCount(Search);

            DatatablePaggeing d = new DatatablePaggeing();
            d.aaData = obj;
            d.iTotalDisplayRecords = total;
            d.iTotalRecords = total;
            d.sEcho = "";


            return new JsonResult { Data = d, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        public ActionResult Edit(long Countryid)
        {
            return PartialView(new CountryBAL().GetCountryById(Countryid));
        }

        public ContentResult Delete(string id)
        {
            string suc;
            suc = new CountryBAL().DeleteCountry(Convert.ToInt64(id));
            if (suc == "Deleted")
            {
                //return new JsonResult { Data = new { status = true, message = "Product deleted successfully" } };
                return Content("Country deleted successfully", "application/text");
            }
            else
            {
                //return new JsonResult { Data = new { status = false, message = "Oops!! Some error occured" } };
                return Content("Oops!! Some error occured", "application/text");
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveCountry(CountryBAL country)
        {

            try
            {

                string suc;
                if (new CountryBAL().GetCountryCount(country.Country_Name) > 0)
                {

                    suc = "Exist";
                }
                else
                {
                    if (country.Pk_Country_Id > 0)
                    {
                        country.Modified_By = Convert.ToInt64(Session["UserId"]);
                        suc = new CountryBAL().EditCountry(country);
                    }
                    else
                    {
                        country.Created_by = Convert.ToInt64(Session["UserId"]);
                        suc = CountryBAL.InsertCountry(country);
                    }
                }
                
                if (suc == "Success")
                {
                    return new JsonResult { Data = new { status = true, message = "Country added successfully" } };
                }
                else if (suc == "Updated")
                {
                    return new JsonResult { Data = new { status = true, message = "Country Updated Successfully" } };
                }
                else if( suc=="Exist")
                {
                    return new JsonResult { Data = new { status = false, message = "Country Already Exist" } };
                }
                else
                {
                    return new JsonResult { Data = new { status = false, message = "Try Again!" } };
                }

            }
            catch (Exception e)
            {
                return new JsonResult { Data = new { status = false, message = e.Message } };
            }
        }


    }
}
