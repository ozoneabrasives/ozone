﻿using Newtonsoft.Json;
using Ozone.BAL;
using Ozone.DAL;
using OzoneUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OzoneUI.Controllers
{
    public class PackagingController : Controller
    {
        //
        // GET: /Packaging/

        public ActionResult Index()
        {
            return View();
        }
        
        public ActionResult Create(long Packageid = 0)
        {
            if (Packageid != null)
            {
                PackageModels Package = new PackageModels();
                Package.lstProduct = new ProductBAL().DDLProduct();
                Package.lstunits = new List<string>();
                Package.lstunits.Add("K.G");
                Package.lstunits.Add("Tones");

                if (Packageid > 0)
                {
                    PACKAGE_MASTER balPackage = new PackageBAL().GetPackageById(Packageid);
                    Package.Pk_Package_Id = Packageid;
                    Package.Package_Title = balPackage.Package_Title;
                    Package.Package_Unit = balPackage.Package_Unit;
                    Package.Fk_Product_Id = balPackage.Fk_Product_Id;
                    Package.Cost_Price = balPackage.Cost_Price;
                    Package.Package_Amt = balPackage.Package_Amt==null? 0 : balPackage.Package_Amt.Value;
                    Package.Sell_Price = balPackage.Sell_Price;
                    
                    return PartialView(Package);
                }
                else
                {
                    return PartialView(Package);
                }
            }
            else
            {
                return PartialView();

            }

        }

        public ContentResult GetPackage(int? start, int length, string search)
        {
            int iDisplayStart = start == null ? 0 : start.Value;

            var obj = new PackageBAL().ListPackage(iDisplayStart, length,search);
            var total = new PackageBAL().GetPackageCount();

            DatatablePaggeing d = new DatatablePaggeing();
            d.aaData = obj;
            d.iTotalDisplayRecords = total;
            d.iTotalRecords = total;
            d.sEcho = "";

            var list = JsonConvert.SerializeObject(d,
            Formatting.None,
            new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            });

            return Content(list, "application/json");
            //return new JsonResult { Data = d, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public ActionResult Edit(long Packageid)
        {
            return PartialView(new PackageBAL().GetPackageById(Packageid));
        }

        public ContentResult Delete(string id)
        {
            string suc;
            suc = new PackageBAL().DeletePackage(Convert.ToInt64(id));
            if (suc == "Deleted")
            {
                //return new JsonResult { Data = new { status = true, message = "Package deleted successfully" } };
                return Content("Package deleted successfully", "application/text");
            }
            else
            {
                //return new JsonResult { Data = new { status = false, message = "Oops!! Some error occured" } };
                return Content("Oops!! Some error occured", "application/text");
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SavePackage(PackageBAL Package)
        {

            try
            {

                string suc;
                if (Package.Pk_Package_Id > 0)
                {
                    suc = new PackageBAL().EditPackage(Package);
                }
                else
                {
                    suc = PackageBAL.InsertPackage(Package);
                }
                if (suc == "Success")
                {
                    return new JsonResult { Data = new { status = true, message = "Package added successfully" } };
                }
                else if (suc == "Updated")
                {
                    return new JsonResult { Data = new { status = true, message = "Package Updated Successfully" } };
                }
                else
                {
                    return new JsonResult { Data = new { status = false, message = "Oops!! Some error occured" } };
                }

            }
            catch (Exception e)
            {
                return new JsonResult { Data = new { status = false, message = e.Message } };
            }
        }

        [HttpGet]
        public JsonResult LoadUnits(string package)
        {
            return Json(new PackageBAL().DDLUnits(package), JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult LoadPackage(long productid)
        {
            var lst= new PackageBAL().GetPackageByProduct(productid).ToList();
            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetPriceByID(long id)
        {
            var d = new PackageBAL().BindPrice(id);
            if (d == null)
            {
                d = 0;
            }
            return Json(d, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetBPriceByID(long id)
        {
            return Json(new PackageBAL().BindBPrice(id), JsonRequestBehavior.AllowGet);
        }
    }
}
