﻿using OzoneUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ozone.BAL;
using Ozone.DAL;

namespace OzoneUI.Controllers
{
    public class RegisterViewController : Controller
    {
        [AllowAnonymous]
        public ActionResult Index()
        {
            UsrRegisterModels modl = new UsrRegisterModels();
            modl.CortNum = "OALC" + RegisterDAL.GetCorLastId().ToString("D" + 5);
            modl.RetNum = "OALR" + RegisterDAL.GetRetailLastId().ToString("D" + 5);
            modl.Customer_ID = modl.RetNum;
            modl.lstCountry = new CountryBAL().DDLCountry();
            modl.lstState = new List<Ozone.DAL.STATE_MASTER>();
            return View(modl);
        }
        //
        // GET: /RegisterView/
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Index(UsrRegisterModels model)
        {
            //if (model.Register_Type == "")
            //{

            //}
            try
            {


                if (ModelState.IsValid)
                {
                    RegisterBAL bal = new RegisterBAL();
                    bal.Auth_Officer_Name = model.Auth_Officer_Name;
                    bal.Company_Name = model.Company_Name;
                    bal.Company_Reg_No = model.Company_Reg_No;
                    bal.Correspond_Add = model.Correspond_Add;
                    bal.Created_Date = System.DateTime.Now;
                    bal.Delivery_Detail = model.Delivery_Detail;
                    bal.Delivery_Type = model.Delivery_Type;
                    bal.Email_ID = model.Email_ID;
                    bal.Fax = model.Fax;
                    bal.Fk_City_Id = model.Fk_City_Id;
                    bal.Fk_Country_Id = model.Fk_Country_Id;
                    bal.Fk_State_Id = model.Fk_State_Id;
                    bal.Is_Approved = model.Is_Approved;
                    bal.Landline_No = model.Landline_No;
                    bal.Mobile_No = model.Mobile_No;
                    bal.Payment_Terms = model.Payment_Terms;
                    bal.Permanent_Add = model.Permanent_Add;
                    bal.Register_Type = model.Register_Type;
                    bal.User_Name = model.User_Name;
                    bal.User_Password = model.User_Password;
                    bal.VAT_No = model.VAT_No;
                    if (bal.Register_Type == "Retail")
                    {
                        bal.Customer_ID = model.RetNum;
                    }
                    else
                    {
                        bal.Customer_ID = model.CortNum;
                    }
                   
                    bal.Have_Credit = false;
                    bal.Is_Approved = false;
                    bal.User_Type = "Company";
                    long id = bal.InsertRegister(bal);
                    bal.Pk_Register_Id = id;
                    Utility.SendConformationMail(bal);
                    TempData["status"] = "Success";
                    return RedirectToAction("LoginUsr", "RegisterView");
                    //return View(model);
                }
                else
                {
                    model.lstCountry = new CountryBAL().DDLCountry();
                    model.lstState = new List<Ozone.DAL.STATE_MASTER>();
                }
                return View(model);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost]
        [AllowAnonymous]
      
        public ActionResult LoginUsr(UsrRegisterModels model)
        {
            try
            {
                RegisterBAL bal = new RegisterBAL();
                bal.User_Name = model.User_Name;
                bal.User_Password = model.User_Password;

                USER_REGISTRATION chk = bal.CheckUser(bal);

                if (chk == null)
                {
                    ModelState.AddModelError("", "The user name or password provided is incorrect.");
                    return View(model);
                }
                else
                {
                    if (chk.User_Type == "Vendor")
                    {
                        return RedirectToAction("LoginUsr", "RegisterView");
                    }
                    Session["UserName"] = chk.User_Name;
                    Session["UserType"] = chk.User_Type;
                    Session["UserId"] = chk.Pk_Register_Id;
                    Session["IsCredit"] = chk.Have_Credit;
                    if (Session["UserType"].ToString() == "Admin")
                        return RedirectToAction("Index", "Home");
                    else
                        return RedirectToAction("CompnyIndex", "Home");
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("LoginUsr", "RegisterView");
            }
        }

        public ActionResult LoginUsr()
        {
            Session.Remove("UserName");
            Session.Remove("UserType");
            Session.Remove("UserId");
            if (TempData["status"] != null)
            {
                ViewBag.Status = "Success";
                TempData.Remove("status");
            }
            Session.RemoveAll();
          //  string grm = new PurchaseOrderDAL().GenerateGRN();
            return View();
        }

        public ActionResult ForgotPassword()
        {
            return View();
        }


        public ActionResult UsersList()
        {
            return View();
        }

        [HttpGet]
        public JsonResult LoadStates(long countryId)
        {
            return Json(new StateBAL().DDLState(countryId), JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckExist(string name)
        {
            RegisterBAL bal = new RegisterBAL();
            bal.User_Name = name;
            USER_REGISTRATION chk = new RegisterBAL().CheckUser(bal);
            if (chk == null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult CheckEmail(string email)
        {
            var user = new RegisterBAL().getuserByemail(email);
            if (user == null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetUsers(int? start, int length,string usertype)
        {
            int iDisplayStart = start == null ? 0 : start.Value;

            var obj = new RegisterBAL().ListUsers(iDisplayStart, length,usertype);
            var total = new RegisterBAL().GetUsersCount(usertype);

            DatatablePaggeing d = new DatatablePaggeing();
            d.aaData = obj;
            d.iTotalDisplayRecords = total;
            d.iTotalRecords = total;
            d.sEcho = "";


            return new JsonResult { Data = d, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        public JsonResult AllowCredit(List<USER_REGISTRATION> lst)
        {
            List<USER_REGISTRATION> lstCred = new RegisterBAL().UpdateCredit(lst);
            var lstuser = new RegisterBAL().getuserbyids(lst.Select(d=>d.Pk_Register_Id).ToList());

            foreach (USER_REGISTRATION item in lstuser)
            {
                if (item.Have_Credit == true)
                {
                    string details = "Hi, " + item.Auth_Officer_Name + " <br/> We would like to inform that you are now eligible for credit cycle . You can place future orders without payment gateway";
                    Utility.SendEmaildynamic(item.Email_ID, item.Auth_Officer_Name, "Credit is allowed for further orders - OZONE ABRASIVES", details);
                }
                foreach (USER_REGISTRATION usr in lstCred.Where(c=> c.Pk_Register_Id == item.Pk_Register_Id))
                {
                    if (item.Have_Credit == false)
                    {
                        string detailusr = "Hi, " + usr.Auth_Officer_Name + " <br/> We would like to inform you that you are not eligble for credit cycle. You have to use payment gateway for future orders";
                        Utility.SendEmaildynamic(usr.Email_ID, usr.Auth_Officer_Name, "Credit is disable for further orders - OZONE ABRASIVES", detailusr);
                    }
                }
            }
            

            string str = "Credits updated.";
            return new JsonResult { Data = str, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public ActionResult CreateUser(long UserId = 0,string utype="")
        {
            UsrRegisterModels reg = new UsrRegisterModels();
            reg.lstCountry = new CountryBAL().DDLCountry();
            reg.lstcurrency = new CurrencyBAL().getlist();
            if (utype == "corp")
            {
                reg.Customer_ID = "OALC" + RegisterDAL.GetCorLastId().ToString("D" + 5);
            }
            else if (utype == "retail")
            {
                reg.Customer_ID="OALR" + RegisterDAL.GetRetailLastId().ToString("D" + 5);
            }
            

            if (UserId > 0)
            {
                USER_REGISTRATION bal = new RegisterBAL().BindById(UserId);
                reg.Pk_Register_Id = bal.Pk_Register_Id;
                reg.Auth_Officer_Name = bal.Auth_Officer_Name;
                reg.Company_Name = bal.Company_Name;
                reg.Company_Reg_No = bal.Company_Reg_No;
                reg.Correspond_Add = bal.Correspond_Add;
                reg.Created_Date = System.DateTime.Now;
                reg.Delivery_Detail = bal.Delivery_Detail;
                reg.Delivery_Type = bal.Delivery_Type;
                reg.Email_ID = bal.Email_ID;
                reg.Fax = bal.Fax;
                reg.Fk_City_Id = bal.Fk_City_Id;
                reg.Fk_Country_Id = bal.Fk_Country_Id;
                reg.lstState = new StateBAL().DDLState((long)bal.Fk_Country_Id);
                reg.Fk_State_Id = bal.Fk_State_Id;
                reg.Is_Approved = bal.Is_Approved;
                reg.Landline_No = bal.Landline_No;
                reg.Mobile_No = bal.Mobile_No;
                reg.Payment_Terms = bal.Payment_Terms;
                reg.Permanent_Add = bal.Permanent_Add;
                reg.Register_Type = bal.Register_Type;
                reg.User_Name = bal.User_Name;
                reg.User_Password = bal.User_Password;
                reg.VAT_No = bal.VAT_No;
                reg.CurrencyID = bal.CurrencyID==null? 0 :bal.CurrencyID.Value;
                if (bal.Register_Type=="Vendor")
                {
                    utype = "ven";
                    
                }
                else if (bal.Register_Type == "Corporate")
                {
                    utype = "corp";
                }
                else if (bal.Register_Type == "Retail")
                {
                    utype = "retail";
                }
                ViewBag.reg = utype;
                return PartialView(reg);
            }
            else
            {

                reg.lstState = new List<Ozone.DAL.STATE_MASTER>();
                ViewBag.reg = utype;
                return PartialView(reg);
            }


        }

        public JsonResult Sendmail(string email)
        {
            string message;
            var user = new RegisterBAL().getuserByemail(email);
            if (user!=null)
            {
                string details = "Hi, " + user.Company_Name + " <br/> We have recived request for password your password is  <b>" + user.User_Password + "</b>. <br/> Thank you  ";
                Utility.SendEmaildynamic(user.Email_ID, user.Company_Name, "Password for Ozone", details);

                message = "We have sent Password to your register email id";
            }
            else
            {
                message = "Email id is not Register";
            }
            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        public ActionResult CreateUser(UsrRegisterModels model)
        {
            //if (model.Register_Type == "")
            //{

            //}
            try
            {
                RegisterBAL bal = new RegisterBAL();

                bal.Auth_Officer_Name = model.Auth_Officer_Name;
                bal.Company_Name = model.Company_Name;
                bal.Company_Reg_No = model.Company_Reg_No;
                bal.Correspond_Add = model.Correspond_Add;
                bal.Created_Date = System.DateTime.Now;
                bal.Delivery_Detail = model.Delivery_Detail;
                bal.Delivery_Type = model.Delivery_Type;
                bal.Email_ID = model.Email_ID;
                bal.Fax = model.Fax;
                bal.Fk_City_Id = model.Fk_City_Id;
                bal.Fk_Country_Id = model.Fk_Country_Id;
                bal.Fk_State_Id = model.Fk_State_Id;
                bal.Is_Approved = model.Is_Approved;
                bal.Landline_No = model.Landline_No;
                bal.Mobile_No = model.Mobile_No;
                bal.Payment_Terms = model.Payment_Terms;
                bal.Permanent_Add = model.Permanent_Add;
                
                bal.Customer_ID = model.Customer_ID;
                bal.Delivery_Type = model.Delivery_Type;
                bal.Payment_Terms = model.Payment_Terms;
                bal.User_Name = model.User_Name;
                bal.User_Password = model.User_Password;

                bal.VAT_No = model.VAT_No;
                bal.Have_Credit = false;
                bal.Is_Approved = true;
                bal.Currecnyid = model.CurrencyID;
               

                if (model.Pk_Register_Id != 0)
                {
                    bal.Pk_Register_Id = model.Pk_Register_Id;

                }
                else
                {
                    bal.Register_Type = model.Register_Type;
                    bal.User_Type = model.Register_Type;
                }
                long str = bal.InsertRegister(bal);
                //return RedirectToAction("LoginUsr", "RegisterView");
                //return View(model);               
                if (str > 0)
                    return new JsonResult { Data = new { status = true, message = "User Updated Successfully" } };
                else
                    return new JsonResult { Data = new { status = false, message = "Oops!! Some error occured" } };
            }
            catch (Exception ex)
            {
                return new JsonResult { Data = new { status = false, message = "Oops!! Some error occured" } };
            }
        }


        [HttpGet]
        public JsonResult GetUserById(long UserId)
        {
            USER_REGISTRATION cart = new RegisterBAL().BindById(UserId);
            UsrRegisterModels modl = new UsrRegisterModels();
            modl.Correspond_Add = cart.Permanent_Add;
            modl.Fk_Country_Id = cart.Fk_Country_Id;
            modl.Fk_State_Id = cart.Fk_State_Id;
            modl.Fk_City_Id = cart.Fk_City_Id;
            modl.Mobile_No = cart.Mobile_No;
            modl.CurrencyID = cart.CurrencyID==null?0:cart.CurrencyID.Value;

            return Json(modl, JsonRequestBehavior.AllowGet);

        }

        public ActionResult ConfirmAccount(long id)
        {
            RegisterDAL dal = new RegisterDAL();
            dal.ConfirmUsr(id);
            return RedirectToAction("LoginUsr", "RegisterView");
        }
    }
}
