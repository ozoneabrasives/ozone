﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ozone.BAL;
using OzoneUI.Models;
using Ozone.DAL;
using System.Web.Script.Serialization;
using Newtonsoft.Json;


namespace OzoneUI.Controllers
{
    public class CategoryController : Controller
    {
        //
        // GET: /Category/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Create(long Categoryid = 0)
        {
            if (Categoryid != null)
            {
                CategoryModels Category = new CategoryModels();


                if (Categoryid > 0)
                {
                    CATEGORY_MASTER balCategory = new CategoryBAL().GetCategoryById(Categoryid);
                    Category.Pk_Category_Id = Categoryid;
                    Category.Category_Name = balCategory.Category_Name;
                    Category.Category_Description = balCategory.Category_Description;

                    return PartialView(Category);
                }
                else
                {
                    return PartialView(Category);
                }
            }
            else
            {
                return PartialView();

            }

        }

        public JsonResult GetCategory(int? start, int length,string search)
        {
            int iDisplayStart = start == null ? 0 : start.Value;

            var obj = new CategoryBAL().ListCategory(iDisplayStart, length,search);
            var total = new CategoryBAL().GetCategoryCount(search);

            DatatablePaggeing d = new DatatablePaggeing();
            d.aaData = obj;
            d.iTotalDisplayRecords = total;
            d.iTotalRecords = total;
            d.sEcho = "";

            return new JsonResult { Data = d, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public ActionResult Edit(long Categoryid)
        {
            return PartialView(new CategoryBAL().GetCategoryById(Categoryid));
        }
        [HttpPost]
        public ContentResult Delete(string id)
        {
            string suc;
            suc = new CategoryBAL().DeleteCategory(Convert.ToInt64(id));
            if (suc == "Deleted")
            {
                //return new JsonResult { Data = new { status = true, message = "Product deleted successfully" } };
                return Content("Category deleted successfully", "application/text");
            }
            else
            {
                //return new JsonResult { Data = new { status = false, message = "Oops!! Some error occured" } };
                return Content("Oops!! Some error occured", "application/text");
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveCategory(CategoryBAL Category)
        {

            try
            {

                string suc;
                if (Category.Pk_Category_Id > 0)
                {
                    Category.Modified_By = Convert.ToInt64(Session["UserId"]);
                    suc = new CategoryBAL().EditCategory(Category);
                }
                else
                {
                    Category.Created_by = Convert.ToInt64(Session["UserId"]);
                    suc = CategoryBAL.InsertCategory(Category);
                }
                if (suc == "Success")
                {
                    return new JsonResult { Data = new { status = true, message = "Category Added Successfully" } };
                }
                else if (suc == "Updated")
                {
                    return new JsonResult { Data = new { status = true, message = "Category Updated Successfully" } };
                }
                else
                {
                    return new JsonResult { Data = new { status = false, message = "Oops!! Some error occured" } };
                }

            }
            catch (Exception e)
            {
                return new JsonResult { Data = new { status = false, message = e.Message } };
            }
        }
    }
}
