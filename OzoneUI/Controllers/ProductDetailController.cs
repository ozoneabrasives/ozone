﻿using Newtonsoft.Json;
using Ozone.BAL;
using Ozone.DAL;
using OzoneUI.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;


namespace OzoneUI.Controllers
{
    public class ProductDetailController : Controller
    {
        //
        // GET: /ProductDetail/

        public ActionResult Index()
        {
            return View();
        }

        public ContentResult GetProduct(int? start, int length,string search)
        {
            int iDisplayStart = start == null ? 0 : start.Value;

            var obj = new ProductBAL().ListProduct(iDisplayStart, length,search);
            var total = new ProductBAL().GetProductCount();

            DatatablePaggeing d = new DatatablePaggeing();
            d.aaData = obj;
            d.iTotalDisplayRecords = total;
            d.iTotalRecords = total;
            d.sEcho = "";
            var list = JsonConvert.SerializeObject(d,
           Formatting.None,
           new JsonSerializerSettings()
           {
               ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
           });

            return Content(list, "application/json");
            //return new JsonResult { Data = d, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public ActionResult Create(long Productid = 0)
        {
            if (Productid != null)
            {
                ProductModels Product = new ProductModels();
                Product.lstCategory = new CategoryBAL().DDLCategory();

                if (Productid > 0)
                {
                    PRODUCT_DETAILS balProduct = new ProductBAL().GetProductById(Productid);
                    Product.Pk_Product_Id = Productid;
                    Product.Product_Title = balProduct.Product_Title;
                    Product.Product_Code = balProduct.Product_Code;
                    Product.Fk_Category_Id = balProduct.Fk_Category_Id;
                    Product.Clearing_Rate = balProduct.Clearing_Rate;
                    Product.Consumption_Rate = balProduct.Consumption_Rate;
                    Product.Manufactur_Date = balProduct.Manufactur_Date.Value.ToShortDateString();
                    Product.Packaging = balProduct.Packaging;
                    Product.Price = balProduct.Price;
                    Product.Product_Description = balProduct.Product_Description;
                    Product.Surface_Profile = balProduct.Surface_Profile;
                    Product.Product_Img = balProduct.Product_Img;
                    return PartialView(Product);
                }
                else
                {
                    return PartialView(Product);
                }
            }
            else
            {
                return PartialView();

            }

        }


        public ActionResult Edit(long Productid)
        {
            return PartialView(new ProductBAL().GetProductById(Productid));
        }

        public ContentResult Delete(string id)
        {
            string suc;
            suc = new ProductBAL().DeleteProduct(Convert.ToInt64(id));
            if (suc == "Deleted")
            {
                //return new JsonResult { Data = new { status = true, message = "Product deleted successfully" } };
                return Content("Product deleted successfully", "application/text");
            }
            else
            {
                //return new JsonResult { Data = new { status = false, message = "Oops!! Some error occured" } };
                return Content("Oops!! Some error occured", "application/text");
            }
        }
        [HttpPost]
       // [ValidateAntiForgeryToken]
        public ActionResult SaveProduct(ProductModels data)
        {
            try
            {
                
                if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())                
                {
                    // Get the uploaded image from the Files collection
                    var httpPostedFile = System.Web.HttpContext.Current.Request.Files["UploadedImage"];

                    if (httpPostedFile != null)
                    {
                        // Validate the uploaded image(optional)

                        // Get the complete file path
                        var fileSavePath = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/CompanyLogo"), httpPostedFile.FileName);

                        // Save the uploaded file to "UploadedFiles" folder
                        httpPostedFile.SaveAs(fileSavePath);
                    }
                }
                ProductBAL balProd = new ProductBAL();                
                balProd.Clearing_Rate = data.Clearing_Rate;
                balProd.Consumption_Rate = data.Consumption_Rate;
              
                balProd.Fk_Category_Id = data.Fk_Category_Id;
                balProd.Is_Deleted = false;
                DateTime date = DateTime.ParseExact(data.Manufactur_Date,
                                  "dd/MM/yyyy",
                                  CultureInfo.InvariantCulture);
                balProd.Manufactur_Date = date;
                balProd.Packaging = data.Packaging;
               
                balProd.Price = data.Price;
                balProd.Product_Code = data.Product_Code;
                balProd.Product_Description = data.Product_Description;
                balProd.Product_Img = data.Product_Img;
                balProd.Product_Title = data.Product_Title;
                balProd.Surface_Profile = data.Surface_Profile;                
                string suc="";
                if (data.Pk_Product_Id > 0)
                {
                    balProd.Modified_By = Convert.ToInt64(Session["UserId"]);
                    balProd.Modified_Date = System.DateTime.Now;
                    balProd.Pk_Product_Id = data.Pk_Product_Id;
                    suc = new ProductBAL().EditProduct(balProd);
                }
                else
                {
                    balProd.Created_by = Convert.ToInt64(Session["UserId"]);
                    balProd.Created_Date = System.DateTime.Now;
                    suc = ProductBAL.InsertProduct(balProd);
                }
                if (suc == "Success")
                {
                    return new JsonResult { Data = new { status = true, message = "Product added successfully" } };
                }
                else if (suc == "Updated")
                {
                    return new JsonResult { Data = new { status = true, message = "Product updated successfully" } };
                }
                else
                {
                    return new JsonResult { Data = new { status = false, message = "Oops!! Some error occured" } };
                }

            }
            catch (Exception e)
            {
                return new JsonResult { Data = new { status = false, message = e.Message } };
            }
        }

        public ActionResult ProductForSale()
        {
            try
            {

           
            List<PRODUCT_DETAILS> lst = new ProductBAL().ProductToSale();
            List<ShoppingCartBAL> lstCart = new List<ShoppingCartBAL>();
            if (Session["CartList"] != null)
            {
                lstCart = (List<ShoppingCartBAL>)Session["CartList"];
                StringBuilder str = new StringBuilder();
                foreach (var item in lstCart)
                {
                    str.Append("<a href='#' class='list-group-item'>");
                    str.Append("<label id='lblProdId' >" + item.Fk_Product_Id + "</label>");
                    str.Append("<label id='lblPackId' >" + item.Fk_Package_Id + "</label>");
                    str.Append("<label id='lblcnt' >" + lstCart.Count + "</label>");
                    str.Append("<img src='" + item.ProductImg + "' class='pull-left' alt='" + item.ProductCode + "' />");
                    str.Append("<span class='contacts-title'>" + item.ProductCode + "</span><br /><span class='contacts-title'>" + item.PackageNM + "-" + item.UnitNM + "</span><p>Qty :-" + item.Quantity + "</p>");
                    str.Append("<br /><span class='contacts-title'>" + item.Price + "</span></a>");
                }
            }
            return View(lst);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult ProductDetail(long id)
        {
            PRODUCT_DETAILS product = new ProductBAL().GetProductById(id);
            List<PRODUCT_DETAILS> lst = new List<PRODUCT_DETAILS>();
            lst.Add(product);
            return View(lst);
        }


    }
}
