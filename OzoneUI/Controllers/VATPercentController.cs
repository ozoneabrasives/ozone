﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ozone.BAL;
using Ozone.DAL;
using OzoneUI.Models;

namespace OzoneUI.Controllers
{
    public class VATPercentController : Controller
    {
        //
        // GET: /VATPercent/

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetVAT(int? start, int length)
        {
            int iDisplayStart = start == null ? 0 : start.Value;

            var obj = new  VatBAL().ListVAT(iDisplayStart, length);
            var total = new VatBAL().GetVATCount();

            DatatablePaggeing d = new DatatablePaggeing();
            d.aaData = obj;
            d.iTotalDisplayRecords = total;
            d.iTotalRecords = total;
            d.sEcho = "";

            return new JsonResult { Data = d, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]   
        public ActionResult SaveVAT(VatBAL vat)
        {

            try
            {

                string suc;
                if (vat.Pk_VAT_Id > 0)
                {
                    vat.Modified_By = Convert.ToInt64(Session["UserId"]);
                    suc = new VatBAL().EditVAT(vat);
                }
                else
                {
                    vat.Created_By = Convert.ToInt64(Session["UserId"]);
                    suc = VatBAL.InsertVAT(vat);
                }
                if (suc == "Success")
                {
                    return new JsonResult { Data = new { status = true, message = "VAT Added Successfully" } };
                }
                else if (suc == "Updated")
                {
                    return new JsonResult { Data = new { status = true, message = "VAT Updated Successfully" } };
                }
                else
                {
                    return new JsonResult { Data = new { status = false, message = "Oops!! Some error occured" } };
                }

            }
            catch (Exception e)
            {
                return new JsonResult { Data = new { status = false, message = e.Message } };
            }
        }
    }
}
