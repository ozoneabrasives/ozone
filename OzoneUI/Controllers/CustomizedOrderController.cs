﻿using Newtonsoft.Json;
using Ozone.BAL;
using Ozone.DAL;
using OzoneUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;



namespace OzoneUI.Controllers
{   
    public class CustomizedOrderController : Controller
    {
        //
        // GET: /ExtraQuantity/
        public static List<CartItemsModels> lstItems { get; set; }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddExtra()
        {
            Session.Remove("cartItems");
            CartItemsModels cartItem = new CartItemsModels();
            cartItem.Fk_Shopping_Cart_Id = 0;
            cartItem.lstProduct = new ProductBAL().DDLProduct();
            cartItem.lstPackage = new PackageBAL().DDLPackage();
            int va = 0;
            if (Request.QueryString["CartId"] != null)
            {
                cartItem.Fk_Shopping_Cart_Id = Convert.ToInt64(Request.QueryString["CartId"]);
                va = 1;
            }
            ViewBag.hidval = va;
            return View(cartItem);
        }
        public JsonResult Create(long Itemid, long scartId)
        {
            if (lstItems != null)
            {

            }
            if (Itemid != null)
            {
                CartItemsModels POItem = new CartItemsModels();
                // POItem.Fk_PO_Id = POId;
                POItem.lstProduct = new ProductBAL().DDLProduct();
                POItem.lstPackage = new PackageBAL().DDLPackage();
                if (Itemid > 0)
                {
                    //
                    if (scartId > 0)
                    {
                        SHOPPING_CART_ITEMS balItem = new CartItemBAL().GetItemById(Itemid);
                        POItem.lstUnits = new PackageBAL().DDLUnits(balItem.Fk_Package_Id.ToString());
                        POItem.Pk_Shopping_Cart_Item_Id = Itemid;
                        POItem.Is_Deleted = false;
                        POItem.Create_Date = System.DateTime.Now;
                        POItem.Fk_Shopping_Cart_Id = balItem.Fk_Shopping_Cart_Id;
                        POItem.Fk_Product_Id = (long)balItem.Fk_Product_Id;
                        POItem.Product_Description = balItem.PRODUCT_DETAILS.Product_Description;
                        POItem.Fk_Package_Id = Convert.ToInt64(balItem.Fk_Package_Id.ToString());
                        POItem.Product_Code = balItem.PRODUCT_DETAILS.Product_Code;
                        POItem.Total_Amt = balItem.Total_Amt;
                        POItem.Quantity = balItem.Quantity;
                        POItem.Unit_Price = balItem.Unit_Price;

                    }
                    else
                    {
                        CartItemsModels balPOItem = new CartItemsModels();
                        balPOItem = lstItems.SingleOrDefault(c => c.Pk_Shopping_Cart_Item_Id == Itemid);
                        POItem.lstUnits = new PackageBAL().DDLUnits(balPOItem.Fk_Package_Id.ToString());
                        POItem.Pk_Shopping_Cart_Item_Id = Itemid;
                        POItem.Is_Deleted = false;
                        POItem.Create_Date = System.DateTime.Now;
                        POItem.Fk_Shopping_Cart_Id = balPOItem.Fk_Shopping_Cart_Id;
                        POItem.Fk_Product_Id = (long)balPOItem.Fk_Product_Id;
                        POItem.Product_Description = balPOItem.Product_Description;
                        POItem.Fk_Package_Id = Convert.ToInt64(balPOItem.Fk_Package_Id.ToString());
                        POItem.Product_Code = balPOItem.Product_Code;
                        POItem.Total_Amt = balPOItem.Total_Amt;
                        POItem.Quantity = balPOItem.Quantity;
                        POItem.Unit_Price = balPOItem.Unit_Price;
                    }
                    //POItem.Units = balPOItem.Units;
                    return Json(POItem);
                }
                else
                {
                    POItem.lstUnits = new PackageBAL().DDLUnits("");
                    return Json(POItem);
                }
            }
            else
            {
                return new JsonResult { Data = null, JsonRequestBehavior = JsonRequestBehavior.AllowGet };

            }

        }

        public ActionResult Delete(long id, long scartId)
        {
            string suc;
            if (scartId == 0)
            {
                var dt = lstItems.SingleOrDefault(c => c.Pk_Shopping_Cart_Item_Id == id);
                lstItems.Remove(dt);
                suc = "Deleted";
            }
            else
            {
                suc = new CartItemBAL().DeleteItem(id);
            }
            if (suc == "Deleted")
            {
                return new JsonResult { Data = new { status = true, message = "Item deleted successfully" } };
            }
            else
            {
                return new JsonResult { Data = new { status = false, message = "Oops!! Some error occured" } };
            }
        }

        public ContentResult GetCartItems(int? start, int length, string CartId)
        {
            List<CartItemsModels> lst = new List<CartItemsModels>();
            int iDisplayStart = start == null ? 0 : start.Value;
            if (CartId != "" && CartId != null)
            {
                List<SHOPPING_CART_ITEMS> listcart = new CartItemBAL().GetListById(Convert.ToInt64(CartId));
                foreach (var item in listcart)
                {
                    CartItemsModels modl = new CartItemsModels();
                    modl.Fk_Package_Id = item.Fk_Package_Id;
                    modl.Fk_Product_Id = item.Fk_Product_Id;
                    modl.Fk_Shopping_Cart_Id = item.Fk_Shopping_Cart_Id;
                    modl.Quantity = item.Quantity;
                    modl.Package = item.PACKAGE_MASTER.Package_Title;
                    modl.Unit = item.PACKAGE_MASTER.Package_Unit;
                    modl.Product_Code = item.PRODUCT_DETAILS.Product_Code;
                    modl.Pk_Shopping_Cart_Item_Id = item.Pk_Shopping_Cart_Item_Id;
                    modl.Unit_Price = item.Unit_Price;
                    modl.Total_Amt = item.Total_Amt;
                    lst.Add(modl);
                }
            }
            else
            {
                lst = (List<CartItemsModels>)Session["cartItems"];
                if (lst == null)
                    lst = new List<CartItemsModels>();
            }
            var obj = lst; //new PurchaseOrderBAL().ListPO(iDisplayStart, length);
            var total = lst == null ? 0 : lst.Count;// new PurchaseOrderBAL().GetPOCount();

            DatatablePaggeing d = new DatatablePaggeing();
            d.aaData = obj;
            d.iTotalDisplayRecords = total;
            d.iTotalRecords = total;
            d.sEcho = "";

            var list = JsonConvert.SerializeObject(d,
            Formatting.None,
            new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            });

            return Content(list, "application/json");

            //return new JsonResult { Data = d, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult SaveItem(CartItemsModels PO)
        {
            try
            {
                lstItems = (List<CartItemsModels>)Session["cartItems"];
                SHOPPING_CART_ITEMS dalItem = new SHOPPING_CART_ITEMS();
                string suc = "";
                if (PO.Pk_Shopping_Cart_Item_Id > 0)
                {
                    if (PO.Fk_Shopping_Cart_Id == 0 && lstItems.Count > 0)
                    {
                        var data = lstItems.SingleOrDefault(c => c.Pk_Shopping_Cart_Item_Id == PO.Pk_Shopping_Cart_Item_Id);
                        data.Product_Code = PO.Product_Code;
                        data.Fk_Package_Id = PO.Fk_Package_Id;
                        data.Quantity = PO.Quantity;
                        data.Unit_Price = PO.Unit_Price;
                        data.Fk_Product_Id = PO.Fk_Product_Id;
                        data.Package = PO.Package;
                        data.Unit = PO.Unit;
                        data.Product_Description = PO.Product_Description;
                        data.Total_Amt = PO.Total_Amt;
                        data.Create_By = Convert.ToInt64(Session["UserId"]);
                        data.Create_Date = System.DateTime.Now;
                        suc = "Success";
                    }
                    else
                    {
                        dalItem.Modified_By = Convert.ToInt64(Session["UserId"]);
                        dalItem.Modified_Date = System.DateTime.Now;
                        //dalItem.Product_Code = PO.Product_Code;
                        dalItem.Fk_Package_Id = PO.Fk_Package_Id;
                        dalItem.Quantity = PO.Quantity;
                        dalItem.Unit_Price = PO.Unit_Price;
                        dalItem.Fk_Product_Id = PO.Fk_Product_Id;
                        //dalItem.Package = PO.Package;
                        // dalItem.Unit = PO.Unit;
                        //dalItem.Product_Description = PO.Product_Description;
                        dalItem.Total_Amt = PO.Total_Amt;
                        dalItem.Pk_Shopping_Cart_Item_Id = PO.Pk_Shopping_Cart_Item_Id;
                        dalItem.Fk_Shopping_Cart_Id = PO.Fk_Shopping_Cart_Id;
                        suc = CartItemBAL.EditItems(dalItem);
                    }
                }
                else
                {
                    if (PO.Fk_Shopping_Cart_Id != 0 && PO.Fk_Shopping_Cart_Id != null)
                    {
                        //CartItemBAL item = new CartItemBAL();
                        dalItem.Fk_Product_Id = PO.Fk_Product_Id;
                        //   dalItem.Product_Code = PO.Product_Code;
                        dalItem.Fk_Package_Id = PO.Fk_Package_Id;
                        //item.Package = PO.Package;
                        //item.Unit = PO.Unit;
                        dalItem.Quantity = PO.Quantity;
                        dalItem.Unit_Price = PO.Unit_Price;
                        dalItem.Fk_Shopping_Cart_Id = PO.Fk_Shopping_Cart_Id;
                        //   dalItem.Product_Description = PO.Product_Description;
                        dalItem.Total_Amt = PO.Total_Amt;
                        dalItem.Create_Date = System.DateTime.Now;
                        dalItem.Create_By = Convert.ToInt64(Session["UserId"]);
                        dalItem.Is_Deleted = false;
                        CartItemBAL.AddItem(dalItem);
                    }
                    else
                    {
                        if (lstItems == null)
                        {
                            lstItems = new List<CartItemsModels>();
                        }
                        CartItemsModels item = new CartItemsModels();
                        item.Pk_Shopping_Cart_Item_Id = lstItems.Count + 1;
                        item.Fk_Product_Id = PO.Fk_Product_Id;
                        item.Product_Code = PO.Product_Code;
                        item.Product_Description = PO.Product_Description;
                        item.Total_Amt = PO.Total_Amt;
                        item.Fk_Package_Id = PO.Fk_Package_Id;
                        item.Package = PO.Package;
                        item.Unit = PO.Unit;
                        item.Fk_Shopping_Cart_Id = 0;
                        //item.Units = PO.Units;
                        item.Quantity = PO.Quantity;
                        item.Unit_Price = PO.Unit_Price;
                        item.Create_Date = System.DateTime.Now;
                        item.Create_By = Convert.ToInt64(Session["UserId"]);
                        item.Is_Deleted = false;
                        lstItems.Add(item);
                    }
                    suc = "Success";// POItemsBAL.InsertPOItem(PO);
                }
                bool isvalid = false;
                int tonesum = lstItems.Where(d => d.Unit.Contains("Tones")).Sum(d => d.Quantity).Value;
                if (tonesum == 0)
                {
                    int sumqty = lstItems.Where(d => d.Unit.Contains("K.G")).Sum(d => d.Quantity).Value;
                    if (sumqty > 500)
                    {
                        isvalid = true;
                    }

                }
                else
                {
                    isvalid = true;
                }
               


                Session["cartItems"] = lstItems;
                if (suc == "Success")
                {

                    return new JsonResult { Data = new { status = true, message = "Item added successfully", valid= isvalid } };
                }
                else if (suc == "Updated")
                {
                    return new JsonResult { Data = new { status = true, message = "Item Updated Successfully",valid=isvalid } };
                }
                else
                {
                    return new JsonResult { Data = new { status = false, message = "Oops!! Some error occured" , valid=isvalid } };
                }

            }
            catch (Exception e)
            {
                return new JsonResult { Data = new { status = false, message = e.Message } };
            }
        }


        [HttpPost]
        // [ValidateAntiForgeryToken]
        public ActionResult SaveCartItems(long id)
        {
            try
            {               
                List<SHOPPING_CART_ITEMS> lstCartItem = new List<SHOPPING_CART_ITEMS>();
                List<CartItemsModels> lstItems = (List<CartItemsModels>)Session["cartItems"];
                string suc = "";
                USER_REGISTRATION usr = new RegisterBAL().BindById(Convert.ToInt64(Session["UserId"]));

                if (lstItems != null)
                {
                    foreach (var item in lstItems)
                    {
                        SHOPPING_CART_ITEMS balCart = new SHOPPING_CART_ITEMS();
                        balCart.Create_By = Convert.ToInt64(Session["UserId"]);
                        balCart.Create_Date = System.DateTime.Now;
                        balCart.Fk_Package_Id = item.Fk_Package_Id;
                        balCart.Fk_Product_Id = item.Fk_Product_Id;
                        balCart.Fk_Shopping_Cart_Id = 0;
                        balCart.Is_Deleted = false;
                        balCart.Is_Extra = true;
                        balCart.Quantity = item.Quantity;
                        balCart.Total_Amt = item.Total_Amt;
                        balCart.Unit_Price = item.Unit_Price;
                        lstCartItem.Add(balCart);
                    }
               
                    ShoppingCartBAL bal = new ShoppingCartBAL();
                    bal.Create_By = Convert.ToInt64(Session["UserId"]);
                    bal.Create_Date = System.DateTime.Now;
                    string orderNo = "OD" + DateTime.Now.Year + ShoppingCartDAL.GetLastId().ToString("D" + 5);
                    //Random ran = new Random();
                    //string Num = "I_" + ran.Next();
                    bal.Order_Number = orderNo;
                    bal.Order_Status = "Extra";
                    bal.Payment_Status = "Pending";
                    bal.Full_Name = usr.User_Name;
                    bal.Phone_No = usr.Mobile_No;
                    bal.lstItems = lstCartItem;
                    suc = CartItemBAL.InsertCartItem(bal);
                }
                else
                {
                    string orderNo = new ShoppingCartBAL().GetById(id).Order_Number;
                    suc = Utility.SendGeneralMail("po@ozoneabrasives.co.uk", "", "", orderNo);
                }
                if (suc == "Success")
                {
                    #region PDF Generate Portion
                    //string FilePath = "";
                    //string FullPath = Path.Combine(Server.MapPath("/PO_Generate/"), PO.PO_Number);
                    // FilePath = Path.Combine(FullPath, PO.PO_Number + ".pdf");
                    //DateTime date = DateTime.Parse(PO.PO_Date, (IFormatProvider)CultureInfo.InvariantCulture);
                    //POModels po = new POModels();
                    //po.Grand_Total = PO.Grand_Total;
                    //po.Ship_Name = PO.Ship_Name;
                    //po.Ship_Address = PO.Ship_Address;
                    //po.Checked_By = PO.Checked_By;
                    //po.Document_Req = PO.Document_Req;
                    //po.Payment_Terms = PO.Payment_Terms;
                    //po.PO_Date = date;
                    //po.PO_Number = PO.PO_Number;
                    //po.Ship_City = PO.Ship_City;
                    //po.Ship_Compny = PO.Ship_Compny;
                    //po.Ship_Country = PO.Ship_Country;
                    //po.Ship_Phone = PO.Ship_Phone;
                    //po.Ship_State = PO.Ship_State;
                    //po.Ship_Zip_code = PO.Ship_Zip_code;
                    //po.Shipment_Detail = PO.Shipment_Detail;
                    //po.Transport = PO.Transport;
                    //po.lstPOI = PurchaseOrderItemsController.lstItems;
                    //var actionResult = new Rotativa.ViewAsPdf("GeneratePDF", po);

                    //var byteArray = actionResult.BuildPdf(ControllerContext);
                    //Directory.CreateDirectory(FullPath);
                    //var fileStream = new FileStream(FilePath, FileMode.Create, FileAccess.Write);
                    //fileStream.Write(byteArray, 0, byteArray.Length);
                    //fileStream.Close();
                    PurchaseOrderItemsController.lstItems = null;
                    #endregion
                    #region Mail Portion
                  
                    Session.Remove("cartItems");
                    #endregion
                    return new JsonResult { Data = new { status = true, message = "PO added successfully" } };
                }
                else if (suc == "Updated")
                {
                    return new JsonResult { Data = new { status = true, message = "PO Updated Successfully" } };
                }
                else
                {
                    return new JsonResult { Data = new { status = false, message = "Oops!! Some error occured" } };
                }

            }
            catch (Exception e)
            {
                return new JsonResult { Data = new { status = false, message = e.Message } };
            }
        }


        public ContentResult GetExtraItem(int? start, int length)
        {
            int iDisplayStart = start == null ? 0 : start.Value;

            var obj = new ShoppingCartBAL().ListExtraCart(iDisplayStart, length, Session["UserType"].ToString(), Convert.ToInt64(Session["UserId"].ToString()));
            ///var total = new ShoppingCartBAL().GetExtraCount(Session["UserType"].ToString(), Convert.ToInt64(Session["UserId"].ToString()));
            var total = new ShoppingCartBAL().GetExtraCount(Session["UserType"].ToString(), Convert.ToInt64(Session["UserId"].ToString()));

            DatatablePaggeing d = new DatatablePaggeing();
            d.aaData = obj;
            d.iTotalDisplayRecords = total;
            d.iTotalRecords = total;
            d.sEcho = "";

            var list = JsonConvert.SerializeObject(d,
            Formatting.None,
            new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            });

            return Content(list, "application/json");
        }
    }
}
