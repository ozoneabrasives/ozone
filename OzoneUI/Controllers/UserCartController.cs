﻿using Ozone.BAL;
using OzoneUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Ozone.DAL;
using Newtonsoft.Json;
using System.IO;
using Rotativa.MVC;

namespace OzoneUI.Controllers
{
    public class UserCartController : Controller
    {
        //
        // GET: /UserCart/

        public ActionResult Index()
        {
            StringBuilder str = new StringBuilder();
            //List<CartModels> lstCart = new List<CartModels>();
            List<ShoppingCartBAL> lst = (List<ShoppingCartBAL>)Session["CartList"];
            int i = 0;
            if (lst != null && lst.Count>0)
            {

                decimal tax = 0;// new TaxMasterBAL().GetTaxByUserid(lst[0].Create_By.Value, lst.Sum(d => d.Quantity).Value);
                foreach (var item in lst)
                {
                    i++;
                    str.Append("<a href='/UserCart/Index'  class='list-group-item'>");
                    str.Append("<label id='lblProdId' >" + item.Fk_Product_Id + "</label>");
                    str.Append("<label id='lblPackId' >" + item.Fk_Package_Id + "</label>");
                    str.Append("<label id='lblcnt' >" + lst.Count + "</label>");
                    str.Append("<img src='" + item.ProductImg + "' class='pull-left' style='width:70px;height:70px;' alt='John Doe' />");
                    str.Append("<span class='contacts-title'>" + item.ProductCode + "</span><br /><span class='contacts-title'>" + item.PackageNM + "-" + item.UnitNM + "</span><p>Qty :" + item.Quantity + "&nbsp;&nbsp;&nbsp;Price :" + item.Price + "</p>");
                    //str.Append("Price :<span class='contacts-title'>" + item.Price + "</span></a>");
                    str.Append("<label id='lblCartId' style='visibility: hidden;'>" + i + "</label></a>");
                    item.TAX = tax;
                }
                
            }
            else
            {
                return RedirectToAction("CompnyIndex", "Home");
            }
            // new HtmlString("<a href="'/Home/Profile/seeker'">seeker</a> has applied to <a href="'/Jobs/Details/9'">Job</a> floated by you.</br>");
            IHtmlString strHtml = new HtmlString(str.ToString());
            Session["PopUp"] = strHtml;
            return View(lst);
        }

        public ActionResult Create(string number, string status, string payStatus)
        {
            CartModels cart = new CartModels();
            cart.Order_Number = number;
            cart.Order_Status = status;
            cart.Payment_Status = payStatus;
            return PartialView(cart);
        }
        [HttpPost]
        public ActionResult SaveCart(ShoppingCartBAL Scart)
        {
            string suc = "Success";
            Scart.Create_By = Convert.ToInt64(Session["UserId"]);
            Scart.Create_Date = System.DateTime.Now;

            int totoalqty = (Scart.unit * Scart.Quantity).Value;
            List<ShoppingCartBAL> lstCart = new List<ShoppingCartBAL>();
            if (Session["CartList"] != null)
            {
                lstCart = (List<ShoppingCartBAL>)Session["CartList"];
                totoalqty += lstCart.Sum(d => d.Quantity* d.unit).Value;
                
            }

            if ((totoalqty + Scart.Quantity) > 500)
            {
                suc = "errqty";
            }
            else
            {
                lstCart.Add(Scart);
            }

           
            Session["CartList"] = lstCart;
            StringBuilder str = new StringBuilder();
            int i = 0;
            foreach (var item in lstCart)
            {
                i++;
                item.Pk_UserCart_Id = i;
                //str.Append("<a href='#'  class='list-group-item'>");
                //str.Append("<label id='lblProdId' >" + item.Fk_Product_Id + "</label>");
                //str.Append("<label id='lblPackId' >" + item.Fk_Package_Id + "</label>");
                //str.Append("<label id='lblcnt' >" + lstCart.Count + "</label>");
                //str.Append("<img src='" + item.ProductImg + "' class='pull-left' alt='John Doe' />");
                //str.Append("<span class='contacts-title'>" + item.ProductCode + "</span><br /><span class='contacts-title'>" + item.PackageNM + "-" + item.UnitNM + "</span><p>Qty :-" + item.Quantity + "</p>");
                //str.Append("<br /><span class='contacts-title'>" + item.Price + "</span></a>");
                //str.Append("<label id='lblCartId' style='visibility: hidden;'>" + i + "</label>");
                str.Append("<a href='/UserCart/Index'  class='list-group-item'>");
                str.Append("<label id='lblProdId' >" + item.Fk_Product_Id + "</label>");
                str.Append("<label id='lblPackId' >" + item.Fk_Package_Id + "</label>");
                str.Append("<label id='lblcnt' >" + lstCart.Count + "</label>");
                str.Append("<img src='" + item.ProductImg + "' class='pull-left'  style='width:70px;height:70px;' alt='John Doe' />");
                str.Append("<span class='contacts-title'>" + item.ProductCode + "</span><br /><span class='contacts-title'>" + item.PackageNM + "-" + item.UnitNM + "</span><p>Qty :" + item.Quantity + "&nbsp;&nbsp;&nbsp;Price :" + item.Price + "</p>");
                //str.Append("Price :<span class='contacts-title'>" + item.Price + "</span></a>");
                str.Append("<label id='lblCartId' style='visibility: hidden;'>" + i + "</label></a>");
            }
            IHtmlString strHtml = new HtmlString(str.ToString());
            Session["PopUp"] = strHtml;
            //suc = ShoppingCartBAL.InsertCart(Scart);
            if (suc == "Success")
            {
                return new JsonResult { Data = new { status = true, message = "Item Added Successfully", success = str.ToString() } };
            }
            else if (suc=="errqty")
            {
                return new JsonResult { Data = new { status = false, message = "err", success = str.ToString() } };
            }
            else
            {
                return new JsonResult { Data = new { status = false, message = "Oops!! Some error occured", success = str.ToString() } };
            }
        }

        public ActionResult CheckOutProduct(ShoppingCartBAL balScart)
        {
            
            string str = "";
            CartModels balShop = new CartModels();
            balShop.vatPercent = new VatBAL().GetVATById(1).VAT_Percent1;
            if (Request.QueryString["CartId"] != null)
            {
                
                
                long id = Convert.ToInt64(Request.QueryString["CartId"]);
                List<SHOPPING_CART_ITEMS> lst = new List<SHOPPING_CART_ITEMS>();
                lst = new CartItemBAL().GetListById(id);
                List<ShoppingCartBAL> lstbal = new List<ShoppingCartBAL>();
                decimal tax = new TaxMasterBAL().GetTaxByUserid(lst[0].Create_By.Value, lst.Sum(d => d.Quantity).Value);
                foreach (var item in lst)
                {
                    ShoppingCartBAL bal = new ShoppingCartBAL();

                    //  PRODUCT_DETAILS balProd = new ProductBAL().GetProductById((long)item.Fk_Product_Id);
                    bal.ProductImg = "/CompanyLogo/" + item.PRODUCT_DETAILS.Product_Img;// balProd.Product_Img;
                    bal.ProductCode = " :" + item.PRODUCT_DETAILS.Product_Code; //balProd.Product_Code;
                    bal.Fk_Product_Id = Convert.ToInt64(item.Fk_Product_Id);
                    bal.Fk_Package_Id = Convert.ToInt64(item.Fk_Package_Id);
                    bal.PackageNM = " :" + item.PACKAGE_MASTER.Package_Title;
                    bal.UnitNM = " :" + item.PACKAGE_MASTER.Package_Unit;
                    bal.Quantity = item.Quantity;
                    bal.Price = Convert.ToInt32(item.Unit_Price);
                    bal.TAX = tax;
                    //bal.Pk_UserCart_Id =(long)item.Fk_Shopping_Cart_Id;
                    lstbal.Add(bal);
                }
                balShop.Pk_UserCart_Id = id;
                balShop.lstCart = lstbal;
                Session["CheckoutItem"] = lstbal;
            }
            if (balScart.Full_Name == null || balScart.Full_Name == string.Empty)
            {
                if (Request.QueryString["CartId"] == null)
                {
                    balShop.lstCart = (List<ShoppingCartBAL>)Session["CartList"];
                }
                balShop.lstCountry = new CountryBAL().DDLCountry();
                balShop.lstState = new List<Ozone.DAL.STATE_MASTER>();
                balShop.ddlCart = new ShoppingCartBAL().ListCart(0, 100, "Company", Convert.ToInt64(Session["UserId"]), "");
               // Session["OrderList"] = balShop.ddlCart.Select(c => new SHOPPING_CART() { Del_Address = c.Del_Address,Pk_UserCart_Id=c.Pk_UserCart_Id }).ToList();
                if (Session["UserId"] != null)
                {
                    SHOPPING_CART shop = new ShoppingCartBAL().GetById(Convert.ToInt64(Session["UserId"]));
                    if (shop != null)
                    {
                        balShop.Del_Address = shop.Del_Address;
                        balShop.Del_City = shop.Del_City;
                        balShop.Del_Country = shop.Del_Country;
                        balShop.Del_Zipcode = shop.Del_Zipcode;
                        balShop.Full_Name = shop.Full_Name;
                        balShop.Phone_No = shop.Phone_No;
                        balShop.lstState = new StateBAL().DDLState(Convert.ToInt64(shop.Del_Country));
                        balShop.Del_State = shop.Del_State;

                    }
                }

                return View(balShop);
            }
            else
            {
                List<ShoppingCartBAL> lst = (List<ShoppingCartBAL>)Session["CartList"];
                if (lst != null)
                {
                    if (Request["rgrp"] == "1")
                    {
                        TempData["extradata"]= balScart;
                        return RedirectToAction("PaymentWithPaypal", "Paypal");

                    }
                    else
                    {

                        List<SHOPPING_CART_ITEMS> lstItems = new List<SHOPPING_CART_ITEMS>();
                        foreach (var item in lst)
                        {
                            SHOPPING_CART_ITEMS items = new SHOPPING_CART_ITEMS();
                            items.Create_By = Convert.ToInt64(Session["UserId"]);
                            items.Create_Date = System.DateTime.Now;
                            items.Fk_Package_Id = item.Fk_Package_Id;
                            items.Fk_Product_Id = item.Fk_Product_Id;
                            items.Is_Deleted = false;
                            items.Quantity = item.Quantity;
                            items.Unit_Price = item.Price;
                            items.Total_Amt = item.Total;
                            items.PRODUCT_DETAILS = new ProductBAL().GetProductById(item.Fk_Product_Id);
                            items.PACKAGE_MASTER = new PackageBAL().GetPackageById(item.Fk_Package_Id);
                            lstItems.Add(items);
                        }
                        string orderNo = "SO" + DateTime.Now.Year + ShoppingCartDAL.GetLastId().ToString("D" + 5);
                        string r = Request["rgrp"];
                        Random ran = new Random();
                        string Num = "I_" + ran.Next();
                        balScart.Order_Number = orderNo;
                       
                            balScart.Payment_Status = "Credited";
                        
                        balScart.Order_Status = "Requested";
                        balScart.Create_By = Convert.ToInt64(Session["UserId"]);
                        balScart.Fk_Product_Id = 1;
                        balScart.Fk_Package_Id = 1;
                        balScart.lstItems = lstItems;
                        str = CartItemBAL.InsertCartItem(balScart);
                      string details ="<b>hello " + balScart.Full_Name + "</b> </br> Your are now Credit Customer";
                      SHOPPING_CART objshop = new ShoppingCartBAL().GetcartByDo(orderNo);
                     
                      string FullPath = Path.Combine(Server.MapPath("/Invoice_Generate/"), objshop.USER_REGISTRATION.Company_Name + "/" + objshop.Order_Number);
                      string FilePath = Path.Combine(FullPath, objshop.Order_Number + ".pdf");
                      Directory.CreateDirectory(FullPath);
                      var actionResult = new ViewAsPdf("GenerateDO", objshop);
                      var byteArray = actionResult.BuildPdf(ControllerContext);

                      var fileStream = new FileStream(FilePath, FileMode.Create, FileAccess.Write);
                      fileStream.Write(byteArray, 0, byteArray.Length);
                      fileStream.Close();
                        var d = new RegisterBAL().BindById(Convert.ToInt64(Session["UserId"]));
                        Utility.SendEmaildynamic(d.Email_ID, balScart.Full_Name, "Purchase Order", details);
                    }
                }
                else
                {
                    if (Session["CheckoutItem"] == null)
                    {
                        balShop.lstCountry = new CountryBAL().DDLCountry();
                        balShop.lstState = new List<Ozone.DAL.STATE_MASTER>();
                        balShop.ddlCart = new ShoppingCartBAL().ListCart(0, 100, "Company", Convert.ToInt64(Session["UserId"]), "");
                        return View(balShop);
                    }
                    List<ShoppingCartBAL> lstcart = (List<ShoppingCartBAL>)Session["CheckoutItem"];
                    balScart.lstItems = lstcart[0].lstItems;
                    //balScart.Pk_UserCart_Id =Convert.ToInt64( Request.QueryString["CartId"]);
                    balScart.Modified_By = Convert.ToInt64(Session["UserId"]);
                    if (balScart.Is_Deleted == true)
                    {
                        balScart.Payment_Status = "Credited";
                    }
                    else
                    {
                        balScart.Payment_Status = "Pending";
                    }
                    balScart.Order_Status = "Requested";
                    str = CartItemBAL.UpdateCart(balScart);
                    Session.Remove("CheckoutItem");
                }
                if (str == "Success" || str == "Updated")
                {
                    Utility.SendOrderMail("po@ozoneabrasives.co.uk", balScart);
                }
                Session.Remove("CartList");
                Session.Remove("PopUp");
                return Redirect("../../UserCart/GetCart");
            }
        }

        [HttpPost]
        public ActionResult RemoveCart(long ScartId, int qty)
        {
            string suc = "Success";
            List<ShoppingCartBAL> lstCart = new List<ShoppingCartBAL>();
            if (Session["CartList"] != null)
            {
                lstCart = (List<ShoppingCartBAL>)Session["CartList"];
            }

            ShoppingCartBAL bal = lstCart.SingleOrDefault(c => c.Pk_UserCart_Id == ScartId);
            if (qty > 0)
            {
                lstCart.Where(c => c.Pk_UserCart_Id == ScartId).ToList().ForEach(d => d.Quantity = qty);
            }
            else
            {
                lstCart.Remove(bal);
            }
            Session["CartList"] = lstCart;
            StringBuilder str = new StringBuilder();
            int i = -1;
            foreach (var item in lstCart)
            {
                i++;
                //str.Append("<a href='#'  class='list-group-item'>");
                //str.Append("<label id='lblProdId' >" + item.Fk_Product_Id + "</label>");
                //str.Append("<label id='lblPackId' >" + item.Fk_Package_Id + "</label>");
                //str.Append("<label id='lblcnt' >" + lstCart.Count + "</label>");
                //str.Append("<img src='" + item.ProductImg + "' class='pull-left' alt='John Doe' />");
                //str.Append("<span class='contacts-title'>" + item.ProductCode + "</span><br /><span class='contacts-title'>" + item.PackageNM + "-" + item.UnitNM + "</span><p>Qty :-" + item.Quantity + "</p>");
                //str.Append("<br /><span class='contacts-title'>" + item.Price + "</span></a>");
                //str.Append("<label id='lblCartId' style='visibility: hidden;'>" + i + "</label>");
                str.Append("<a href='/UserCart/Index'  class='list-group-item'>");
                str.Append("<label id='lblProdId' >" + item.Fk_Product_Id + "</label>");
                str.Append("<label id='lblPackId' >" + item.Fk_Package_Id + "</label>");
                str.Append("<label id='lblcnt' >" + lstCart.Count + "</label>");
                str.Append("<img src='" + item.ProductImg + "' class='pull-left' alt='John Doe'  style='width:70px;height:70px;' />");
                str.Append("<span class='contacts-title'>" + item.ProductCode + "</span><br /><span class='contacts-title'>" + item.PackageNM + "-" + item.UnitNM + "</span><p>Qty :" + item.Quantity + "&nbsp;&nbsp;&nbsp;Price :" + item.Price + "</p>");
                //str.Append("Price :<span class='contacts-title'>" + item.Price + "</span></a>");
                str.Append("<label id='lblCartId' style='visibility: hidden;'>" + i + "</label></a>");
            }
            IHtmlString strHtml = new HtmlString(str.ToString());
            Session["PopUp"] = strHtml;
            //suc = ShoppingCartBAL.InsertCart(Scart);
            if (suc == "Success")
            {
                return new JsonResult { Data = new { status = true, message = "Item Changed Successfully", success = str.ToString() } };
            }
            else
            {
                return new JsonResult { Data = new { status = false, message = "Oops!! Some error occured", success = str.ToString() } };
            }
        }

        public ActionResult GetCart()
        {
            return View();
        }

        public ContentResult BindData(int? start, int length,string st)
        {
            int iDisplayStart = start == null ? 0 : start.Value;

            var obj = new ShoppingCartBAL().ListCart(iDisplayStart, length, Session["UserType"].ToString(), Convert.ToInt64(Session["UserId"]),st);
            var total = new ShoppingCartBAL().GetProductCount(Session["UserType"].ToString(), Convert.ToInt64(Session["UserId"]), st);

            DatatablePaggeing d = new DatatablePaggeing();
            d.aaData = obj;
            d.iTotalDisplayRecords = total;
            d.iTotalRecords = total;
            d.sEcho = "";
            var list = JsonConvert.SerializeObject(d,
           Formatting.None,
           new JsonSerializerSettings()
           {
               ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
           });

            return Content(list, "application/json");
            //return new JsonResult { Data = d, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        public ContentResult GetGRN(int? start, int length, string startdate, string enddate, int poid)
        {
            int iDisplayStart = start == null ? 0 : start.Value;

            var obj = new CartItemBAL().ListGRN(iDisplayStart, length, poid);
            //var total = new PurchaseOrderBAL().GetPOCount(Convert.ToInt64(Session["UserId"].ToString()), Convert.ToString(Session["UserType"]));
            var total = new CartItemBAL().GetGrnCount(poid);

            DatatablePaggeing d = new DatatablePaggeing();
            d.aaData = obj;
            d.iTotalDisplayRecords = total;
            d.iTotalRecords = total;
            d.sEcho = "";

            var list = JsonConvert.SerializeObject(d,
            Formatting.None,
            new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            });

            return Content(list, "application/json");
        }
        public ContentResult GetInvoice(int? start, int length, string startdate, string enddate, int poid)
        {
            int iDisplayStart = start == null ? 0 : start.Value;

            var obj = new CartItemBAL().ListInvoice(iDisplayStart, length, poid);
            //var total = new PurchaseOrderBAL().GetPOCount(Convert.ToInt64(Session["UserId"].ToString()), Convert.ToString(Session["UserType"]));
            var total = new CartItemBAL().GetInvoiceCount(poid);

            DatatablePaggeing d = new DatatablePaggeing();
            d.aaData = obj;
            d.iTotalDisplayRecords = total;
            d.iTotalRecords = total;
            d.sEcho = "";

            var list = JsonConvert.SerializeObject(d,
            Formatting.None,
            new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            });

            return Content(list, "application/json");
        }

        public JsonResult OrderUpdate(CartModels model)
        {
            string result = new ShoppingCartBAL().UpdateStatus(model.Order_Number, model.Order_Status, model.Order_Note, model.Payment_Status);
            // return Content(result, "application/text");
            if (result == "Stauts Updated Successfully")
            {
                Utility.SendGeneralMail("po@ozoneabrasives.co.uk", "", model.Order_Status + ":" + model.Order_Note, model.Order_Number);
            }
            return new JsonResult { Data = new { status = true, message = result }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public ActionResult TrackOrder(long orderId)
        {
            SHOPPING_CART cart = new ShoppingCartBAL().GetById(orderId);
            CartModels model = new CartModels();
            model.Order_Status = cart.Order_Status;
            model.Accepted_Note = cart.Accepted_Note;
            model.Delivery_Note = cart.Delivery_Note;
            model.Order_Note = cart.Order_Note;
            model.Processed_Note = cart.Processed_Note;
            model.Rejected_Note = cart.Rejected_Note;
            model.Shipped_Note = cart.Shipped_Note;
            model.Order_Number = cart.Order_Number;

            return View(model);
        }

        public ActionResult GetCredits()
        {
            return View();
        }

        public ContentResult BindCreditData(int? start, int length,long id)
        {
            int iDisplayStart = start == null ? 0 : start.Value;

            var obj = new ShoppingCartBAL().ListCreditCart(iDisplayStart, length, Session["UserType"].ToString(), id);
            var total = new ShoppingCartBAL().GetCreditCount(Session["UserType"].ToString(), id);

            DatatablePaggeing d = new DatatablePaggeing();
            d.aaData = obj;
            d.iTotalDisplayRecords = total;
            d.iTotalRecords = total;
            d.sEcho = "";
            var list = JsonConvert.SerializeObject(d,
           Formatting.None,
           new JsonSerializerSettings()
           {
               ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
           });

            return Content(list, "application/json");
            //return new JsonResult { Data = d, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }


        public ActionResult ViewOrder(long OrderId)
        {
            CartModels balShop = new CartModels();
            balShop.lstCountry = new CountryBAL().DDLCountry();
            balShop.lstState = new List<Ozone.DAL.STATE_MASTER>();
             
                SHOPPING_CART shop = new ShoppingCartBAL().GetById(OrderId);
                if (shop != null)
                {
                    balShop.Del_Address = shop.Del_Address;
                    balShop.Del_City = shop.Del_City;
                    balShop.Del_Country = shop.Del_Country;
                    balShop.Del_Zipcode = shop.Del_Zipcode;
                    balShop.Full_Name = shop.Full_Name;
                    balShop.Phone_No = shop.Phone_No;                    
                    balShop.Del_State = shop.Del_State;
                    balShop.Card_Cvv = shop.Card_Cvv;
                    balShop.Card_Detail = shop.Card_Detail;
                    balShop.Card_Exp_Month = shop.Card_Exp_Month;
                    balShop.Card_Exp_Year = shop.Card_Exp_Year;
                    balShop.Card_No = shop.Card_No;
                    balShop.Order_Number = shop.Order_Number;
                    balShop.Order_Status = shop.Order_Status;
                    balShop.Payment_Amt =Convert.ToDecimal( shop.Payment_Amt);
                    balShop.Payment_Card = shop.Payment_Card;
                    balShop.ShipRate = shop.Shipp_Rate.Value;
                    balShop.VAT_Amount = shop.VAT_Amount.Value;
                    balShop.Username = new RegisterBAL().BindById(shop.Create_By.Value).Company_Name;

                    balShop.Payment_Status = shop.Payment_Status;
                    balShop.Phone_No = shop.Phone_No;
                    balShop.Quantity = shop.Quantity;
                    
                    balShop.lstDALItems = new CartItemBAL().GetListById(OrderId);
                }
             
            return View(balShop);
        }

        [HttpGet]
        public JsonResult GetOrderById(long OrderId)        
        {
            CartModels modl = new CartModels();
            SHOPPING_CART cart = new ShoppingCartBAL().GetById(OrderId);
            modl.Full_Name = cart.Full_Name;
            modl.Del_Address = cart.Del_Address;
            modl.Del_City = cart.Del_City;
            modl.Del_Country = cart.Del_Country;
            modl.lstState = new StateBAL().DDLState((long)cart.Del_Country);
            modl.Del_State = cart.Del_State;
            modl.Del_Zipcode = cart.Del_Zipcode;
            modl.Phone_No = cart.Phone_No;
            return Json(modl, JsonRequestBehavior.AllowGet);
             
        }
        public ActionResult ViewItems(long soid)
        {
            DOItemModel objmodel = new DOItemModel();
            objmodel.lstitems = new ShoppingCartBAL().GetlistbyGRN(soid);
            return PartialView(objmodel);

        }
          [HttpPost]
        public JsonResult UpdateSO(List<SHOPPING_CART_ITEMS> lstshoping)
        {
            var d = new CartItemBAL().UpdateInventory(lstshoping);
SHOPPING_CART objshopcart = new ShoppingCartBAL().GetById(lstshoping[0].Fk_Shopping_Cart_Id.Value);
              objshopcart.SHOPPING_CART_ITEMS= d;


              string FullPath = Path.Combine(Server.MapPath("/Invoice_Generate/"), objshopcart.USER_REGISTRATION.Company_Name + "/" + objshopcart.Order_Number);
              string FilePath = Path.Combine(FullPath, d[0].GRNNumber + ".pdf");
              Directory.CreateDirectory(FullPath);
              var actionResult = new ViewAsPdf("GenerateGRN", objshopcart);
              var byteArray = actionResult.BuildPdf(ControllerContext);

              var fileStream = new FileStream(FilePath, FileMode.Create, FileAccess.Write);
              fileStream.Write(byteArray, 0, byteArray.Length);
              fileStream.Close();
             

              FullPath = Path.Combine(Server.MapPath("/Invoice_Generate/"), objshopcart.USER_REGISTRATION.Company_Name + "/" + objshopcart.Order_Number);
          var invoice = InvoiceBAL.GetInvByID(d[0].pkinvoiceid);
          objshopcart.Salesordernumber = objshopcart.Order_Number;
          objshopcart.Order_Number = invoice.InvoiceNumber;
          objshopcart.Shipp_Rate = invoice.ShippingRate;
          objshopcart.VAT_Amount = invoice.Vat;
          objshopcart.Payment_Amt = objshopcart.SHOPPING_CART_ITEMS.Sum(a => a.Total_Amt) + invoice.ShippingRate + invoice.Vat;
          objshopcart.DONumber = invoice.DoNumber;
             
              FilePath = Path.Combine(FullPath, invoice.InvoiceNumber + ".pdf");
              Directory.CreateDirectory(FullPath);
              actionResult = new ViewAsPdf("GenerateInvoice", objshopcart);
              byteArray = actionResult.BuildPdf(ControllerContext);

              fileStream = new FileStream(FilePath, FileMode.Create, FileAccess.Write);
              fileStream.Write(byteArray, 0, byteArray.Length);
              fileStream.Close();


            return new JsonResult { Data = new { status = true, message = "Delivery Order Generated Successfully" } };
        }
          public ActionResult GenerateGRN( SHOPPING_CART objshop)
          {
            

              return View(objshop);
          }
          public ActionResult AddPayment(long invid)
          {
              InvoiceTab invtab = InvoiceBAL.GetInvByID(invid);
              invtab.TotalCost = invtab.TotalCost + invtab.ShippingRate;
              return View(invtab);
          }
          public ActionResult GenerateDO(SHOPPING_CART orderNo)
          {
              orderNo.Name_On_Card = NumberToWords(Convert.ToInt32( orderNo.Payment_Amt));
              return View(orderNo);

          }
          public ActionResult GenerateInvoice(SHOPPING_CART objshop)
          {
              objshop.Name_On_Card = NumberToWords(Convert.ToInt32(objshop.Payment_Amt));

              return View(objshop);
          }
          public ActionResult ListofGRN(long Poid = 0)
          {
              return View();
          }
          public ActionResult ListOfInvoice(long poid = 0)
          {
              return View();
          }
          public ActionResult ListOfPayments(long invid)
          {
              ListofInvoicePayment model = new ListofInvoicePayment();
              model.lstpayment= InvoiceBAL.lstofpayment(invid);
           
              return View(model);
          }
        [HttpPost]
          public ActionResult GetTaxByCountry(int Countryid)
          {
              List<ShoppingCartBAL> lst = (List<ShoppingCartBAL>)Session["CartList"];
              int totalqty = lst.Sum(d => d.Quantity * d.unit).Value;
             
            double shipqty = Convert.ToDouble( totalqty) /(double)25;
            if (Convert.ToInt32(shipqty.ToString("F").Split('.')[1]) != 0)
                shipqty++;
            int qty = Convert.ToInt32(shipqty);
            qty = qty == 0 ? 1 : qty;

              var tax = new TaxMasterBAL().GetTaxByUserid(Countryid, qty);
              lst.ForEach(d => d.TAX = tax);
              return new JsonResult {Data=tax };
          }
        [HttpPost]
        public ActionResult AddInvoicePayment(InvoiceTab objinvoice)
        {
            var result = InvoiceBAL.Addpayment(objinvoice);
            return new JsonResult { Data = result };
        }

        public string NumberToWords(int number)
        {
            if (number == 0)
                return "zero";

            if (number < 0)
                return "minus " + NumberToWords(Math.Abs(number));

            string words = "";

            if ((number / 1000000) > 0)
            {
                words += NumberToWords(number / 1000000) + " million ";
                number %= 1000000;
            }

            if ((number / 1000) > 0)
            {
                words += NumberToWords(number / 1000) + " thousand ";
                number %= 1000;
            }

            if ((number / 100) > 0)
            {
                words += NumberToWords(number / 100) + " hundred ";
                number %= 100;
            }

            if (number > 0)
            {
                if (words != "")
                    words += "and ";

                var unitsMap = new[] { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
                var tensMap = new[] { "zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

                if (number < 20)
                    words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0)
                        words += "-" + unitsMap[number % 10];
                }
            }

            return words;
        }
    }
}
