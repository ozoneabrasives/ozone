﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OzoneUI.Models;
using Ozone.BAL;



namespace OzoneUI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (Session["UserType"] == null || Session["UserId"] == null)
            {
                return RedirectToAction("LoginUsr", "RegisterView");
            }
            int iDisplayStart =0;
            string st = "";
            DashboardModels model = new DashboardModels();
            model.lstCart = new ShoppingCartBAL().ListCart(iDisplayStart, 10, Session["UserType"].ToString(), Convert.ToInt64(Session["UserId"]), st);
            model.lstPO = new PurchaseOrderBAL().ListPO(iDisplayStart, 10, Convert.ToInt64(Session["UserId"]), Session["UserType"].ToString(),"","",0,"").Take(5).ToList();
            model.lstPayment = new ShoppingCartBAL().getListByPaymentStatus();
            model.lstinvoice =  InvoiceBAL.GetListofinvbydate();
            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult CompnyIndex()
        {
            if (Session["UserType"] == null || Session["UserId"] == null)
            {
                return RedirectToAction("LoginUsr", "RegisterView");
            }
            int iDisplayStart = 0;
            string st = "";
            DashboardModels model = new DashboardModels();
            model.lstCart = new ShoppingCartBAL().ListCart(iDisplayStart, 10, Session["UserType"].ToString(), Convert.ToInt64(Session["UserId"]), st);
            model.lstPending = new ShoppingCartBAL().ListCart(iDisplayStart, 10, Session["UserType"].ToString(), Convert.ToInt64(Session["UserId"]), st).Where(c => c.Order_Status == "Requested").ToList();
            return View(model);
        }
    }
}
