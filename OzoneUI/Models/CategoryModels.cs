﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OzoneUI.Models
{
    public class CategoryModels
    {
        public long Pk_Category_Id { get; set; }
        [Required]
        [Display(Name = "Category")]
        public string Category_Name { get; set; }

        [Display(Name = "Category Description")]
        public string Category_Description { get; set; }
        public Nullable<long> Created_by { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<long> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public Nullable<bool> Is_Deleted { get; set; }
    }
}