﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OzoneUI.Models
{
    public class ShipRateUKModel
    {
        
        public long Rateid { get; set; }

    [Required(ErrorMessage="Quantity is Required")]
        [Display(Name="Quantity")]
        public int Qty { get; set; }
    [Required(ErrorMessage = "Quantity is Required")]
    [Display(Name = "Rate")]
      public decimal Rate { get; set; }
    
    }
}