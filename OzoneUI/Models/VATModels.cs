﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OzoneUI.Models
{
    public class VATModels
    {
        public long Pk_VAT_Id { get; set; }

        [Display(Name="VAT %")]
        public string VAT_Percent1 { get; set; }
        public Nullable<long> Created_By { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<long> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }

        public List<VATModels> lstVAT { get; set; }
    }
}