﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ozone.DAL;
using System.ComponentModel.DataAnnotations;

namespace OzoneUI.Models
{
    public class POItemModels
    {
        public long Pk_PO_Item_Id { get; set; }
        ///[Required(ErrorMessage = "*")]
        public long Fk_Product_Id { get; set; }
        [Display(Name = "Product Code")]

        public string Product_Code { get; set; }
        [Display(Name = "Product Description")]
        public string Product_Description { get; set; }
       ///[Required(ErrorMessage = "*")]
        public string Packaging { get; set; }
        public string Units { get; set; }
       //[Required(ErrorMessage = "*")]
        [Display(Name = "Total Qty")]
        [DataType(DataType.Currency)]
        public Nullable<long> Total_Qty { get; set; }
        //[Required(ErrorMessage = "*")]
        [Display(Name = "Unit Price")]
        [DataType(DataType.Currency)]
        public Nullable<decimal> Unit_Price { get; set; }
        //[Required(ErrorMessage = "*")]
        [Display(Name = "Total Amount")]
        [DataType(DataType.Currency)]
        public Nullable<decimal> Total_Amt { get; set; }
        public Nullable<long> CurrencyId { get; set; }
        public Nullable<long> Created_by { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<long> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public Nullable<bool> Is_Deleted { get; set; }
        public Nullable<long> Fk_PO_Id { get; set; }

        public List<PRODUCT_DETAILS> lstProduct { get; set; }
        public List<PACKAGE_MASTER> lstPackage { get; set; }
        public List<PACKAGE_MASTER> lstUnits { get; set; }
        public List<CurrencyTab> lstcurrency { get; set; }
        public string CurrencyCode { get; set; }
    }
}