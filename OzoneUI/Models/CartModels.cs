﻿using Ozone.BAL;
using Ozone.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OzoneUI.Models
{
    public class CartModels
    {

        public long Pk_UserCart_Id { get; set; }
        public long Fk_Product_Id { get; set; }
        public long Fk_Package_Id { get; set; }
        [Required]
        [Display(Name = "Quantity")]
        public Nullable<int> Quantity { get; set; }
        public Nullable<long> Create_By { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public System.DateTime Create_Date { get; set; }
        public Nullable<long> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public bool Is_Deleted { get; set; }
        [Display(Name = "Name")]
        public string Full_Name { get; set; }
        [Display(Name = "Address")]
        public string Del_Address { get; set; }
        [Display(Name = "Country")]
        public Nullable<long> Del_Country { get; set; }
        [Display(Name = "State")]
        public Nullable<long> Del_State { get; set; }
        [Display(Name = "City")]
        public string Del_City { get; set; }
        [Display(Name = "Zip Code")]
        public string Del_Zipcode { get; set; }
        [Display(Name = "Phone No.")]
        public string Phone_No { get; set; }
        [Display(Name = "Payment Card")]
        public string Payment_Card { get; set; }
        [Display(Name = "Card Detail")]
        public string Card_Detail { get; set; }
        [Display(Name = "Name on Card")]
        public string Name_On_Card { get; set; }
        [Display(Name = "Card Number")]
        public Nullable<long> Card_No { get; set; }
        [Display(Name = "Expire on Month")]
        public Nullable<int> Card_Exp_Month { get; set; }
        [Display(Name = "Expire on Yean")]
        public Nullable<int> Card_Exp_Year { get; set; }
        [Display(Name = "CVV")]
        public Nullable<int> Card_Cvv { get; set; }
        [Display(Name = "Order Notes")]
        public string Order_Note { get; set; }
        [Display(Name = "Order Number")]

        public string Accepted_Note { get; set; }
        public string Rejected_Note { get; set; }
        public string Processed_Note { get; set; }
        public string Shipped_Note { get; set; }
        public string Delivery_Note { get; set; }

        public string Order_Number { get; set; }
        [Display(Name = "Order Status")]
        public string Order_Status { get; set; }
        [Display(Name = "Payment Status")]
        public string Payment_Status { get; set; }

        [Display(Name = "Payment Amount")]
        public decimal Payment_Amt { get; set; }

        [Display(Name = "VAT")]
        public decimal VAT_Amount { get; set; }
        public string vatPercent { get; set; }
        public decimal ShipRate { get; set; }

        public List<COUNTRY_MASTER> lstCountry { get; set; }
        public List<STATE_MASTER> lstState { get; set; }

        public List<ShoppingCartBAL> lstCart { get; set; }
        public List<SHOPPING_CART> ddlCart { get; set; }
        public ICollection<SHOPPING_CART_ITEMS> lstDALItems { get; set; }
        public string Username { get; set; }

        public IEnumerable<SelectListItem> lstStatus
        {
            get
            {
                return new[]
            {
                new SelectListItem { Value = "Requested", Text = "Requested" },
                new SelectListItem { Value = "Accepted", Text = "Accepted" },
                new SelectListItem { Value = "In Process", Text = "In Process" },
                new SelectListItem { Value = "Processed", Text = "Processed" },
                new SelectListItem { Value = "Shipped", Text = "Shipped" },
                new SelectListItem { Value = "Delivered", Text = "Delivered" },
                new SelectListItem { Value = "Rejected", Text = "Rejected" },              
            };
            }
        }

        public IEnumerable<SelectListItem> lstPaymentStatus
        {
            get
            {
                return new[]
            {
                new SelectListItem { Value = "Partial Payment Received", Text = "Partial Payment Received" },
                new SelectListItem { Value = "Full Payment Received", Text = "Full Payment Received" },
                new SelectListItem{Value="Pending",Text="Pending"},
                new SelectListItem { Value = "Credited", Text = "Credited" },
            };
            }
        }


    }
}