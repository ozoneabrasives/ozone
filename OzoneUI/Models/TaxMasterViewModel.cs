﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ozone.DAL;
using System.ComponentModel.DataAnnotations;
namespace OzoneUI.Models
{
    public class TaxMasterViewModel
    {
        public List<COUNTRY_MASTER> lstcountries { get; set; }
        public TaxMaster objtax { get; set; }
        public long taxid { get; set; }
        [Required(ErrorMessage="Country is Required") ]
        [Display(Name="Country")]
        public long CountryId { get; set; }
        [Required(ErrorMessage="Tax is Required")]
        [Display(Name="Shipping Rate")]
        public decimal Tax { get; set; }

    }
}