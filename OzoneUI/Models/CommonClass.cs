﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OzoneUI.Models
{
    public class CommonClass
    {
        public List<String> TransportList()
        {
            List<string> lsttransport = new List<string>();
            lsttransport.Add("Sea");
            lsttransport.Add("Land");
            lsttransport.Add("Air");
            return lsttransport;
        }
        public List<string> AllowedList()
        {
            List<string> lstallow = new List<string>();
            lstallow.Add("Allowed");
            lstallow.Add("Not Allowed");
            return lstallow;
        }
    }
}