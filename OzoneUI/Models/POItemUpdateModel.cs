﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OzoneUI.Models
{
    public class POItemUpdateModel
    {
        public int Fk_Product_Id { get; set; }
        public int deadstock { get; set; }
        public int ReceiveQty { get; set; }
        public int Pk_PO_Item_Id { get; set; }
        public int Created_by { get; set; }
        public long POID { get; set; }
    }
}