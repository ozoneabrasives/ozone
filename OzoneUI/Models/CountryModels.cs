﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OzoneUI.Models
{
    public class CountryModels
    {

        public long Pk_Country_Id { get; set; }
        [Required]
        [Display(Name = "Country Name")]
        public string Country_Name { get; set; }
        
        [Display(Name = "Country Code")]
        public string Country_Code { get; set; }
        public Nullable<long> Created_by { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<long> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public Nullable<bool> Is_Deleted { get; set; }
    }
}