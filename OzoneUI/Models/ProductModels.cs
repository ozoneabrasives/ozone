﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Ozone.DAL;

namespace OzoneUI.Models
{
    public class ProductModels
    {
        public MultipartDataMediaFormatter.Infrastructure.HttpFile UploadFile { get; set; }
        public long Pk_Product_Id { get; set; }
        [Required]
        [Display(Name = "Product Name")]
        public string Product_Title { get; set; }
     
        [Display(Name = "Product Description")]
        public string Product_Description { get; set; }

        [Display(Name = "Clearing Rate")]
        public string Clearing_Rate { get; set; }
        [Display(Name = "Consumption Rate")]
        public string Consumption_Rate { get; set; }
        [Display(Name = "Surface Profile")]
        public string Surface_Profile { get; set; }
        public string Packaging { get; set; }
        [Display(Name = "Date of Manufacture")]
        public string Manufactur_Date { get; set; }
        [Required ]
        [Display(Name = "Product Code")]
        public string Product_Code { get; set; }
        
        public Nullable<decimal> Price { get; set; }
        [Display(Name = "Product Image")]
        public string Product_Img { get; set; }
        public Nullable<long> Created_by { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<long> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public Nullable<bool> Is_Deleted { get; set; }
        [Required]
        [Display(Name = "Category")]
        public Nullable<long> Fk_Category_Id { get; set; }

        public List<CATEGORY_MASTER> lstCategory { get; set; }
    }
}