﻿using Ozone.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OzoneUI.Models
{
    public class StateModels
    {
        public long Pk_State_Id { get; set; }
        [Required]
        [Display(Name = "Coutnry")]
        public Nullable<long> Fk_Country_Id { get; set; }

        [Required]
        [Display(Name = "State Name")]
        public string State_Name { get; set; }


        [Display(Name = "State Code")]
        public string State_Code { get; set; }
        public Nullable<long> Created_by { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<long> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public Nullable<bool> Is_Deleted { get; set; }

        public List<COUNTRY_MASTER> lstCountry { get; set; }
    }
}