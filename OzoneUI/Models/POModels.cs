﻿using Ozone.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OzoneUI.Models
{
    public class POModels 
    {
        public long Pk_PurchaseOrder_Id { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        [Required(ErrorMessage="Date is Required")]
        [Display(Name = "Order Date")]
        public string PO_Date { get; set; }
        [Display(Name = "Purchase Order #")]
        [Required(ErrorMessage="PO Number is Required")]
        public string PO_Number { get; set; }
        [Display(Name = "Payment Terms")]
        public string Payment_Terms { get; set; }
        public string Transport { get; set; }
        [Display(Name = "Ship Name")]
        [Required(ErrorMessage = "Ship Name is Required")]
        public string Ship_Name { get; set; }
        [Required(ErrorMessage = "Ship Address is Requierd")]
        [Display(Name = "Shipment Address")]
        public string Ship_Address { get; set; }
        [Display(Name = "Shipping Company")]
        [Required(ErrorMessage = "Ship Company is Required")]
        public string Ship_Compny { get; set; }
        [Display(Name = "Ship City")]
        public string Ship_City { get; set; }
        [Display(Name = "Ship State")]
        public string Ship_State { get; set; }

        [Display(Name = "Ship Country")]
        public string Ship_Country { get; set; }
            
        [Display(Name = "Ship ZipCode")]
        [DataType(DataType.PostalCode)]
        public string Ship_Zip_code { get; set; }
        [Required(ErrorMessage = "Ship Phone is Required")]
        [Display(Name = "Ship Phone")]
        public string Ship_Phone { get; set; }
        [Display(Name = "Doc. Required")]
        public Nullable<bool> Document_Req { get; set; }
        [Display(Name = "Shipment Detail")]
        public string Shipment_Detail { get; set; }
        [Display(Name = "Grand Total")]
        public Nullable<decimal> Grand_Total { get; set; }
        [Display(Name = "Checked By")]
        public string Checked_By { get; set; }
        [Display(Name = "Document Required")]
        public string DocumentDetais { get; set; }
        [Required(ErrorMessage="Please select one Vendor")]
        [Display(Name="Vendor")]
        public Nullable<long> Fk_Vendor_Id { get; set; }

        public string PO_Status { get; set; }
        public POItemModels modlAdd { get; set; }
        public Nullable<long> Created_by { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<long> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public Nullable<bool> Is_Deleted { get; set; }
        public Nullable<long> Fk_Package_Id { get; set; }

        public List<COUNTRY_MASTER> lstCountry { get; set; }
        public List<STATE_MASTER> lstState { get; set; }
        public List<PURCHASEORDER_ITEMS> lstPOI { get; set; }

        public List<USER_REGISTRATION> lstVendor { get; set; }
        [Required]
        [Display(Name="Shipper")]
        public string Shipper { get; set; }
        [Required]
        [Display(Name="Consignee")]
        public string Consignee { get; set; }
        [Required]
        [Display(Name="Notify Party")]
        public string NotifyParty { get; set; }
        [Display(Name="Port of Loading")]

        public string PortLoading { get; set; }

        public string PortDischarge { get; set; }
        public string PortDestination { get; set; }
        [Display(Name="Partial Payment")]
        public Nullable<bool> IsPartial { get; set; }
        [Display(Name="Trans Shipment")]
        public Nullable<bool> TransShip { get; set; }
        [Display(Name="Last Date of Shipment")]
        public string LstDateofShip { get; set; }
        public List<String> lstTransport { get; set; }
        public List<String> lstpartialpayment { get; set; }
        public string GRN { get; set; }
        [Display(Name="Delivery Terms")]
        public string DeliveryTerms { get; set; }
        public string amountinnumber { get; set; }
        public string cureencyName { get; set; }
        public string Note { get; set; }
    }
}