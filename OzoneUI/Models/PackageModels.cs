﻿using Ozone.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OzoneUI.Models
{
    public class PackageModels
    {
        public long Pk_Package_Id { get; set; }
        [Required]
        [Display(Name = "Product")]
        public Nullable<long> Fk_Product_Id { get; set; }
        [Required]
        [Display(Name = "Package Name")]
        public string Package_Title { get; set; }
        [Required]
        [Display(Name = "Package Unit")]
        public int Package_Amt { get; set; }
        [Display(Name="Package Unit")]
        public string Package_Unit { get; set; }
        public Nullable<long> Created_by { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<long> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public Nullable<bool> Is_Deleted { get; set; }

        [Display(Name = "Cost Price")]
        public Nullable<decimal> Cost_Price { get; set; }

        [Display(Name = "Sell Price")]
        public Nullable<decimal> Sell_Price { get; set; }

        public List<PRODUCT_DETAILS> lstProduct { get; set; }
        public List<String> lstunits { get; set; }
    }
}