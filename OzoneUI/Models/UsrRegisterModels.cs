﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Ozone.BAL;
using Ozone.DAL;
using System.Web.WebPages.Html;


namespace OzoneUI.Models
{
    public class UsrRegisterModels
    {

        public long Pk_Register_Id { get; set; }
        [Required]
        [Display(Name = "Register Type")]
        public string Register_Type { get; set; }
        [Required]
        [Display(Name = "Company Name/Name")]
        public string Company_Name { get; set; }
        [Required]
        [Display(Name = "Registered Address")]
        public string Permanent_Add { get; set; }
        [Required]
        [Display(Name = "Correspondence Address ")]
        public string Correspond_Add { get; set; }

        [Display(Name = "Registration Number")]
        public string Company_Reg_No { get; set; }
        [Required]
        [Display(Name = "VAT No")]
        public string VAT_No { get; set; }
        [Required]
        [Display(Name = "Email-Id")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email_ID { get; set; }

        [Display(Name = "Telephone No.")]
        public string Landline_No { get; set; }
        [Required]
        [Display(Name = "Mobile No.")]
        public string Mobile_No { get; set; }
       
        public string Fax { get; set; }
        //[Required]
        [Display(Name = "Autorised Oficer Name")]
        public string Auth_Officer_Name { get; set; }
        //[Required]
        [Display(Name = "Delivery Type")]
        public string Delivery_Type { get; set; }

        [Display(Name = "Delivery Detail")]
        public string Delivery_Detail { get; set; }

        [Display(Name = "Payment Terms")]
        public string Payment_Terms { get; set; }
        public Nullable<bool> Is_Approved { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }

        [Display(Name = "Country")]
        public Nullable<long> Fk_Country_Id { get; set; }

        [Display(Name = "State")]
        public Nullable<long> Fk_State_Id { get; set; }

        [Display(Name = "City")]
        public string Fk_City_Id { get; set; }
        [Required]
        [Display(Name = "User Name")]
        public string User_Name { get; set; }
        [Required]
        [Display(Name = "Password")]
        public string User_Password { get; set; }

        [Required]
        [Display(Name = "Confirm Password")]
        [Compare("User_Password", ErrorMessage = "The new password and confirmation password do not match.")]
        public string Confirm_Password { get; set; }
         [Display(Name = "Give Credit Facility")]
        public bool Have_Credit { get; set; }

        [Display(Name="Customer ID")]
         public string Customer_ID { get; set; }
        public string CortNum { get; set; }
        public string RetNum { get; set; }
        [Required]
        [Display(Name="Select Currency")]
        public int CurrencyID { get; set; }
        public List<CurrencyTab> lstcurrency { get; set; }


        public List<USER_REGISTRATION> lstUSR { get; set; }
        public List<COUNTRY_MASTER> lstCountry { get; set; }
        public List<STATE_MASTER> lstState { get; set; }
        
        public IEnumerable<SelectListItem> lstPaymentStatus
        {
            get
            {
                return new[]
            {
                new SelectListItem { Value = "Partial Payment Received", Text = "Partial Payment Received" },
                new SelectListItem { Value = "Full Payment Received", Text = "Full Payment Received" },
                new SelectListItem{Value="Pending",Text="Pending"},
                new SelectListItem { Value = "Credit", Text = "Credit" },
            };
            }
        }
    }
}