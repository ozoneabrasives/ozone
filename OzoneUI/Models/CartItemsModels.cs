﻿using Ozone.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OzoneUI.Models
{
    public class CartItemsModels
    {
        public long Pk_Shopping_Cart_Item_Id { get; set; }
        public Nullable<long> Fk_Shopping_Cart_Id { get; set; }
        public Nullable<long> Fk_Product_Id { get; set; }
        public Nullable<long> Fk_Package_Id { get; set; }
        [Required]
        public Nullable<int> Quantity { get; set; }
        public Nullable<long> Create_By { get; set; }
        public System.DateTime Create_Date { get; set; }
        public Nullable<long> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public Nullable<bool> Is_Deleted { get; set; }
        [Required]
        public Nullable<decimal> Unit_Price { get; set; }
        [Required]
        public Nullable<decimal> Total_Amt { get; set; }
        public Nullable<bool> Is_Extra { get; set; }
        public string Package { get; set; }
        public string Unit { get; set; }
        public string Product_Code { get; set; }
        [Display(Name = "Product Description")]
        public string Product_Description { get; set; }
        public virtual PACKAGE_MASTER PACKAGE_MASTER { get; set; }
        public virtual PRODUCT_DETAILS PRODUCT_DETAILS { get; set; }
        public virtual SHOPPING_CART SHOPPING_CART { get; set; }

        public List<PRODUCT_DETAILS> lstProduct { get; set; }
        public List<PACKAGE_MASTER> lstPackage { get; set; }
        public List<PACKAGE_MASTER> lstUnits { get; set; }
    }
}