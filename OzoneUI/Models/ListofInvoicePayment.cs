﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ozone.DAL;

namespace OzoneUI.Models
{
    public class ListofInvoicePayment
    {
        public List<INV_PAYMENT_TAB> lstpayment { get; set; }
    }
}