﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ozone.DAL;
using Ozone.DAL.Models;

namespace OzoneUI.Models
{
    public class DashboardModels
    {
        public List<SHOPPING_CART> lstCart { get; set; }
        public List<PURCHASE_ORDER> lstPO { get; set; }

        public List<SHOPPING_CART> lstPending { get; set; }

        public List<SHOPPING_CART> lstPayment { get; set; }
        public List<ListofinvoceModel> lstinvoice { get; set; }

    }
}