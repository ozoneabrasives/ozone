﻿using System.Web;
using System.Web.Optimization;

namespace OzoneUI
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/js/plugins/jquery/jquery-ui.js",
                         "~/Scripts/jquery.dataTables.js",
                         "~/js/plugins/icheck/icheck.js",
                         "~/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js",
                         "~/js/plugins.js",
                         "~/js/actions.js"));

            bundles.Add(new ScriptBundle("~/bundles/validation").Include(
                 "~/Scripts/jquery.validate*",
                         "~/Scripts/jquery.validate.unobtrusive.min.js",
                          "~/Scripts/jquery.validate.unobtrusive.min.js",
                          "~/Scripts/jquery.unobtrusive-ajax.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/datepicker").Include("~/js/plugins/bootstrap/bootstrap-datepicker.js"));

            bundles.Add(new ScriptBundle("~/bundles/timepicker").Include("~/js/plugins/bootstrap/bootstrap-timepicker.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/bundles/css").Include(
                     
                      "~/css/jquery/jquery-ui.min.css"));
            bundles.Add(new ScriptBundle("~/bundles/notify").Include(
                 "~/js/plugins/noty/jquery.noty.js",
                 "~/js/plugins/noty/layouts/topCenter.js",
                 "~/js/plugins/noty/layouts/topLeft.js",
                 "~/js/plugins/noty/layouts/topRight.js",
                 "~/js/plugins/noty/themes/default.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/datatable").Include(
                "~/js/plugins/datatables/jquery.dataTables.js"));
            bundles.Add(new ScriptBundle("~/bundles/selector").Include(
                "~/js/plugins/bootstrap/bootstrap-select.js"
                ));
        }
    }
}