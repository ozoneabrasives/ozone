﻿using Ozone.BAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;

namespace OzoneUI
{
    public class Utility
    {
        public static long userId = 0;
        public static string userType = "";

        public static string SendConformationMail(RegisterBAL reg)
        {
            try
            {
                using (MailMessage mm = new MailMessage("no-reply@ozoneabrasives.co.uk", reg.Email_ID))
                {
                    
                      
                    mm.Subject = "Confirmation Link";
                    StringBuilder str = new StringBuilder();
                    str.Append("<html><head></head><body>");
                    str.Append("<b>Hi, <br>" + reg.Company_Name + "</b>");
                    str.Append("<br><br>");
                    str.Append("You are registered successfully, click on link given below to activate your account.");
                    str.Append("<br><br>");
                    str.Append("http://shop.ozoneabrasives.co.uk/RegisterView/ConfirmAccount?id=" + reg.Pk_Register_Id);
                   // str.Append("http://localhost:59980//RegisterView/ConfirmAccount?id=" + reg.Pk_Register_Id);
                    str.Append("<br><br>");                                        
                    str.Append("<br><br>");
                   
                    str.Append("</body>");
                    mm.Body = str.ToString();                     
                    mm.IsBodyHtml = true;

                    SmtpClient smtp = new SmtpClient();                    
                    smtp.EnableSsl = true;
                    smtp.UseDefaultCredentials = true;
                    NetworkCredential NetworkCred = new NetworkCredential("no-reply@ozoneabrasives.co.uk", "Ozone#321");
                   
                    smtp.Credentials = NetworkCred;
                    smtp.Host = "send.one.com";
                 //   smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    //smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtp.Send(mm);
                    return "Success";
                    // ClientScript.RegisterStartupScript(GetType(), "alert", "alert('Email sent.');", true);
                }
            }
            catch (Exception ex)
            {
                return "Fail";
            }
        }

        public static string SendMail(string mailid, string name, string usrtype,PurchaseOrderBAL po)
        {
            try
            {
                using (MailMessage mm = new MailMessage("no-reply@ozoneabrasives.co.uk", mailid))
                {
                    mm.Bcc.Add("hardik.mehta151@gmail.com");

                    string encodEmailID = Encode(mailid);
                    string encodName = Encode(name);
                    string utype = Encode(usrtype);
                    mm.Subject = "Pruchase Order";
                    StringBuilder str = new StringBuilder();
                    str.Append("<html><head></head><body>");
                    str.Append("<b>Hi <br>" + po.Ship_Name + "</b>");
                    str.Append("<br><br>");
                    str.Append("Your Purchase Order is Generated For the Date "+po.PO_Date);
                    str.Append("<br><br>");
                    str.Append("Purchase Order Number is :"+ po.PO_Number);
                    str.Append("<br><br>");
                    str.Append("Plsease find the attachment to view Purchase Order in detail.");
                    str.Append("<br><br>");
                    if (name != null)
                        mm.Attachments.Add(new Attachment(name));
                    //http://localhost:14416/Register/RegisterUsr   http://papaeye.softnsource.com/
                    //if (usrtype == "parents")
                    //{
                    //    str.Append("http://papaeye.softnsource.com/Parents/Create?id=" + encodEmailID + "&name=" + encodName + "&type=" + utype);
                    //}
                    //else
                    //{
                    //    str.Append("http://papaeye.softnsource.com/Institute/Index?id=" + encodEmailID + "&name=" + encodName + "&type=" + utype);
                    //}
                    str.Append("</body>");
                    mm.Body = str.ToString();
                    //if (fuAttachment.HasFile)
                    //{
                    //    string FileName = Path.GetFileName(fuAttachment.PostedFile.FileName);
                    //    mm.Attachments.Add(new Attachment(fuAttachment.PostedFile.InputStream, FileName));
                    //}
                    mm.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.EnableSsl = true;
                    smtp.UseDefaultCredentials = true;
                    NetworkCredential NetworkCred = new NetworkCredential("no-reply@ozoneabrasives.co.uk", "Ozone#321");
                    smtp.Credentials = NetworkCred;
                    smtp.Host = "send.one.com";
                    smtp.Port = 587;
                    //smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtp.Send(mm);
                    return "Success";
                    // ClientScript.RegisterStartupScript(GetType(), "alert", "alert('Email sent.');", true);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static string SendGeneralMail(string mailid, string name, string usrtype,string body)
        {
            try
            {
                using (MailMessage mm = new MailMessage("no-reply@ozoneabrasives.co.uk", mailid))
                {
                  

                    string encodEmailID = Encode(mailid);
                    //string encodName = Encode(name);
                    //string utype = Encode(usrtype);
                    mm.Subject = "Pruchase Order";
                    StringBuilder str = new StringBuilder();
                    str.Append("<html><head></head><body>");
                    str.Append("<b>Hi <br>" + name + "</b>");
                    str.Append("<br><br>");
                    str.Append("Your Order is Changed For the Date " + DateTime.Now.ToShortDateString());
                    str.Append("<br><br>");
                    str.Append("Order Number is :" + body);
                    str.Append("<br><br>");
                    if (usrtype != "")
                    {
                        str.Append("Order Staus :" + usrtype.Split(':')[0]);
                        str.Append("<br><br>");
                        str.Append("Notes :" + usrtype.Split(':')[1]);
                        str.Append("<br><br>");
                    }
                    str.Append("Plsease check your order for further process.");
                    str.Append("<br><br>");
                    //if (name != null && name != "")
                    //    mm.Attachments.Add(new Attachment(name));
                    //http://localhost:14416/Register/RegisterUsr   http://papaeye.softnsource.com/
                    //if (usrtype == "parents")
                    //{
                    //    str.Append("http://papaeye.softnsource.com/Parents/Create?id=" + encodEmailID + "&name=" + encodName + "&type=" + utype);
                    //}
                    //else
                    //{
                    //    str.Append("http://papaeye.softnsource.com/Institute/Index?id=" + encodEmailID + "&name=" + encodName + "&type=" + utype);
                    //}
                    str.Append("</body>");
                    mm.Body = str.ToString();
                    //if (fuAttachment.HasFile)
                    //{
                    //    string FileName = Path.GetFileName(fuAttachment.PostedFile.FileName);
                    //    mm.Attachments.Add(new Attachment(fuAttachment.PostedFile.InputStream, FileName));
                    //}
                    mm.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.EnableSsl = true;
                    smtp.UseDefaultCredentials = true;
                    NetworkCredential NetworkCred = new NetworkCredential("no-reply@ozoneabrasives.co.uk", "Ozone#321");
                    smtp.Credentials = NetworkCred;
                    smtp.Host = "send.one.com";
                    smtp.Port = 587;
                    smtp.Send(mm);
                    return "Success";
                    // ClientScript.RegisterStartupScript(GetType(), "alert", "alert('Email sent.');", true);
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static string SendEmaildynamic(string mailid, string name, string subject, string body)
        {
            try
            {
                using (MailMessage mm = new MailMessage("no-reply@ozoneabrasives.co.uk", mailid))
                {
                   

                    string encodEmailID = Encode(mailid);
                    //string encodName = Encode(name);
                    //string utype = Encode(usrtype);
                    mm.Subject = subject;
                    //StringBuilder str = new StringBuilder();
                    //str.Append("<html><head></head><body>");
                    //str.Append("<b>Hi <br>" + name + "</b>");
                    //str.Append("<br><br>");
                    //str.Append("Your Order is Changed For the Date " + DateTime.Now.ToShortDateString());
                    //str.Append("<br><br>");
                    //str.Append("Order Number is :" + body);
                    //str.Append("<br><br>");
                    
                    //str.Append("Plsease check your order for further process.");
                    //str.Append("<br><br>");
                    //if (name != null && name != "")
                    //    mm.Attachments.Add(new Attachment(name));
                    //http://localhost:14416/Register/RegisterUsr   http://papaeye.softnsource.com/
                    //if (usrtype == "parents")
                    //{
                    //    str.Append("http://papaeye.softnsource.com/Parents/Create?id=" + encodEmailID + "&name=" + encodName + "&type=" + utype);
                    //}
                    //else
                    //{
                    //    str.Append("http://papaeye.softnsource.com/Institute/Index?id=" + encodEmailID + "&name=" + encodName + "&type=" + utype);
                    //}
                   // str.Append("</body>");
                    mm.Body = body;
                    //if (fuAttachment.HasFile)
                    //{
                    //    string FileName = Path.GetFileName(fuAttachment.PostedFile.FileName);
                    //    mm.Attachments.Add(new Attachment(fuAttachment.PostedFile.InputStream, FileName));
                    //}
                    mm.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.EnableSsl = true;
                    smtp.UseDefaultCredentials = true;
                    NetworkCredential NetworkCred = new NetworkCredential("no-reply@ozoneabrasives.co.uk", "Ozone#321");
                    smtp.Credentials = NetworkCred;
                    smtp.Host = "send.one.com";
                    smtp.Port = 587;
                    smtp.Send(mm);
                    return "Success";
                    // ClientScript.RegisterStartupScript(GetType(), "alert", "alert('Email sent.');", true);
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

       

        public static string SendOrderMail(string mailId,ShoppingCartBAL bal)
        {
            try
            {
                using (MailMessage mm = new MailMessage("no-reply@ozoneabrasives.co.uk", mailId))
                {
                  
                    string encodEmailID = Encode(mailId);
                    string encodName = Encode(bal.Full_Name);                    
                    mm.Subject = "Order Confirmed";
                    StringBuilder str = new StringBuilder();
                    str.Append("<html><head></head><body>");
                    str.Append("<b>Hello <br />" + bal.Full_Name + "</b>");
                    str.Append("<br><br>");
                    str.Append("Your Order is confirmed For the Date " + DateTime.Now.ToShortDateString());
                    str.Append("<br><br>");
                    str.Append("Order Number is :" + bal.Order_Number);
                    str.Append("<br><br>");
                  str.Append(" <div class='sr-item'>");
                  str.Append("                 <div class='col-md-8'>");
                  str.Append("                     <div class='table-responsive push-up-10'>");
                  str.Append("                         <div class='panel-heading'>");
                  str.Append("                             <h3 class='panel-title'>Item(s) Confirmed</h3>");
                  str.Append("                         </div>");
                  str.Append("                         <table id='tblItems' width='100%' class='table table-actions table-striped'>");
                  str.Append("                             <thead>");
                  str.Append("                                 <tr>");
                  str.Append("                                     <th>Item</th>");
                  str.Append("                                     <th>Description</th>");
                  str.Append("                                     <th>Price</th>");
                  str.Append("                                     <th>Quantity</th>");
                  str.Append("                                     <th>Total</th>");              
                  str.Append("                                 </tr>");
                  str.Append("                             </thead>");
                  str.Append("                             <tbody>");
                 

                    foreach (var item in bal.lstItems)
	                    { 
                        str.Append("                                         <tr>");
		                 str.Append("                                             <td>");
                         string url = HttpContext.Current.Server.MapPath("~/CompanyLogo/" + item.PRODUCT_DETAILS.Product_Img);
                  str.Append("                                                 <a href='javascript:void(0)'><img src="+ url+" alt='' style='width:80px;'></a>");
                  str.Append("                                             </td>");
                  str.Append("                                             <td>");
                  str.Append("                                                 <h4><a href='javascript:void(0)'>"+item.PRODUCT_DETAILS.Product_Title+"</a></h4>");
                  str.Append("                                                 <p>"+item.PACKAGE_MASTER.Package_Title+" - "+item.PACKAGE_MASTER.Package_Unit+"</p>");
                  str.Append("                                             </td>");
                  str.Append("                                             <td>");
                  str.Append("                                                 <p id='pPrice'>"+item.Unit_Price+"</p>");
                  str.Append("                                             </td>");
                  str.Append("                                             <td>");
                  str.Append("                                              "+item.Quantity+" ");
                  str.Append("                                             </td>");
                  str.Append("                                             <td>");
                  str.Append("                                                 <p class='cart_total_price' id='pTotal'>"+item.Total_Amt+"</p>");                
                  str.Append("                                             </td>");                  
                  str.Append("                                         </tr>");
	                    }                 
                  str.Append("                             </tbody>");
                  str.Append("                         </table>");
                  str.Append("                     </div>");
                  str.Append("                 </div>");
                  str.Append("                 <div class='col-md-4'>");
                  str.Append("                     <div class='table-responsive push-up-10'>");
                  str.Append("                         <div class='panel-heading'>");                
                  str.Append("                         </div>");
                  str.Append("                         <div class='form-group' style='float:left' >");
                  str.Append("                             <label class='col-md-8 control-label' >Order Total</label>");
                  str.Append("                             <label class='col-md-8 control-label'>" + bal.Payment_Amt + "</label>");
                  str.Append("                         </div>");
                  str.Append("                     </div>");
                  str.Append("                 </div>");
                  str.Append("             </div>");
              
                    str.Append("<br><br>");                                        
                    str.Append("</body>");
                    mm.Body = str.ToString();                    
                    mm.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.EnableSsl = true;
                    smtp.UseDefaultCredentials = true;
                    NetworkCredential NetworkCred = new NetworkCredential("no-reply@ozoneabrasives.co.uk", "Ozone#321");
                    smtp.Credentials = NetworkCred;
                    smtp.Host = "send.one.com";
                    smtp.Port = 587;
                    smtp.Send(mm);
                    return "Success";
                    // ClientScript.RegisterStartupScript(GetType(), "alert", "alert('Email sent.');", true);
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            
        }

        public static string Encode(string encodeMe)
        {
            if (!string.IsNullOrEmpty(encodeMe))
            {
                byte[] encoded = System.Text.Encoding.UTF8.GetBytes(encodeMe);
                return Convert.ToBase64String(encoded);
            }
            else
            {
                return string.Empty;
            }
        }

        public static string Decode(string decodeMe)
        {
            if (!string.IsNullOrEmpty(decodeMe))
            {
                byte[] encoded = Convert.FromBase64String(decodeMe);
                return System.Text.Encoding.UTF8.GetString(encoded);
            }
            else
            {
                return string.Empty;
            }
        }
    }
}